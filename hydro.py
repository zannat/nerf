#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK
"""
Elastic deformation from GRACE data.
"""
from argparse import ArgumentParser
from os.path import join, basename
from os import remove
from glob import glob
from datetime import datetime, timedelta
import gc
from collections import defaultdict

import numpy
from numpy import pi, sqrt, array, radians
from numpy.linalg import norm
from scipy.interpolate import interp1d

from coseismic import surface_displacements
from earth import earth_radius
from basic import ii, angular_to_cartesian
from stations import Stations, uniform_area_grid
from summary import standard_deviation_T, standard_deviation_R
from summary import standard_deviation_D
from summary import Field, display_raw_statistics, convert_to_binary
from summary import binary_to_field
from utils import info, read_fortran, care
from frame import write_options
from frame import shift_in_origin
from frame import read_options
from frame import summation_shift, transformation_shift_timeseries
from monte_carlo import UniformAreaSampling, converged
from GPS import stations_from_table, stations_from_SOPAC


def read_Love_numbers(filename):
    """
    Read Love numbers from data file.
    """
    def converter(s):
        """ Convert Fortran-style floats to Python-style floats. """
        return float(s.replace('D', 'e').replace('d', 'e'))

    converters = {1: converter, 2: converter, 3: converter}

    _, h, l, k = numpy.loadtxt(filename, converters=converters,
                               unpack=True)
    return {'h': h, 'l': l, 'k': k}


def read_anomaly_file(filename, file_format='anomaly'):
    """
    Read gravity anomaly field from files.
    """
    if file_format == 'anomaly':
        # the static offset has been subtracted
        l, m, C, S = numpy.loadtxt(filename, usecols=(0, 1, 2, 3),
                                   unpack=True)
    elif file_format == 'RL05':
        # the public release version 5
        l, m, C, S = numpy.loadtxt(filename, usecols=(1, 2, 3, 4),
                                   unpack=True, skiprows=7)
    elif file_format == 'geo':
        # static model GGM03C
        l, m, C, S = [], [], [], []
        with open(filename) as fl:
            for line in fl:
                try:
                    _, l_, m_, C_, S_ = read_fortran('A6,2I3,2D21.14', line)
                    if l_ < 150:
                        # prevent overflow in spherical harmonic norm
                        l.append(l_)
                        m.append(m_)
                        C.append(C_)
                        S.append(S_)
                except ValueError:
                    pass

            l = numpy.array(l)
            m = numpy.array(m)
            C = numpy.array(C)
            S = numpy.array(S)

    l = l.astype(int)
    m = m.astype(int)

    result = {}
    for i in range(l.shape[0]):
        if m[i] == 0:
            result[l[i], 0] = 2. * sqrt(pi) * C[i]
        else:
            result[l[i], m[i]] = ((-1) ** m[i] * sqrt(2. * pi) *
                                  (C[i] - ii * S[i]))

    return result, max([i for i, _ in result])


def read_degree_1_coefficients(filename):
    """
    Read degree-1 coefficients timeseries.
    """
    m0 = {}
    m1 = {}

    def to_year(s):
        """
        Convert to year.
        """
        year = int(s[:4])
        month = int(s[4:6])
        day = int(s[6:])
        return decimal_year(year, month=month, day=day)

    def process_line(line):
        """
        Read one line and put the value into one of the
        two dictionaries *m0* and *m1*.
        """
        fmt = '(a6,2i5,2E19.12,2E11.4,2(A14))'
        try:
            _, l, m, C, S, _, _, start, end = read_fortran(fmt, line)
        except ValueError:
            return

        if l is None or l != 1 or m not in [0, 1]:
            return

        start = start[:start.find('.')].strip()
        end = end[:end.find('.')].strip()

        mid = (to_year(start) + to_year(end)) / 2.

        if m == 0:
            m0[mid] = 2. * sqrt(pi) * C
        elif m == 1:
            m1[mid] = ((-1) ** m * sqrt(2. * pi) *
                       (C - ii * S))

    with open(filename) as fl:
        for line in fl:
            process_line(line)

    assert sorted(m0.keys()) == sorted(m1.keys())
    epochs = sorted(m0.keys())

    m0 = array([m0[epoch] for epoch in epochs])
    m1 = array([m1[epoch] for epoch in epochs])

    epochs = array(epochs)

    def interp(arr):
        """ Cubic spline interpolation. """
        return interp1d(epochs, arr, kind='cubic')

    return interp(m0), interp(m1)


def hydro_modes(love, anomaly):
    """
    Hydrological elastic deformation modes
    from Love numbers and gravity anomaly.
    """
    result = {}

    with numpy.errstate(invalid='ignore', divide='ignore'):
        vertical = love['h'] / (1. + love['k'])
        lateral = love['l'] / (1. + love['k'])

    # no l=0 motion
    vertical[0] = 0.
    lateral[0] = 0.

    # include geocenter motion for l=1
    h_1 = love['h'][1]
    l_1 = love['l'][1]

    alpha_CF = (h_1 + 2. * l_1) / 3.
    k_1_CF = -1. - alpha_CF
    vertical[1] = h_1 / (1 + k_1_CF)
    lateral[1] = l_1 / (1 + k_1_CF)

    for l, m in anomaly:
        result[l, m] = array([vertical[l] * anomaly[l, m], 0.,
                              lateral[l] * anomaly[l, m], 0.])

    return result


def one_point(latitude, longitude, max_l):
    """ Test station set with just one station. """
    theta = array([radians(90. - latitude)])
    phi = array([radians(longitude)])
    return Stations(theta, phi, max_l, max_l)


def theory_std(spheroidal, T_CF, radius_km):
    """ Theoretical standard deviation of the fields. """
    toroidal = {}
    R_CF = array([0., 0., 0.])
    D_CF = 0.
    # print 'theory:'
    u_std = standard_deviation_T(spheroidal, toroidal,
                                 T_CF, radius_km) * 10.
    w_std = standard_deviation_R(spheroidal, toroidal, R_CF)
    s_std = standard_deviation_D(spheroidal, D_CF)
    # print '    u std (mm): {}'.format(u_std)
    # print '   w std (μas): {}'.format(w_std)
    return u_std, s_std, w_std


def decimal_year(year, month=None, day=None):
    """
    If *month* is `None` then convert (year, day of year),
    otherwise convert (year, month, day of month)
    into deciaml year.
    """
    if day is None:
        raise ValueError("case not covered: day is None")

    this_year = datetime(year=year, month=1, day=1)
    next_year = datetime(year=year + 1, month=1, day=1)
    whole_year = next_year - this_year

    if month is None:
        elapsed = timedelta(days=day)
    else:
        elapsed = datetime(year=year, month=month, day=day) - this_year

    return year + elapsed.total_seconds() / whole_year.total_seconds()


def decimal_year_to_date(decimal):
    """
    Convert a decimal year back to date.
    """
    year = int(numpy.floor(decimal))
    fraction = decimal - year

    this_year = datetime(year=year, month=1, day=1)
    next_year = datetime(year=year + 1, month=1, day=1)
    whole_year_in_seconds = (next_year - this_year).total_seconds()
    fraction_in_seconds = fraction * whole_year_in_seconds
    return this_year + timedelta(seconds=fraction_in_seconds)


def timeline(folder, year_range=None, file_format='anomaly'):
    """
    The chronological list of anomaly files.
    """
    if file_format == 'anomaly':
        anomaly_files = glob(join(folder, '*.anomaly'))
    elif file_format == 'RL05':
        anomaly_files = glob(join(folder, '*'))
    elif file_format == 'npz':
        anomaly_files = glob(join(folder, '*.npz'))

    basenames = [basename(fl) for fl in anomaly_files]

    def entry(name):
        """ Extract representative epoch from filename. """
        mid_bit = name[6:21]

        start, end = mid_bit.split('-')

        def convert(s):
            """ Convert to decimal year. """
            return decimal_year(int(s[:4]), day=int(s[4:]))

        start = convert(start)
        end = convert(end)
        return (start + end) / 2., name, (start, end)

    lookup = [entry(name) for name in basenames]
    result = sorted(lookup, key=lambda triple: triple[0])

    if year_range is not None:
        first, last = year_range

        return [x for x in result if x[0] >= first and x[0] <= last]
    else:
        return result


def snapshot(love, anomaly, stations, debug=False):
    """
    The hydrological elastic deformation field
    evaluated on a network at a given epoch.
    """
    # in cm
    radius_cm = earth_radius * 1.e+2

    spheroidal = hydro_modes(love, anomaly)
    toroidal = {}

    spherical_u = numpy.zeros((3,) + stations.shape)
    for batch, stations_batch in stations.batches:
        if debug:
            info('.')

        spherical_u[:, batch] += (radius_cm *
                                  surface_displacements(spheroidal,
                                                        toroidal,
                                                        stations_batch))

    return Field.from_spherical(stations.theta, stations.phi, spherical_u)


def fix_degree_1(epoch, folder, anomaly_file, interp_m,
                 file_format='anomaly'):
    """
    Read gravity anomaly coefficients from file
    and replace the degree-1 values by the interpolated values
    of ocean model predictions at the given epoch.
    """
    m0, m1 = interp_m

    anomaly, max_l = read_anomaly_file(join(folder, anomaly_file),
                                       file_format=file_format)
    anomaly[1, 0] = m0(epoch)
    anomaly[1, 1] = m1(epoch)
    return anomaly, max_l


def time_range(args):
    """ Given range of time to report. """
    if args.from_year > 0 and args.to_year > 0:
        return (args.from_year, args.to_year)
    else:
        return None


def anomaly_grid2(args):
    """
    Generate globals grids of elastic deformation due to
    gravity anomaly but without using the static gravity
    field model GGM03.
    """
    folder = args.grid_folder
    grid_timeline = timeline(folder, time_range(args),
                             file_format='npz')

    theta = None
    phi = None
    u = []
    for _, grid_file, _ in grid_timeline:
        grd = numpy.load(join(folder, grid_file))
        if theta is None and phi is None:
            theta, phi = grd['theta'], grd['phi']
        u.append(grd['u'])

    u = numpy.array(u)
    static = {'theta': theta, 'phi': phi,
              'u': numpy.average(u, axis=0)}

    for _, grid_file, _ in grid_timeline:
        grd = numpy.load(join(folder, grid_file))
        anomaly_file = join(args.anomaly_output_folder2, grid_file)
        numpy.savez_compressed(anomaly_file,
                               theta=static['theta'],
                               phi=static['phi'],
                               u=grd['u'] - static['u'])

    options = read_options(args.grid_folder)
    write_options(args.anomaly_output_folder2, options)


def anomaly_grid(args):
    """
    Generate globals grids of elastic deformation due to
    gravity anomaly.
    """
    folder = args.grid_folder
    grid_timeline = timeline(folder, time_range(args),
                             file_format='npz')

    static = numpy.load(args.static_grid)

    for _, grid_file, _ in grid_timeline:
        grd = numpy.load(join(folder, grid_file))
        anomaly_file = join(args.anomaly_output_folder, grid_file)
        numpy.savez_compressed(anomaly_file,
                               theta=static['theta'],
                               phi=static['phi'],
                               u=grd['u'] - static['u'])

    options = read_options(args.grid_folder)
    write_options(args.anomaly_output_folder, options)


def static_grid(args):
    """ Generate a global grid of static field. """
    # in km
    radius_km = earth_radius * 1.e-3
    love = read_Love_numbers(args.love_numbers_file)

    anomaly_file = args.static_model
    file_base = basename(anomaly_file)

    anomaly, max_l = read_anomaly_file(anomaly_file, file_format='geo')
    stations = uniform_area_grid(args.latitudes, args.longitudes,
                                 max_l, max_l)
    field = snapshot(love, anomaly, stations, debug=args.debug)
    folder = args.static_output_folder
    field.write(join(folder, file_base + ".log"))
    convert_to_binary(folder, filename=file_base)
    remove(join(folder, file_base + ".log"))

    options = {'method': 'area-grid',
               'model': {'radius': radius_km},
               'latitudes': args.latitudes,
               'longitudes': args.longitudes}
    write_options(folder, options)


def grid(args):
    """ Generate a timeseries of displacement field on a global grid. """

    # in km
    radius_km = earth_radius * 1.e-3

    love = read_Love_numbers(args.love_numbers_file)
    folder = args.anomaly_folder
    anomaly_timeline = timeline(folder, time_range(args),
                                file_format=args.file_format)
    interp_m = read_degree_1_coefficients(args.geocenter_coefficients)

    def process_epoch(epoch, anomaly_file):
        """
        Process one epoch.
        """
        if args.file_format == 'anomaly':
            file_base = basename(anomaly_file)[:-len('.anomaly')]
        else:
            file_base = basename(anomaly_file)

        anomaly, max_l = fix_degree_1(epoch, folder, anomaly_file, interp_m,
                                      file_format=args.file_format)
        stations = uniform_area_grid(args.latitudes, args.longitudes,
                                     max_l, max_l)
        field = snapshot(love, anomaly, stations)
        field.write(join(args.output_folder, file_base + ".log"))
        convert_to_binary(args.output_folder, filename=file_base)
        remove(join(args.output_folder, file_base + ".log"))

    for epoch, anomaly_file, _ in anomaly_timeline:
        try:
            if args.debug:
                info("processing {}\n", anomaly_file)

            process_epoch(epoch, anomaly_file)

        except ValueError:
            if args.debug:
                info("... failed, unfortunately.\n")

    options = {'method': 'area-grid',
               'model': {'radius': radius_km},
               'latitudes': args.latitudes,
               'longitudes': args.longitudes}
    write_options(args.output_folder, options)


def bias_timeseries(args):
    " Produce timeseries of network bias at each snapshot. "
    # in km
    radius_km = earth_radius * 1.e-3
    # in cm
    radius_cm = earth_radius * 1.e+2

    love = read_Love_numbers(args.love_numbers_file)
    folder = args.anomaly_folder
    anomaly_timeline = timeline(folder, time_range(args),
                                file_format=args.file_format)

    interp_m = read_degree_1_coefficients(args.geocenter_coefficients)

    def process_epoch(epoch, anomaly_file):
        """
        Process on epoch.
        """
        anomaly, _ = fix_degree_1(epoch, folder, anomaly_file, interp_m,
                                  file_format=args.file_format)
        spheroidal = hydro_modes(love, anomaly)

        T_CF = shift_in_origin(spheroidal) * radius_cm
        u_std, s_std, w_std = theory_std(spheroidal, T_CF, radius_km)

        info("{:.3f} {:.3f} {:.3f} {:.3f} {} {:.3f} {}\n"
             .format(epoch, T_CF[0], T_CF[1], T_CF[2],
                     u_std, s_std, w_std))

    for epoch, anomaly_file, _ in anomaly_timeline:
        try:
            if args.debug:
                info("processing {}\n", anomaly_file)

            process_epoch(epoch, anomaly_file)

        except ValueError:
            if args.debug:
                info("... failed, unfortunately.\n")


def geocenter(args):
    """
    Geocenter motion from GRGS anomaly files.
    """
    # in cm
    radius_cm = earth_radius * 1.e+2

    love = read_Love_numbers(args.love_numbers_file)
    folder = args.anomaly_folder
    anomaly_timeline = timeline(folder, time_range(args),
                                file_format=args.file_format)
    interp_m = read_degree_1_coefficients(args.geocenter_coefficients)

    def process_epoch(epoch, anomaly_file, start, end):
        """ Process one epoch at a time. """
        if args.debug:
            info("processing {}\n", anomaly_file)

        try:
            anomaly, _ = fix_degree_1(epoch, folder, anomaly_file, interp_m,
                                      file_format=args.file_format)
            spheroidal = hydro_modes(love, anomaly)
            T_CF = shift_in_origin(spheroidal) * radius_cm
            string = ("{:.3f} {:.3f} {:.3f} {:.3f} {:.3f} {:.3f}\n"
                      .format(epoch,
                              T_CF[0], T_CF[1], T_CF[2],
                              start, end))
            info(string)
        except ValueError:
            if args.debug:
                info("... failed, unfortunately.\n")

    for epoch, anomaly_file, (start, end) in anomaly_timeline:
        process_epoch(epoch, anomaly_file, start, end)


def timeseries(args):
    """
    Timeseries for one observation point from GRGS anomaly files
    and ocean model.
    """
    love = read_Love_numbers(args.love_numbers_file)
    folder = args.anomaly_folder

    anomaly_timeline = timeline(args.anomaly_folder, time_range(args),
                                file_format=args.file_format)
    _, max_l = read_anomaly_file(join(folder, anomaly_timeline[0][1]),
                                 args.file_format)
    stations = one_point(args.latitude, args.longitude, max_l)
    interp_m = read_degree_1_coefficients(args.geocenter_coefficients)

    for epoch, anomaly_file, _ in anomaly_timeline:
        if args.debug:
            info("processing {}\n", anomaly_file)
        try:
            anomaly, _ = fix_degree_1(epoch, folder, anomaly_file, interp_m,
                                      file_format=args.file_format)
            field = snapshot(love, anomaly, stations)
            u = field.geographical_u[:, 0]
            info("{:.3f} {:.3f} {:.3f} {:.4f} {:.4f} {:.4f}\n"
                 .format(args.latitude, args.longitude, epoch,
                         u[0], u[1], u[2]))
        except ValueError:
            if args.debug:
                info("... failed, unfortunately.\n")


def test_scaling(args):
    """ Test CLT scaling. """
    # in km
    radius_km = earth_radius * 1.e-3

    love = read_Love_numbers(args.love_numbers_file)

    # test_anomaly = 'GSM-2_2010008-2010017_simple.anomaly'
    test_anomaly = 'GSM-2_2007004-2007013_simple.anomaly'

    anomaly, max_l = read_anomaly_file(join(args.anomaly_folder, test_anomaly),
                                       file_format=args.file_format)

    stations = uniform_area_grid(args.latitudes, args.longitudes,
                                 max_l, max_l)
    field = snapshot(love, anomaly, stations)

    options = {'method': 'area-grid',
               'model': {'radius': radius_km},
               'latitudes': args.latitudes,
               'longitudes': args.longitudes}

    folder = '../data/hydro/first-test-CLT-scaling'
    field.write(join(folder, 'field.log'))
    convert_to_binary(folder)
    write_options(folder, options)
    display_raw_statistics(field, options)


def least_square_fit(epochs, vectors, delta_t, axis=1):
    """
    Line to fit timeseries of vector data.
    """
    def time_average(data, axis=None):
        """
        Average over the timeline.
        """
        return numpy.average(data, axis=axis, weights=delta_t)

    t_ref = time_average(epochs)
    x_ref = time_average(vectors, axis=axis)
    var_t = time_average(epochs ** 2) - t_ref ** 2
    cov_tx = time_average(vectors * epochs, axis=axis) - x_ref * t_ref
    v_ref = cov_tx / var_t
    return t_ref, x_ref, v_ref


def read_grid_timeseries(grid_folder, grid_timeline):
    """
    Read timeseries of displacements on a grid into a single
    `Field` object.
    """
    # number of epochs
    n = len(grid_timeline)

    epochs = numpy.zeros(n)
    delta_t = numpy.zeros(n)

    _, npz_file, _ = grid_timeline[0]
    field = binary_to_field(grid_folder, npz_file)
    theta = field.theta
    phi = field.phi

    cartesian_u = numpy.zeros((3, theta.shape[0], n))

    for t, (epoch, npz_file, (start, end)) in enumerate(grid_timeline):
        field = binary_to_field(grid_folder, npz_file)
        cartesian_u[:, :, t] = field.cartesian_u
        epochs[t] = epoch
        delta_t[t] = end - start

    return Field(theta, phi, cartesian_u), epochs, delta_t


def Helmert_parameters(args):
    """ Calculate helmert parameters from grid files. """
    numpy.set_printoptions(precision=3, suppress=True)
    grid_timeline = timeline(args.grid_folder, time_range(args),
                             file_format='npz')
    options = read_options(args.grid_folder)
    field, epochs, delta_t = read_grid_timeseries(args.grid_folder,
                                                  grid_timeline)

    t_ref, u_ref, v_ref = least_square_fit(epochs, field.u_mean(options),
                                           delta_t)
    t_ref, s_ref, sdot_ref = least_square_fit(epochs, field.s_mean(options),
                                              delta_t)
    t_ref, w_ref, omega_ref = least_square_fit(epochs, field.w_mean(options),
                                               delta_t)

    print_Helmert_parameters(t_ref, (u_ref, s_ref, w_ref),
                             (v_ref, sdot_ref, omega_ref))


ABGS = one_point(0.2208, 99.3875, 2)
BRAZ = one_point(-15.947, 312.122, 2)


def average(tally, key, axis=1):
    """ Mean of a tally of data. """
    return tally[key].mean(axis=axis)


def stddev(tally, key, axis=1):
    """ Standard deviation of a tally of data. """
    mean = average(tally, key, axis=axis)
    diff = (tally[key].T - mean).T ** 2
    variance = diff.mean(axis=axis)
    return numpy.sqrt(variance)


def interpolated_displacement_field(args):
    """
    Interpolated field as a function of space and time.
    Use with care.
    """
    options = read_options(args.grid_folder)
    grid_timeline = timeline(args.grid_folder, time_range(args),
                             file_format='npz')
    field, epochs, delta_t = read_grid_timeseries(args.grid_folder,
                                                  grid_timeline)
    u = field.interpolate(options)

    # just in case
    gc.collect()

    return u, epochs, delta_t


def transformation_shift_factory(epochs, delta_t):
    """
    Create the transformation shift function for timeseries
    for the specificied set of epochs.
    """
    def shift(field, radius, Voronoi=False):
        """ Shift by transformation method. """
        t_ref = numpy.average(epochs, weights=delta_t)
        dt = epochs - t_ref

        (inst_params,
         deriv_params) = transformation_shift_timeseries(field, radius, dt,
                                                         Voronoi=Voronoi)

        return t_ref, inst_params, deriv_params

    return shift


def summation_shift_factory(epochs, delta_t):
    """
    Create the summation shift function for timeseries
    for the specificied set of epochs.
    """
    def shift(field, radius, Voronoi=False):
        """ Shift by summation method. """
        u_CF, s_CF, w_CF = summation_shift(field, radius,
                                           Voronoi=Voronoi)

        t_ref, u_ref, v_ref = least_square_fit(epochs, u_CF, delta_t)
        t_ref, s_ref, sdot_ref = least_square_fit(epochs, s_CF, delta_t,
                                                  axis=None)
        t_ref, w_ref, omega_ref = least_square_fit(epochs, w_CF, delta_t)

        return t_ref, (u_ref, s_ref, w_ref), (v_ref, sdot_ref, omega_ref)

    return shift


def print_Helmert_parameters(t_ref, inst_params, deriv_params):
    """ Pretty print Helmert parameters. """
    (u_ref, s_ref, w_ref) = inst_params
    (v_ref, sdot_ref, omega_ref) = deriv_params

    print 't (yr)', t_ref
    print 'u (mm)', u_ref * 10
    print 'v (mm/yr)', v_ref * 10
    print 's (ppb)', s_ref
    print 'sdot (ppb/yr)', sdot_ref
    print 'w (μas)', w_ref
    print 'ω (μas/yr)', omega_ref


def Helmert_parameters_for_network(args, stations):
    """
    Print estimated Helmert parameters for the given network.
    """
    numpy.set_printoptions(precision=3, suppress=True)

    # in cm
    radius_cm = earth_radius * 1.e+2

    u, epochs, delta_t = interpolated_displacement_field(args)

    if args.method == 'transformation':
        shift = transformation_shift_factory(epochs, delta_t)
    else:
        shift = summation_shift_factory(epochs, delta_t)

    cartesian_u = numpy.einsum('ij...->ji...',
                               array([u(stations.theta[i], stations.phi[i])
                                      for i in range(stations.shape[0])]))

    field = Field(stations.theta, stations.phi, cartesian_u)

    # (t_ref, u_ref, s_ref, w_ref, v_ref, sdot_ref, omega_ref)
    (t_ref, inst_params,
     deriv_params) = shift(field, radius_cm, Voronoi=args.Voronoi)

    print_Helmert_parameters(t_ref, inst_params, deriv_params)


def ITRF_sites(args):
    """
    Evaluate Helmert parameters for ITRF core networks.
    """
    _, theta, phi = stations_from_table(join(args.stations_folder,
                                             args.stations_file))

    stations = Stations(theta, phi, 2)

    print 'N', stations.theta.shape[0]
    Helmert_parameters_for_network(args, stations)


def SOPAC_sites(args):
    """
    Evaluate Helmert parameters for the SOPAC network.
    """
    records = stations_from_SOPAC(args.sopac_folder)
    IDs = [ID for ID in sorted(records.keys())]
    excluded = []

    # dirty hack to prevent Voronoi decomposition to fail
    theta = numpy.array([records[ID].colatitude
                         for ID in sorted(records.keys())])
    phi = numpy.array([records[ID].longitude
                       for ID in sorted(records.keys())])
    N = len(IDs)
    for i in range(N):
        for j in range(i + 1, N):
            s1 = angular_to_cartesian(theta[i], phi[i])
            s2 = angular_to_cartesian(theta[j], phi[j])

            if numpy.linalg.norm(s1 - s2) < 2.5e-8:
                excluded.append(IDs[j])
    print 'excluded', excluded
    # end hack

    theta = numpy.array([records[ID].colatitude
                         for ID in sorted(records.keys())
                         if ID not in excluded])
    phi = numpy.array([records[ID].longitude
                       for ID in sorted(records.keys())
                       if ID not in excluded])

    stations = Stations(theta, phi, 2)
    print 'N', stations.theta.shape[0]

    Helmert_parameters_for_network(args, stations)


def monte_carlo(args):
    """ Monte Carlo simulation of a network. """
    # generate a random network
    # calculate x_ref, t_ref, v_ref for it
    numpy.set_printoptions(precision=6, suppress=False)

    u, epochs, delta_t = interpolated_displacement_field(args)

    if args.method == 'transformation':
        shift = transformation_shift_factory(epochs, delta_t)
    else:
        shift = summation_shift_factory(epochs, delta_t)

    print '{} {}'.format(args.method,
                         'Voronoi' if args.Voronoi else 'geometric')

    def collect_batch():
        """ Collect one batch of data. """
        # in cm
        radius_cm = earth_radius * 1.e+2

        result = defaultdict(list)

        sampling = UniformAreaSampling(args.network_size,
                                       Voronoi=args.Voronoi)
        for field in sampling.sample(u, radius_cm, args.batch):

            (t_ref, inst_params,
             deriv_params) = shift(field, radius_cm, Voronoi=args.Voronoi)

            result['u'].append(inst_params[0])
            result['s'].append(inst_params[1])
            result['w'].append(inst_params[2])
            result['v'].append(deriv_params[0])
            result['sdot'].append(deriv_params[1])
            result['ω'].append(deriv_params[2])

        return (t_ref, {'u': array(result['u']).T,
                        's': array(result['s']),
                        'w': array(result['w']).T,
                        'v': array(result['v']).T,
                        'sdot': array(result['sdot']),
                        'ω': array(result['ω']).T})

    def join_tally(this, that):
        """ Join two tallies together. """
        result = {}
        for key in this:
            if key in ['s', 'sdot']:
                axis = None
            else:
                axis = 1

            result[key] = numpy.concatenate((this[key], that[key]),
                                            axis=axis)
        return result

    def collect():
        """ Collect samples until convergence.  """
        t_ref, tally = collect_batch()

        seqs = defaultdict(list)

        precisions = {'u': args.u_precision,
                      'w': args.w_precision,
                      'v': args.v_precision,
                      'ω': args.omega_precision,
                      'Δu': args.delta_u_precision,
                      'Δw': args.delta_w_precision,
                      'Δv': args.delta_v_precision,
                      'Δω': args.delta_omega_precision}

        while True:
            t_ref, new_tally = collect_batch()
            tally = join_tally(tally, new_tally)

            for key in ['u', 'w', 'v', 'ω']:
                seqs[key].append(average(tally, key))

            for key in ['u', 'w', 'v', 'ω']:
                seqs['Δ' + key].append(stddev(tally, key))

            conv = {key: converged(seqs[key], precisions[key], args.wait)
                    for key in seqs}

            if all(conv.values()):
                if args.debug:
                    info('\n')
                return t_ref, tally

            elif args.debug:
                def distance(key):
                    """ Distance from convergence. """
                    return converged(seqs[key], precisions[key], args.wait,
                                     return_distance=True)

                culprits = [key for key in conv if not conv[key]]

                msg = "".join(["{}{}".format(key, array([distance(key)]))
                               for key in culprits])

                if len(culprits) == len(conv.keys()):
                    info(msg + '*')
                else:
                    info(msg)

            else:
                info('.')

    def print_stats(t_ref, tally):
        """ Show results. """
        units = {
            'u': '(mm)',
            's': '(ppb)',
            'v': '(mm/yr)',
            'w': '(μas)',
            'sdot': '(ppb/yr)',
            'ω': '(μas/yr)'
        }

        scale = {
            'u': 10,
            's': 1,
            'v': 10,
            'w': 1,
            'sdot': 1,
            'ω': 1
        }

        print 'reference epoch:', t_ref

        for key in ['u', 's', 'w', 'v', 'sdot', 'ω']:
            if key in ['s', 'sdot']:
                axis = None
            else:
                axis = 1
            mean = average(tally, key, axis=axis)
            std = stddev(tally, key, axis=axis)

            print key, 'mean', units[key], mean * scale[key]
            print key, 'std', units[key], array([norm(std)]) * scale[key]

    t_ref, tally = collect()

    print
    print_stats(t_ref, tally)


tasks = {
    'monte-carlo': monte_carlo,
    'helmert': Helmert_parameters,
    'bias-timeseries': bias_timeseries,
    'geocenter': geocenter,
    'scaling': test_scaling,
    'grid': grid,
    'static-grid': static_grid,
    'anomaly-grid2': anomaly_grid2,
    'anomaly-grid': anomaly_grid,
    'timeseries': timeseries,
    'ITRF': ITRF_sites,
    'SOPAC': SOPAC_sites
}


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--love-numbers-file',
                        default='../data/GRGS_files/Love_numbers.dat')
    parser.add_argument('--anomaly-folder',
                        default='../data/GRGS_files')
    parser.add_argument('--grid-folder',
                        default='../data/hydro/GRACE-ocean-grid')
    parser.add_argument('--output-folder',
                        default='../data/hydro/area-grid')
    parser.add_argument('--static-output-folder',
                        default='../data/hydro/static-grid')
    parser.add_argument('--static-grid',
                        default='../data/hydro/static-grid/GGM03C.GEO.npz')
    parser.add_argument('--task', choices=tasks.keys(), required=True)
    parser.add_argument('--Voronoi', action='store_true',
                        help='whether to use Voronoi decomposition')
    parser.add_argument('--method',
                        choices=['transformation', 'summation'],
                        help='which shift function to report',
                        default='summation')
    parser.add_argument('--latitudes', type=int, default=250)
    parser.add_argument('--longitudes', type=int, default=500)
    parser.add_argument('--latitude', type=float)
    parser.add_argument('--longitude', type=float)
    parser.add_argument('--debug', '-d', action='count', default=0,
                        help="emit debug information")
    parser.add_argument('--geocenter-coefficients',
                        default='../data/GRGS_files/ocean-model-geocenter.txt')
    parser.add_argument('--wait', default=50, type=int,
                        help='number of batches to wait before convergence')
    parser.add_argument('--batch', default=100, type=int,
                        help='data points in a batch')
    parser.add_argument('--network-size', '-N', type=int, default=10)
    parser.add_argument('--u-precision', default=0.005, type=float,
                        help='precision of translation')
    parser.add_argument('--delta-u-precision', default=0.05, type=float,
                        help='precision of error in translation')
    parser.add_argument('--w-precision', default=0.5, type=float,
                        help='precision of rotation')
    parser.add_argument('--delta-w-precision', default=5., type=float,
                        help='precision of error in rotation')
    parser.add_argument('--v-precision', default=0.0005, type=float,
                        help='precision of velocity')
    parser.add_argument('--delta-v-precision', default=0.005, type=float,
                        help='precision of error in velocity')
    parser.add_argument('--omega-precision', default=0.05, type=float,
                        help='precision of angular velocity')
    parser.add_argument('--delta-omega-precision', default=0.5, type=float,
                        help='precision of error in angular velocity')
    parser.add_argument('--stations-folder', default='../data/GPS-timeseries')
    parser.add_argument('--stations-file', default=None)
    parser.add_argument('--sopac-folder', default='../data/sopac/raw')
    parser.add_argument('--file-format', default='anomaly')
    parser.add_argument('--from-year', default=2004, type=float)
    parser.add_argument('--to-year', default=2014, type=float)
    parser.add_argument('--static-model',
                        default='../data/RL05/GGM03_Archive/GGM03C.GEO')
    parser.add_argument('--anomaly-output-folder',
                        default='../data/hydro/anomaly-grid')
    parser.add_argument('--anomaly-output-folder2',
                        default='../data/hydro/anomaly-grid2')
    return parser


def main(args):
    """
    Elastic deformation from hydrological loading.
    """
    tasks[args.task](args)


if __name__ == '__main__':
    with care:
        main(argument_parser().parse_args())
