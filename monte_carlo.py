#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Monte Carlo simulation of geodetic networks.
"""

from os.path import join
from argparse import ArgumentParser

import numpy
from numpy import random
from numpy import array
from numpy import pi, arccos, sin, radians
from numpy import concatenate
from numpy.linalg import norm

import summary
from frame import transformation_shift, summation_shift
import voronoi
import basic
from coseismic import surface_displacements
from stations import Stations

from utils import info, care

NO_INTERPOLATION_ANGLE = radians(0.50)


def pdf_theta_for_uniform_area(theta):
    r"""
    Probability distribution function for :math:`\theta`
    for uniform area distribution.
    """
    return sin(theta) / 2.


class ImportanceSampling(object):
    """
    Biased sampling.
    """

    def __init__(self, N, Voronoi=False, excluded_angle=0.,
                 love_numbers=None):
        assert not Voronoi or N >= 4, 'cannot enclose origin'

        self.N = N
        self.Voronoi = Voronoi
        self.excluded_angle = excluded_angle
        self.love_numbers = love_numbers

    def theta(self):
        r"""
        Uniform area distribution by default:
        :math:`sin\theta d\theta`
        """
        _ = self
        return arccos(1 - 2. * random.random())

    def pdf_theta(self, theta):
        """
        Probability density function for :math:`\\theta`.
        """
        _ = self
        return pdf_theta_for_uniform_area(theta)

    def phi(self):
        r"""
        Uniform area distribution by default:
        :math:`d\phi`
        """
        _ = self
        return 2. * pi * random.random()

    def angles(self, trials=1):
        r"""
        Sample points. If Voronoi decomposition is wanted, ensure
        the origin is contained within the convex hull.
        """

        def network():
            """ Generate a random network.  """
            thetas = []
            phis = []

            for _ in range(self.N):
                while True:
                    theta = self.theta()
                    phi = self.phi()
                    if theta >= self.excluded_angle:
                        break

                thetas.append(theta)
                phis.append(phi)

            return numpy.array(thetas), numpy.array(phis)

        def check_voronoi():
            """
            Generate a network that encloses the origin
            in case Voronoi is required.
            """
            if self.Voronoi:
                while True:
                    theta, phi = network()
                    points = basic.angular_to_cartesian(theta, phi)
                    if voronoi.is_origin_inside(points):
                        break
            else:
                theta, phi = network()

            return theta, phi

        # generate trials
        thetas = []
        phis = []
        for _ in range(trials):
            theta, phi = check_voronoi()
            thetas.append(theta)
            phis.append(phi)

        return numpy.concatenate(thetas), numpy.concatenate(phis)

    def sample_scalar(self, u):
        """
        Sample a scalar function for debugging purposes.
        """
        theta, phi = self.angles()
        N = self.N

        area = pdf_theta_for_uniform_area(theta)
        rescale = self.pdf_theta(theta)

        return {'theta': theta,
                'phi': phi,
                'values': array([u(theta[i], phi[i]) for i in range(N)]),
                'weights': area / rescale}

    def evaluate(self, theta, phi, radius):
        r"""
        Without interpolation, evaluate the field again.
        """
        spheroidal, toroidal = self.love_numbers
        max_l = max([l for l, _ in spheroidal])
        stations = Stations(theta, phi, max_l)
        u = surface_displacements(spheroidal, toroidal, stations) * radius
        return basic.spherical_to_cartesian(theta, phi, u)

    def sample(self, u, radius, trials):
        r"""
        :math:`\mathbf{u}(\theta, \phi)`.
        """
        theta, phi = self.angles(trials)
        cartesian_u = numpy.einsum('ij...->ji...',
                                   array([u(theta[i], phi[i])
                                          for i in range(self.N * trials)]))

        if self.love_numbers is not None:
            # co-seismic case, be careful around the epicenter
            mask = theta < NO_INTERPOLATION_ANGLE
            if theta[mask].shape[0] != 0:
                cartesian_u[:, mask] = self.evaluate(theta[mask], phi[mask],
                                                     radius)

        for i in range(trials):
            batch = slice(i * self.N, (i + 1) * self.N)
            yield summary.Field(theta[batch], phi[batch],
                                cartesian_u[:, batch])

    def weights(self, field):
        """
        Default weight is a constant.
        """
        theta = field.theta

        area = pdf_theta_for_uniform_area(theta)
        rescale = self.pdf_theta(theta)

        return numpy.prod(area / rescale)

    def collect(self, trials, u, shift_function, radius_in_cm):
        """
        Collect transformation parameters and weights into a dictionary.
        """
        Ts = []
        Ds = []
        Rs = []
        Ws = []

        for field in self.sample(u, radius_in_cm, trials):
            T, D, R = shift_function(field, radius_in_cm, Voronoi=self.Voronoi)
            W = self.weights(field)

            Ts.append(T)
            Ds.append(D)
            Rs.append(R)
            Ws.append(W)

        return {
            'T': array(Ts).T,
            'D': array(Ds),
            'R': array(Rs).T,
            'W': array(Ws)
        }


class UniformAreaSampling(ImportanceSampling):
    """
    The default is uniform area sampling.
    """
    pass


class UniformAngleSampling(ImportanceSampling):
    """
    Biased sampling that is uniform for :math:`\\theta`
    so samples area around epicenter more.
    """
    def theta(self):
        """
        Draw values of :math:`\\theta` from
        a uniform distribution.
        """
        return pi * random.random()

    def pdf_theta(self, theta):
        """
        Probability distribution that is uniform
        for :math:`\\theta`.
        """
        _, _ = self, theta
        return 1. / pi


def average(tally, key, axis=None):
    """ Mean of the tally. """
    return (tally[key] * tally['W']).mean(axis=axis)


def stddev(tally, key, axis=None):
    """ Standard deviation of the tally. """
    mean = average(tally, key, axis=axis)
    if axis is None:
        diff = (tally[key] - mean) ** 2
    else:
        diff = (tally[key].T - mean).T ** 2
    variance = (diff * tally['W']).mean(axis=axis)
    return numpy.sqrt(variance)


def print_stats(tally):
    """
    Print mean, stardard deviation and covariance.
    """
    T_std = stddev(tally, 'T', axis=1)
    D_std = stddev(tally, 'D', axis=None)
    R_std = stddev(tally, 'R', axis=1)

    print ' T mean (mm)', average(tally, 'T', axis=1) * 10
    print ' T  std (mm)', T_std * 10, '({})'.format(norm(T_std * 10))
    print 'D mean (ppb)', average(tally, 'D', axis=None)
    print 'D  std (ppb)', D_std, '({})'.format(norm(D_std))
    print 'R mean (μas)', average(tally, 'R', axis=1)
    print 'R  std (μas)', R_std, '({})'.format(norm(R_std))


def converged(seq, precision, wait, return_distance=False):
    """
    Decide whether the sequence *seq* is converging within *precision*.
    Checks if the last *wait* values were acceptable.
    """
    last = seq[-1]

    try:
        for j in range(wait):
            distance = norm(last - seq[j - wait])
            if distance > precision:
                if return_distance:
                    return distance
                else:
                    return False

        return True
    except IndexError:
        return False


def join_tally(this, that):
    """
    Join two tallies together.
    """
    return {'T': concatenate((this['T'], that['T']), axis=1),
            'D': concatenate((this['D'], that['D'])),
            'R': concatenate((this['R'], that['R']), axis=1),
            'W': concatenate((this['W'], that['W']))}


def collect(sampling, u, shift_function, radius, args):
    """ Collect samples until convergence.  """
    tally = sampling.collect(args.batch, u, shift_function, radius)
    seqs = {'T': [], 'R': [], 'ΔT': [], 'ΔR': []}
    precisions = {'T': args.T_precision,
                  'R': args.R_precision,
                  'ΔT': args.delta_T_precision,
                  'ΔR': args.delta_R_precision}

    while True:
        try:
            new = sampling.collect(args.batch, u, shift_function, radius)
        except FloatingPointError:
            info('x')
            continue

        tally = join_tally(tally, new)

        seqs['T'].append(average(tally, 'T'))
        seqs['ΔT'].append(stddev(tally, 'T'))
        seqs['R'].append(average(tally, 'R'))
        seqs['ΔR'].append(stddev(tally, 'R'))

        conv = {key: converged(seqs[key], precisions[key], args.wait)
                for key in seqs}

        if all(conv.values()):
            info('\n')
            return tally
        elif args.debug:
            def distance(key):
                """ Distance from convergence. """
                return converged(seqs[key], precisions[key], args.wait,
                                 return_distance=True)
            culprits = [key for key in conv if not conv[key]]

            if len(culprits) == len(conv.keys()):
                info("".join(["{}{}".format(key, array([distance(key)]))
                              for key in culprits]))
                info('*')
            else:

                info("".join(["{}{}".format(key, array([distance(key)]))
                              for key in culprits]))
        else:
            info('.')


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--N', default=16, type=int,
                        help='network size')
    parser.add_argument('--wait', default=50, type=int,
                        help='number of batches to wait before convergence')
    parser.add_argument('--T-precision', default=0.005, type=float,
                        help='precision of translation')
    parser.add_argument('--delta-T-precision', default=0.5, type=float,
                        help='precision of error in translation')
    parser.add_argument('--R-precision', default=0.5, type=float,
                        help='precision of rotation')
    parser.add_argument('--delta-R-precision', default=50., type=float,
                        help='precision of rotation')
    parser.add_argument('--batch', default=1000, type=int,
                        help='data points in a batch')
    parser.add_argument('--data-root', default='../data',
                        help='where data is kept')
    parser.add_argument('--data-set', default='bigger',
                        help='data grouped together')
    parser.add_argument('--name', default='dc-1/10',
                        help='name of data folder')
    parser.add_argument('--task',
                        choices=['transformation', 'summation'],
                        help='which shift function to report',
                        default='summation')
    parser.add_argument('--Voronoi', action='store_true',
                        help='whether to use Voronoi decomposition')
    parser.add_argument('--excluded-angle', '-x', default=0., type=float,
                        help='angle (in degrees) to exclude around epicenter')
    parser.add_argument('--love-numbers-file', default='love.pickle')
    parser.add_argument('--debug', '-d', action='store_true',
                        help='enable debug info')
    return parser


def main(args):
    """
    Monte Carlo simulation of shifts in Helmert parameters
    by different methods.
    """
    numpy.set_printoptions(precision=3, suppress=False)

    folder = join(args.data_root, args.data_set, args.name)
    print 'in folder', folder
    options = summary.read_options(folder)

    radius_in_cm = options['model']['radius'] * 1.e+5    # in cm
    field = summary.binary_to_field(folder)
    u = field.interpolate(options)

    print 'origin (mm)', [x * 10. for x in options['shift']['origin']]
    print '  EOP (μas)', options['shift']['EOP']

    love_numbers = summary.read_love_numbers(folder,
                                             args.love_numbers_file)

    excluded_angle = numpy.radians(args.excluded_angle)
    sampling = UniformAngleSampling(args.N, Voronoi=args.Voronoi,
                                    excluded_angle=excluded_angle,
                                    love_numbers=love_numbers)

    if args.task == 'transformation':
        shift = transformation_shift
    else:
        shift = summation_shift

    print '{} {} x={}'.format(args.task,
                              'Voronoi' if args.Voronoi else 'geometric',
                              args.excluded_angle)

    print_stats(collect(sampling, u, shift, radius_in_cm, args))


if __name__ == '__main__':
    command_line_parser = argument_parser()
    command_line_args = command_line_parser.parse_args()
    with care:
        main(command_line_args)
