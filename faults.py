"""
Seismic sources for both shear and tensile faults.
"""
import numpy
from numpy import sin, cos, pi

from basic import angular_to_cartesian, spherical_to_cartesian
from basic import cartesian_to_angular, normalize
from utils import Record, lazy

# expected resolution of the splitting of an extended fault
# for the purposes of numerical integration
# in the vertical direction as a fraction of earth radius
# about 6.4 km
VERTICAL_RESOLUTION = 0.0005

# expected resolution of the splitting of an extended fault
# for the purposes of numerical integration
# in the horizontal direction as a fraction of earth radius
# about 6.4 km
HORIZONTAL_RESOLUTION = 0.0005


def read_segment(earth, common_line, segment_line):
    """
    Reads the description of a fault from lines read from Pollitz's
    fault files.
    """
    # depth_max, depth_min are in km
    # dip is in degrees
    depth_max, depth_min, dip = common_line.split()

    # convert to the internal "scaled" system
    radius_min = 1 - float(depth_max) / earth.radius
    radius_max = 1 - float(depth_min) / earth.radius
    dip = numpy.radians(float(dip))

    (latitude, longitude,
     length, strike, slip, displacement) = segment_line.split()

    # latitude, longitude, strike, slip are in degrees

    # the location of the lower edge of the fault plane closest
    # to the strike direction according to Pollitz's convention
    colatitude = numpy.radians(90. - float(latitude))
    longitude = numpy.radians(float(longitude))

    slip = numpy.radians(float(slip))
    strike = numpy.radians(float(strike))

    # convert to the internal "scaled" system
    # length is in km, displacement is in cm
    length = float(length) / earth.radius
    displacement = (float(displacement) / (100. * 1000.)) / earth.radius

    # Pollitz's convention
    if slip >= -pi and slip <= pi:
        return ShearFaultSegment(colatitude=colatitude,
                                 longitude=longitude,
                                 radius_min=radius_min,
                                 radius_max=radius_max,
                                 length=length,
                                 strike=strike,
                                 slip=slip,
                                 dip=dip,
                                 displacement=displacement,
                                 earth=earth)
    else:
        return TensileFaultSegment(colatitude=colatitude,
                                   longitude=longitude,
                                   radius_min=radius_min,
                                   radius_max=radius_max,
                                   length=length,
                                   strike=strike,
                                   sign=numpy.sign(slip),
                                   dip=dip,
                                   displacement=displacement,
                                   earth=earth)


class FaultSegment(Record):
    """
    Description of a fault segment.
    """
    # list of known fields
    _fields = ['colatitude', 'longitude', 'radius_min', 'radius_max',
               'length', 'strike', 'dip', 'displacement', 'earth']

    def __init__(self, **properties):
        r"""
        Creates a fault segment object from the given
        dictionary of properties.
        Its properties should include its colatitude and longitude
        :math:`\theta` and :math:`\phi`, dip :math:`\delta`,
        strike :math:`\phi_f`, minimum and maximum radii,
        length, displacement :math:`\Delta u`, and also,
        for a shear fault, slip :math:`\lambda`, or,
        for a tensile fault,
        the sign of the displacement with respect
        to the fault normal.
        """
        super(FaultSegment, self).__init__(**properties)

        assert self.dip >= 0. and self.dip <= pi / 2.
        assert self.radius_min <= self.radius_max

    @lazy
    def height(self):
        """
        Vertical difference in radii of the two horizontal edges of the fault.
        """
        return self.radius_max - self.radius_min

    @lazy
    def source_layer(self):
        """
        The layer that contains the fault.
        Needed to extract physical properties at the source.
        """
        return self.earth.find_layer(self.radius)

    @lazy
    def mu(self):
        """ Shear modulus mu at the fault. """
        return self.source_layer.mu

    @lazy
    def lamda(self):
        """ Lame parameter lambda at the fault. """
        return self.source_layer.lamda

    @lazy
    def width(self):
        """ Width of the fault. """
        return self.height / sin(self.dip)

    @lazy
    def radius(self):
        """
        The distance from the center of the earth to the middle of the fault.
        """
        return (self.radius_max + self.radius_min) / 2.

    @lazy
    def n(self):
        """
        Unit vector normal to the fault plane in spherical coordinates.
        """
        strike, dip = self.strike, self.dip

        return numpy.array([cos(dip),
                            sin(dip) * sin(strike),
                            sin(dip) * cos(strike)])

    def d(self, slip):
        """
        Unit vector lying on the fault plane at the given slip angle
        expressed in spherical coordinates.
        """
        strike, dip = self.strike, self.dip

        d_r = sin(slip) * sin(dip)
        d_theta = -cos(slip) * cos(strike) - sin(slip) * cos(dip) * sin(strike)
        d_phi = cos(slip) * sin(strike) - sin(slip) * cos(dip) * cos(strike)

        return numpy.array([d_r, d_theta, d_phi])

    @lazy
    def A(self):
        """
        Area of the fault as a vector in spherical coordinates.
        """
        return self.length * self.width * self.n

    @lazy
    def M(self):
        """ The moment tensor in spherical coordinates. """
        u, A = self.u, self.A

        return (self.lamda * numpy.eye(3) * u.dot(A) +
                self.mu * (numpy.outer(u, A) + numpy.outer(A, u)))

    @lazy
    def cartesian_location(self):
        """ Cartesian coordinate of the fault location. """
        return self.radius_min * angular_to_cartesian(self.colatitude,
                                                      self.longitude)

    @lazy
    def vertical_divisions(self):
        """
        Expected number of vertical divisions
        to achieve the required vertical resolution.
        """
        return int(numpy.ceil(self.height / VERTICAL_RESOLUTION))

    @lazy
    def horizontal_divisions(self):
        """
        Expected number of horizontal divisions
        to achieve the required horizontal resolution.
        """
        return int(numpy.ceil(self.length / HORIZONTAL_RESOLUTION))

    def split_vertically(self, N=None):
        """
        Splits the fault vertically into *N* smaller faults.
        Used for numerical integration.
        The moment tensor of the fault is distributed among the pieces.
        If *N* is :code:`None`, uses predefined resolution for the splitting.
        """
        if N is None:
            N = self.vertical_divisions

        dr = self.height / N
        dw = self.width / N
        corner = self.cartesian_location

        children = []
        for i in range(N):
            child = self.clone()

            d_cartesian = spherical_to_cartesian(self.colatitude,
                                                 self.longitude,
                                                 self.d(pi / 2.))
            location = normalize(corner + i * dw * d_cartesian)
            child.colatitude, child.longitude = cartesian_to_angular(location)

            child.radius_min = self.radius_min + i * dr
            child.radius_max = self.radius_min + (i + 1) * dr

            children.append(child)

        return children

    def split_horizontally(self, N=None):
        """
        Splits the fault vertically into *N* smaller faults.
        Used for numerical integration.
        The moment tensor of the fault is distributed among the pieces.
        If *N* is :code:`None`, uses predefined resolution for the splitting.
        """
        if N is None:
            N = self.horizontal_divisions

        dl = self.length / N
        corner = self.cartesian_location

        children = []
        for i in range(N):
            child = self.clone()

            d_cartesian = spherical_to_cartesian(self.colatitude,
                                                 self.longitude,
                                                 -self.d(0.))
            location = normalize(corner + i * dl * d_cartesian)
            child.colatitude, child.longitude = cartesian_to_angular(location)

            child.length = dl

            children.append(child)

        return children


class ShearFaultSegment(FaultSegment):
    """
    Description of a shear fault segment.
    """
    # list of known fields
    _fields = FaultSegment._fields + ['slip']

    def __init__(self, **properties):
        """
        Creates a shear fault segment object from the given
        dictionary of properties.
        """
        super(ShearFaultSegment, self).__init__(**properties)

        assert self.slip >= -pi and self.slip <= pi

    @lazy
    def u(self):
        """
        Displacement vector of the fault in spherical coordinates.
        """
        return self.displacement * self.d(self.slip)


class TensileFaultSegment(FaultSegment):
    """
    Description of a tensile fault segment.
    """
    _fields = FaultSegment._fields + ['sign']

    def __init__(self, **properties):
        """
        Creates a tensile fault segment object from the given
        dictionary of properties.
        """
        super(TensileFaultSegment, self).__init__(**properties)

    @lazy
    def u(self):
        """
        Displacement vector of the fault in spherical coordinates.
        """
        return self.sign * self.displacement * self.n


def discretize(faults):
    """
    Break down extended faults into a collection of point-like sources.
    The result is a list containing lists of fault segments. All the segments
    in a member of this list share the seismic source description in
    epicenter coordinates, that is, the moment tensor and the radius.
    Thus the Green's functions need only be evaluated for them.

    :arg faults: list of extended fault segments
    """
    return [horizontal_piece.split_horizontally()
            for fault in faults
            for horizontal_piece in fault.split_vertically()]
