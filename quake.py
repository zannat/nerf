#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
"""
Program to calculate surface coseismic and postseismic displacement fields.
"""
import os.path
from glob import glob
from argparse import ArgumentParser
import cPickle as pickle

from numpy import radians, degrees
import numpy

import coseismic
import postseismic

from basic import cutoff_max_l, cutoff_l
from basic import angular_to_cartesian
from utils import care, info, write_fortran, autocomplete
from utils import Record, lazy
from earth import read_model
from faults import read_segment, discretize
from earth import year, hour
from residue import toroidal_roots, spheroidal_roots
from stations import Stations
from earth import mass_of_earth

# maximum spherical harmonic degree processed by the program
MAX_L = 20000


def read_fault_segments(fl, earth):
    """
    Reads the segments of a fault from a file object.
    """
    def read_common_line():
        """
        Reads parameters common to all faults.
        Also, detects file format.
        """
        common_line = fl.readline()

        try:
            # if successful, the first line was not a comment
            dep_min, dep_max, dip = common_line.split()
            dep_min = float(dep_min)
            dep_max = float(dep_max)
            dip = float(dip)

            file_format = 'coseismic'
        except ValueError:
            # if not, ignore the already read first comment line
            common_line = fl.readline()
            file_format = 'postseismic'

        return common_line, file_format

    common_line, file_format = read_common_line()

    def to_hour(time_in_year):
        """
        Converts from years to hours.
        Times in fault files are in years but this programs uses
        hours as its internal time unit.
        """
        return time_in_year * year / hour

    def read_epochs():
        """ Reads time specification. """
        time_line = fl.readline()
        earthquake_time, start_time, end_time, _ = time_line.split()
        earthquake_time = float(earthquake_time)
        start_time = to_hour(float(start_time) - earthquake_time)
        end_time = to_hour(float(end_time) - earthquake_time)

        return numpy.array([start_time, end_time])

    if file_format == 'postseismic':
        epochs = read_epochs()
    else:
        # placeholder time information
        epochs = numpy.array([0.])

    def read_segments():
        """ Reads fault segments. """
        count = int(fl.readline())

        return [read_segment(earth, common_line, fl.readline())
                for _ in range(count)]

    segments = read_segments()

    return segments, epochs


def read_locations(fl):
    """
    Reads locations of stations, that is, observation points,
    from a file object.
    """
    count = int(fl.readline())

    colatitudes = numpy.zeros(count)
    longitudes = numpy.zeros(count)

    for i in range(count):
        latitude, longitude = fl.readline().split()

        colatitudes[i] = radians(90 - float(latitude))
        longitudes[i] = radians(float(longitude))

    return colatitudes, longitudes


def read_fault_and_location(fault_file, earth):
    """
    Reads fault information in either coseismic (``cograv``) or
    postseismic (``visco1d``) formats.
    Additionally, reads the required observation points.
    """
    with open(fault_file) as fl:
        segments, epochs = read_fault_segments(fl, earth)

        colatitudes, longitudes = read_locations(fl)

    # create an object with the responsibility
    # to evaluate the relevant mathematical functions as needed
    # at the given observation points in space and time
    return segments, colatitudes, longitudes, epochs


def read_faults_and_locations(wanted_fault, earth):
    """
    Reads a collection of fault files.
    These files should contain the same set of observation points.
    """
    # fault descriptions are kept in the faults/ directory
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'faults')

    if wanted_fault.endswith('*'):
        # a collection of faults matching a wildcard pattern
        fault_file_list = [os.path.basename(f)
                           for f in glob(os.path.join(path, wanted_fault))]

    else:
        # a single fault
        fault_file_list = [wanted_fault]

    # join the collections of segments
    faults = []
    for fault in fault_file_list:
        fault_file = os.path.join(path, fault)

        (segments, colatitudes,
         longitudes, epochs) = read_fault_and_location(fault_file, earth)

        faults += segments

    # assumption: all the observation points in the files are same
    return faults, colatitudes, longitudes, epochs


def read_faults(wanted_fault, earth):
    """
    Reads a collection of fault description files.
    """
    # fault descriptions are kept in the faults/ directory
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'faults')

    if wanted_fault.endswith('*'):
        # a collection of faults matching a wildcard pattern
        fault_file_list = [os.path.basename(f)
                           for f in glob(os.path.join(path, wanted_fault))]

    else:
        # a single fault
        fault_file_list = [wanted_fault]

    # join the collections of segments
    faults = []
    for fault in fault_file_list:
        fault_file = os.path.join(path, fault)

        with open(fault_file) as fl:
            (segments, epochs) = read_fault_segments(fl, earth)

        faults += segments

    return faults, epochs


def read_faults_and_read_locations(wanted_fault, stations_file, earth):
    """
    Reads a collection of fault description files
    and a separate stations files.
    """
    # fault descriptions are kept in the faults/ directory
    path = os.path.join(os.path.dirname(os.path.realpath(__file__)),
                        'faults')

    faults, epochs = read_faults(wanted_fault, earth)

    with open(os.path.join(path, stations_file)) as fl:
        colatitudes, longitudes = read_locations(fl)

    # assumption: all the observation points in the files are same
    return faults, colatitudes, longitudes, epochs


def normalize_faults(faults, point_like):
    """
    Convert the list of faults to a list of lists of point-like sources.
    All the sources in a member list share the moment tensor and the depth.
    If *point_like* is true, then the sources are already point-like
    and thus need not be broken down.
    """
    if point_like:
        return [[fault] for fault in faults]
    else:
        return discretize(faults)


def point_source(fault, modes, post_earth, stations, epochs):
    """
    Calculates the contributions to the displacement field
    from a point source in epicentral coordinates and
    in units of earth radius.

    :arg fault: the point source
    :arg co_modes: precalculated coseismic mode data
    :arg post_modes: precalculated postseismic mode data
    :arg post_earth: earth model with layer boundaries at the fault radius
                     and at regular intervals suited for numerical integration
    :arg stations: observation points, that is, stations, in space and time
                   in epicentral coordinates
    """
    co_modes, post_modes = modes
    # indices: [axis, point]
    spherical_u = numpy.zeros((3,) + stations.shape)

    if co_modes is not None:
        spheroidal, toroidal = co_modes

        # calculate static displacement field in epicentral coordinates
        spherical_u += coseismic.surface_displacements(spheroidal, toroidal,
                                                       stations)

    if post_modes is not None:
        spheroidal, toroidal = post_modes

        # calculate postseismic displacement field in epicentral coordinates
        u_scaled = postseismic.surface_displacements((spheroidal, toroidal),
                                                     fault, post_earth,
                                                     stations, epochs)

        # the cumulative displacement field between the two given epochs
        spherical_u += u_scaled[:, :, 1] - u_scaled[:, :, 0]

    return spherical_u


def static_modes(earth, fault, max_l, homogeneous_solutions):
    """
    Returns a pair of dictionaries containing
    coseismic spheroidal and toroidal mode data.
    """
    M = fault.M
    radius = fault.radius
    max_l = cutoff_max_l(max_l, radius)
    spheroidal_homo, toroidal_homo = homogeneous_solutions

    spheroidal = coseismic.spheroidal_modes(earth, radius, M, spheroidal_homo)
    toroidal = coseismic.toroidal_modes(earth, radius, M, toroidal_homo)
    return (spheroidal, toroidal)


def residual_modes(roots, rheology, earth):
    """
    Returns a pair of dictionaries containing
    postseismic spheroidal and toroidal residual mode data.
    """
    sphero_roots, toro_roots = roots

    spheroidal = postseismic.spheroidal_modes(sphero_roots, earth, rheology)
    toroidal = postseismic.toroidal_modes(toro_roots, earth, rheology)

    return (spheroidal, toroidal)


def put_sources(earth, faults):
    """
    Creates layer boundaries at the radii of the given faults.
    Guarantees that the radial solution of the equation of motion
    will be sampled at the required source radii.
    """
    radii = [fault.radius
             for horizontal_piece in faults
             for fault in horizontal_piece]

    for radius in sorted(list(set(radii))):
        earth = earth.put_source(radius)

    return earth


def characteristic_function_roots(earth, rheology, min_l, max_l):
    """
    Precalculate the roots of the characteristic functions.
    These are the :math:`s`-values where the residues are to
    be calculated.
    """
    toro_roots = toroidal_roots(earth, rheology, min_l, max_l)
    sphero_roots = spheroidal_roots(earth, rheology, min_l, max_l)

    print 'toroidal roots'
    for l, lroots in toro_roots:
        for root in reversed(lroots):
            print write_fortran('i4,d17.10', l, -(root / 3.6e-5))
        print len(lroots), 'found'
    print 'spheroidal roots'
    for l, lroots in sphero_roots:
        for root in reversed(lroots):
            print write_fortran('i4,d17.10', l, -(root / 3.6e-5))
        print len(lroots), 'found'

    return sphero_roots, toro_roots


def detect_rheology(earth, rheology, tasks):
    """
    Detect appropriate rheology for a given earth model.
    """
    if rheology != 'detect':
        return rheology

    result = None

    for layer in earth.layers:
        if hasattr(layer, 'eta'):
            result = 'Maxwell'
            break

    for layer in earth.layers:
        if hasattr(layer, 'eta1'):
            result = 'Burgers'
            break

    if result is None and 'postseismic' in tasks:
        raise ValueError("is the earth model viscoelastic?")

    return result


def write_displacement_field(output_file, stations, spherical_u):
    """
    Write out a displacement field to a file.
    """
    with open(output_file, 'w') as fl:
        for i in range(stations.shape[0]):
            latitude = 90 - degrees(stations.theta[i])
            longitude = degrees(stations.phi[i])
            v = spherical_u[:, i]

            # east, north, up components
            fl.write("{:.3f}\t{:.3f}\t{:.4e}\t{:.4e}\t{:.4e}\n"
                     .format(latitude, longitude, v[2], -v[1], v[0]))


class Earthquake(Record):
    """
    Coordinate the task of evaluating the displacement field.
    """
    # list of known fields
    _fields = ['tasks', 'earth', 'faults',
               'min_l', 'max_l', 'rheology']

    # list of optional fields
    _optional_fields = ['debug', 'love_numbers_file']

    def debugging(self):
        """
        Whether to show optional debugging information.
        """
        if not hasattr(self, 'debug'):
            return False
        return self.debug

    @lazy
    def roots(self):
        """
        Roots of the characteristic function for each :math:`l` mode.
        """
        if self.debugging():
            info("calculating roots\n")
        return characteristic_function_roots(self.earth, self.rheology,
                                             self.min_l, self.max_l)

    @lazy
    def co_earth(self):
        """
        Layered earth structure without core
        to use in coseismic calculations.
        """
        # put layer boundaries at seismic sources
        return put_sources(self.earth, self.faults)

    @lazy
    def post_earth(self):
        """
        Layered earth structure with core added
        to use in postseismic calculations.
        """
        # the core layer as well as finer sampling of radial solution
        # are needed to calculate the normalizing factor epsilon
        # by performing a discrete integration over the inside of the earth
        return self.co_earth.with_interior(self.max_l)

    @lazy
    def post_modes(self):
        """
        Postseismic residual modes.
        That is, solutions of the homogeneous equation of motion
        for each :math:`l` mode and each of its characteristic roots.
        """
        # since the residual mode data do not depend on moment tensor M
        # they can be calculated beforehand
        # once layer boundaries at seismic source points are in place
        if self.debugging():
            info("calculating postseismic residual modes\n")
        return residual_modes(self.roots, self.rheology, self.post_earth)

    @lazy
    def homogeneous_co_modes(self):
        """
        Homogeneous solutions of the equation of motion
        for coseismic modes.
        """
        spheroidal = coseismic.homogeneous_spheroidal_modes(self.co_earth,
                                                            self.min_l,
                                                            self.max_l)
        toroidal = coseismic.homogeneous_toroidal_modes(self.co_earth,
                                                        self.min_l,
                                                        self.max_l)
        return spheroidal, toroidal

    @lazy
    def co_modes(self):
        """
        Coseismic static modes.
        That is, solutions of the equation of motion at the surface
        for each :math:`l` mode.
        The result is a list with one element for each group of
        point-like sources that share the source radius and the moment.
        """
        def modes(fault):
            """
            Modes for one point-like fault segment.
            """
            return static_modes(self.co_earth, fault, self.max_l,
                                self.homogeneous_co_modes)

        # any one of the horizontal pieces will do
        return [modes(horizontal_piece[0]) for horizontal_piece in self.faults]

    def surface_displacement_field(self, stations, epochs):
        """
        Evaluate the displacement field for a set of *stations*.
        """
        # indices: [axis, point]
        spherical_u = numpy.zeros((3,) + stations.shape)

        if 'postseismic' in self.tasks:
            post_modes = self.post_modes
        else:
            post_modes = None

        # do not know if we will ever need to save post-seismic modes
        if self.love_numbers_file:
            try:
                with open(self.love_numbers_file) as fl:
                    self.co_modes = pickle.load(fl)
            except IOError:
                with open(self.love_numbers_file, "w") as fl:
                    pickle.dump(self.co_modes, fl)

        # the strategy is to do a segment sum
        # the finite extended fault is split into smaller segments
        # that can be treated as point sources
        # whose contributions are then summed
        for co_mode_index, horizontal_piece in enumerate(self.faults):
            fault = horizontal_piece[0]
            if self.debugging():
                info("source approximately at")
                info("latitude {:.3f}, longitude {:.3f}\n",
                     90 - degrees(fault.colatitude), degrees(fault.longitude))
                info("in radians, colatitude {:.6f}, longitude {:.6f}\n",
                     fault.colatitude, fault.longitude)
                info("moment tensor {} x\n".format(len(horizontal_piece)))
                info("{}", fault.M)
                info("at radius {:.6f}\n", fault.radius)

            if 'coseismic' in self.tasks:
                co_modes = self.co_modes[co_mode_index]
            else:
                co_modes = None

            for fault in horizontal_piece:
                spherical_u += self.point_source(fault,
                                                 (co_modes, post_modes),
                                                 stations, epochs)
                if self.debugging():
                    info(".")

            if self.debugging():
                info("\n")

        return spherical_u

    def point_source(self, fault, modes, stations, epochs):
        """
        Evaluate contributions to the surface displacement field
        by a point source in cm.
        """
        co_modes, post_modes = modes
        # earth radius in cm
        radius = self.earth.radius * 1.e+5

        # station locations in epicentral coordinates
        epicentral = stations.to_epicentral(fault)

        # indices: [axis, point]
        spherical_u = numpy.zeros((3,) + epicentral.shape)

        # process stations in batches
        for batch, stations_batch in epicentral.batches:
            spherical_u[:, batch] += point_source(fault,
                                                  (co_modes, post_modes),
                                                  self.post_earth,
                                                  stations_batch, epochs)

        # transform it back to geographical coordinates
        return epicentral.to_geographical(spherical_u) * radius

    @lazy
    def epicenters(self):
        """
        List of cartesian coordinates of earthquake epicenters.
        """
        return [angular_to_cartesian(fault.colatitude,
                                     fault.longitude)
                for horizontal_piece in self.faults
                for fault in horizontal_piece]


def setup_earthquake(args, model, faults):
    """
    Creates an instance of :class:`Earthquake` according to the
    specifications in *args*.
    """
    # decide which calculations are to be performed
    if args.task == 'co' or args.task == 'coseismic':
        tasks = ['coseismic']
    elif args.task == 'post' or args.task == 'postseismic':
        tasks = ['postseismic']
    else:
        tasks = ['coseismic', 'postseismic']

    radii = [fault.radius
             for horizontal_piece in faults
             for fault in horizontal_piece]

    required_l = numpy.max([cutoff_l(radius) for radius in radii])

    if args.max_l == -1:
        max_l = required_l
    else:
        assert args.min_l <= args.max_l
        max_l = numpy.min([args.max_l, required_l])

    max_l = numpy.min([max_l, MAX_L])

    earth = model.unique_layers().prevent_matrizant_overflow(max_l)

    try:
        if hasattr(args, 'output') and hasattr(args, 'name'):
            love_numbers_file = os.path.join(os.path.expanduser(args.output),
                                             args.name,
                                             args.love_numbers_file[:])
        else:
            love_numbers_file = args.love_numbers_file

    except (NameError, TypeError, AttributeError):
        love_numbers_file = None

    if hasattr(args, 'debug'):
        debug = args.debug
    else:
        debug = 0

    if debug:
        info("calculating for range {} to {} for l\n".format(args.min_l,
                                                             max_l))

    rheology = detect_rheology(earth, args.rheology, tasks)
    if debug:
        info("detected {} rheology\n".format(rheology))

    return Earthquake(tasks=tasks,
                      earth=earth,
                      faults=faults,
                      min_l=args.min_l,
                      max_l=max_l,
                      rheology=rheology,
                      debug=debug,
                      love_numbers_file=love_numbers_file)


def add_common_options(parser):
    """
    Adds options common to scripts to the argument parser.
    """
    task_choices = ['co', 'coseismic', 'post', 'postseismic',
                    'both', 'combined']
    task_help = """
    whether to calculate coseismic, postseismic or combined displacements
    (one of [""" + ", ".join(str(task) for task in task_choices) + """];
    default: both)
    """
    parser.add_argument('--task', '-t', type=str, default='coseismic',
                        metavar='TASK', choices=task_choices, help=task_help)

    parser.add_argument('--min-l', type=int, default=0, help="minimum l mode")
    parser.add_argument('--max-l', '-l', type=int, default=-1,
                        help="maximum l mode (default: -1 for automatic)")

    rheology_choices = ['detect', 'Maxwell', 'Burgers']
    rheology_help = """
    rheology model of the earth for postseismic relaxation
    (one of [""" + ", ".join(str(rheo) for rheo in rheology_choices) + """];
    default: detect)
    """
    parser.add_argument('--rheology', '-r', type=str, default='detect',
                        metavar='RHEOLOGY',
                        choices=rheology_choices,
                        help=rheology_help)

    parser.add_argument('--model', default='earth.model',
                        help="the earth model file")

    parser.add_argument('--love-numbers-file', type=str,
                        help=("write love numbers to the given file"
                              " (default: None)"))


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    point_source_help = """
    whether to treat as a point source
    (default: integrate over an extended fault)
    """

    parser = ArgumentParser(description=__doc__)
    add_common_options(parser)

    parser.add_argument('--fault', '-f', type=str, default='strainx.inTHRUST',
                        help="single fault file or glob expression")
    parser.add_argument('--point-source', '-p', action='store_true',
                        help=point_source_help)
    parser.add_argument('--output-file', '-o', default='log.quake.log',
                        type=str, metavar='OUT',
                        help="file to write the displacement fields to")
    parser.add_argument('--debug', '-d', action='count', default=0,
                        help='emit debug information')
    parser.add_argument('--separate-inputs', '-S', action='store_true',
                        help="read faults and stations from separate files")
    parser.add_argument('--stations', '-s', type=str,
                        default='Sumatra.stations',
                        help="stations list file")
    return parser


def main(args):
    """
    Run ``cograv``, ``visco1d`` or both.
    """
    # read earth model, fault description and observation points
    model = read_model(model_file=args.model)

    if args.separate_inputs:
        (faults, colatitudes,
         longitudes, epochs) = read_faults_and_read_locations(args.fault,
                                                              args.stations,
                                                              model)
    else:
        (faults, colatitudes,
         longitudes, epochs) = read_faults_and_locations(args.fault,
                                                         model)

    info("earth model\n{!s}\n", model.unique_layers())
    info("with mass: {}\n", mass_of_earth(model))

    # set up the coordinator of tasks
    earthquake = setup_earthquake(args, model,
                                  normalize_faults(faults, args.point_source))
    stations = Stations(colatitudes, longitudes, earthquake.max_l)

    field = earthquake.surface_displacement_field(stations, epochs)
    write_displacement_field(args.output_file, stations, field)


if __name__ == '__main__':
    command_line_parser = argument_parser()
    autocomplete(command_line_parser)

    # raise errors if encountered with invalid numerical operations
    # rather than failing silently
    with care:
        main(command_line_parser.parse_args())
