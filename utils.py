"""
Miscellaneous helper functions.
"""
import sys
import json

import fortranformat
import numpy

# optional bash autocomplete support
try:
    import argcomplete

    def autocomplete(parser):
        """
        Provide ``bash`` autocompletion for a argument parser.
        """
        argcomplete.autocomplete(parser)

except ImportError:
    def autocomplete(_):
        """
        Do nothing
        """
        pass

#: Raises error in case of failed floating point operations.
care = numpy.errstate(invalid='raise', divide='raise',
                      under='ignore', over='raise')


def info(string, *args, **kwargs):
    """ Print message to ``stdout`` immediately. """
    print string.format(*args, **kwargs),
    sys.stdout.flush()


def read_json(filename):
    """
    Read the contents of a ``.json`` file.
    """
    with open(filename) as fl:
        return json.load(fl)


def write_json(filename, thing):
    """
    Write a (JSON serializable) object to a ``.json`` file.
    """
    with open(filename, 'w') as fl:
        json.dump(thing, fl, indent=2, sort_keys=True)


def write_fortran(fmt, *things):
    """
    Write in Fortran format.
    Helps compatibility with Pollitz's code.
    """
    return fortranformat.FortranRecordWriter('(' + fmt + ')').write(things)


def read_fortran(fmt, line):
    """
    Read in Fortran format.
    Helps compatibility with Pollitz's code.
    """
    return fortranformat.FortranRecordReader('(' + fmt + ')').read(line)


class lazy(object):
    """
    A property descriptor that guarantees that it is calculated only
    for the first time it is accessed.
    The property should be immutable, as it replaces itself with the
    calculated value.

    Adopted from [BeazleyJones2013]_.
    """

    def __init__(self, getter):
        """
        Saves the definition of the property for later use.

        :arg getter: the function definition of the property
        """
        self.getter = getter
        self.property_name = getter.__name__

    def __get__(self, obj, cls):
        """
        This is called when the property is accessed.
        """
        # was called with no object
        # so return the wrapped getter to let Sphinx access its documentation
        if obj is None:
            return self.getter

        # calculate the required value by calling the defining function
        value = self.getter(obj)

        # an attribute with the same name as the property is created
        # and its value is set with the calculated value
        # the next time the property is accessed,
        # this value will be immediately returned
        # instead of recalculating it
        setattr(obj, self.property_name, value)

        return value


class Record(object):
    """
    A simple object whose set of fields is configurable.
    """
    # list of known fields
    _fields = []

    def __init__(self, **properties):
        """
        Create an instance with attributes given by a dictionary
        of properties.
        """
        self.populate(**properties)

    def populate(self, **properties):
        """
        Populate attributes of *self* from the given dictionary.
        """
        for key in properties:
            setattr(self, key, properties[key])

    def clone(self):
        """
        Create a clone with only the known list of properties copied.
        """
        return type(self)(**{key: getattr(self, key)
                             for key in self._fields})

    def replace(self, **properties):
        """
        Creates a copy with the given attributes replaced.
        """
        result = Record(**vars(self))
        result.populate(**properties)
        return result

    def __repr__(self):
        """
        Readable string representation.
        """
        properties = ", ".join([key + "=" + str(value)
                                for key, value in vars(self).iteritems()])
        return type(self).__name__ + "(" + properties + ")"
