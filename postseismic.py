"""
Calculation of postseismic displacement fields.
"""
import numpy
from residue import residual_mode
from earth import pick_rheology
from basic import X00, dX10, ddX20


def toroidal_modes(toroidal_roots, original_earth, rheology):
    """
    Calculates postseismic residual mode data for toroidal motion.
    Returns a dictionary with (*l*, *root*) pairs as keys and
    :class:`ResidualMode` objects as values.
    It is assumed that *original_earth* has layer boundaries at source radii.
    """
    result = {}

    # create Layer objects with appropriate methods for toroidal
    # solution propagation for the given rheology in the Laplace domain
    cls = pick_rheology(rheology, 'toroidal')
    earth = original_earth.transform(cls.mimic)

    for l, l_roots in toroidal_roots:
        for root in l_roots:
            # propagate the radial solution y and physical properties
            # of earth layers and sample at the array of radii r
            result[l, root] = residual_mode(l, root, earth)

    return result


def spheroidal_modes(spheroidal_roots, original_earth, rheology):
    """
    Calculates postseismic residual mode data for spheroidal motion.
    Returns a dictionary with (*l*, *root*) pairs as keys and
    :class:`ResidualMode` objects as values.
    It is assumed that *original_earth* has layer boundaries at source radii.
    """
    result = {}

    # create Layer objects with appropriate methods for spheroidal
    # solution propagation for the given rheology in the Laplace domain
    cls = pick_rheology(rheology, 'spheroidal')
    earth = original_earth.transform(cls.mimic)

    for l, l_roots in spheroidal_roots:
        for root in l_roots:
            # propagate the radial solution y and physical properties
            # of earth layers and sample at the array of radii r
            result[l, root] = residual_mode(l, root, earth)

    return result


def toroidal_excitation_coefficients(mode, source_index, M, stations):
    """
    Coefficients for the excitation of the toroidal mode by
    the seismic source.
    See [Pollitz1992]_.
    """
    l = mode.l
    y_s = mode.y[source_index]

    # the layer with the source at the bottom
    src = mode.at(source_index)
    r_s = src.bottom

    # mathematical functions
    sin_phi, cos_phi = stations.sin_phi, stations.cos_phi
    sin_2phi, cos_2phi = stations.sin_2phi, stations.cos_2phi

    Sigma = numpy.zeros(stations.shape + (5,))
    dSigma = numpy.zeros(stations.shape + (5,))

    # the indexing is shifted by one from Pollitz's paper
    # due to Python's convention

    # Pollitz's notation
    # indices: [point, subscript]
    if l >= 1:
        Sigma[:, 3] = (2 * dX10(l) * (y_s[1] / src.mu) *
                       (M[0, 2] * cos_phi - M[0, 1] * sin_phi))

    if l >= 2:
        Sigma[:, 4] = (4 * ddX20(l) * (y_s[0] / r_s) *
                       (-(M[1, 1] - M[2, 2]) * sin_2phi / 2 +
                        M[1, 2] * cos_2phi))

    if l >= 1:
        dSigma[:, 3] = (2 * dX10(l) * (y_s[1] / src.mu) *
                        (M[0, 2] * sin_phi + M[0, 1] * cos_phi))

    if l >= 2:
        dSigma[:, 4] = (4 * ddX20(l) * (y_s[0] / r_s) *
                        ((M[1, 1] - M[2, 2]) * cos_2phi +
                         2 * M[1, 2] * sin_2phi))

    return Sigma, dSigma


def toroidal_displacements(mode, coefficients, stations, epochs):
    """
    Postseismic toroidal displacement field evaluated at specified
    observation points and times.
    See [Pollitz1992]_.

    :arg mode: the toroidal residual mode data
    :arg coefficients: coefficients for the excitation of the mode by
                       the seismic source
    :arg stations: observation points, that is, stations
    :arg epochs: observation epochs
    """
    l = mode.l
    y = mode.y

    # mathematical functions
    X, dX = stations.X, stations.dX
    sin_theta = stations.sin_theta

    # coefficients of excitation by the source
    Sigma, dSigma = coefficients

    # indices: [point]
    u_r = numpy.zeros_like(stations.theta)
    u_theta = numpy.zeros_like(stations.theta)
    u_phi = numpy.zeros_like(stations.theta)

    # the contributions are non-zero only for m <= l
    # See Pollitz's equation 32

    # the indexing is shifted by one from Pollitz's paper
    # due to Python's convention

    # displacement field on the surface of the earth

    # u_theta
    if l >= 1:
        u_theta += (y[-1][0] / sin_theta) * X[:, l, 1] * dSigma[:, 3]

    if l >= 2:
        u_theta += (y[-1][0] / sin_theta) * X[:, l, 2] * dSigma[:, 4]

    # u_phi
    if l >= 1:
        u_phi += y[-1][0] * dX[:, l, 1] * Sigma[:, 3]

    if l >= 2:
        u_phi += y[-1][0] * dX[:, l, 2] * Sigma[:, 4]

    # Pollitz's equation 31
    static = (numpy.array([u_r, u_theta, u_phi]) /
              (mode.epsilon * (-mode.s)))
    return numpy.einsum('ij,k', static,
                        ((1 - numpy.exp(mode.s * epochs))))


def spheroidal_excitation_coefficients(mode,
                                       source_index, M, stations):
    """
    Coefficients for the excitation of the spheroidal mode by
    the seismic source.
    See [Pollitz1992]_.
    """
    l = mode.l
    y_s = mode.y[source_index]

    # the layer with the source at the bottom
    src = mode.at(source_index)
    r_s = src.bottom

    # mathematical functions
    sin_phi, cos_phi = stations.sin_phi, stations.cos_phi
    sin_2phi, cos_2phi = stations.sin_2phi, stations.cos_2phi

    # the indexing is shifted by one from Pollitz's paper
    # due to Python's convention

    # derivative of y at source with respect to r
    dy_s = src.differential_matrix(r_s).dot(y_s)

    Sigma = numpy.zeros(stations.shape + (5,))
    dSigma = numpy.zeros(stations.shape + (5,))

    # Pollitz's notation
    # indices: [point, subscript]
    if l == 0:
        Sigma[:, 0] = (X00(l) * (dy_s[0] * M[0, 0] +
                                 (y_s[0] / r_s) *
                                 (M[1, 1] + M[2, 2])))
    else:
        Sigma[:, 0] = (X00(l) * (dy_s[0] * M[0, 0] +
                                 ((y_s[0] -
                                   l * (l + 1.) * y_s[2] / 2.) / r_s) *
                                 (M[1, 1] + M[2, 2])))

    if l >= 1:
        Sigma[:, 1] = (2 * dX10(l) * (y_s[3] / src.mu) *
                       (M[0, 1] * cos_phi + M[0, 2] * sin_phi))
    if l >= 2:
        Sigma[:, 2] = (4 * ddX20(l) * (y_s[2] / r_s) *
                       ((M[1, 1] - M[2, 2]) * cos_2phi / 2. +
                        M[1, 2] * sin_2phi))

    # derivatives with respect to phi
    if l >= 1:
        dSigma[:, 1] = (2 * dX10(l) * (y_s[3] / src.mu) *
                        (-M[0, 1] * sin_phi + M[0, 2] * cos_phi))

    if l >= 2:
        dSigma[:, 2] = (4 * ddX20(l) * (y_s[2] / r_s) *
                        (-(M[1, 1] - M[2, 2]) * sin_2phi +
                         2 * M[1, 2] * cos_2phi))

    return Sigma, dSigma


def spheroidal_displacements(mode, coefficients, stations, epochs):
    """
    Postseismic spheroidal displacement field evaluated at specified
    observation points and times.
    See [Pollitz1992]_.

    :arg mode: the spheroidal residual mode data
    :arg coefficients: coefficients for the excitation of the mode by
                       the seismic source
    :arg stations: observation points, that is, stations
    :arg epochs: observation epochs
    """
    l = mode.l
    y = mode.y

    # mathematical functions
    X, dX = stations.X, stations.dX
    sin_theta = stations.sin_theta

    # coefficients of excitation by the source
    Sigma, dSigma = coefficients

    # indices: [point]
    u_r = numpy.zeros_like(stations.theta)
    u_theta = numpy.zeros_like(stations.theta)
    u_phi = numpy.zeros_like(stations.theta)

    # the contributions are non-zero only for m <= l
    # See Pollitz's equation 27

    # the indexing is shifted by one from Pollitz's paper
    # due to Python's convention

    # displacement field on the surface of the earth

    # u_r
    u_r += y[-1][0] * X[:, l, 0] * Sigma[:, 0]

    if l >= 1:
        u_r += y[-1][0] * X[:, l, 1] * Sigma[:, 1]

    if l >= 2:
        u_r += y[-1][0] * X[:, l, 2] * Sigma[:, 2]

    # u_theta
    if l >= 1:
        u_theta += y[-1][2] * dX[:, l, 0] * Sigma[:, 0]
        u_theta += y[-1][2] * dX[:, l, 1] * Sigma[:, 1]
    if l >= 2:
        u_theta += y[-1][2] * dX[:, l, 2] * Sigma[:, 2]

    # u_phi
    if l >= 1:
        u_phi += (y[-1][2] / sin_theta) * X[:, l, 1] * dSigma[:, 1]

    if l >= 2:
        u_phi += (y[-1][2] / sin_theta) * X[:, l, 2] * dSigma[:, 2]

    # Pollitz's equation 26
    static = (numpy.array([u_r, u_theta, u_phi]) /
              (mode.epsilon * (-mode.s)))
    return numpy.einsum('ij,k', static,
                        ((1 - numpy.exp(mode.s * epochs))))


def surface_displacements(post_modes, fault,
                          earth, stations, epochs):

    """
    Postseismic surface displacement field evaluated at specified stations,
    that is, observation points, and times.

    :arg sphero_modes: dictionary from :func:`spheroidal_modes`
    :arg toro_modes: dictionary from :func:`toroidal_modes`
    :arg fault: description of the fault that acts as the seismic source
    :arg earth: an :class:`Earth` object with layer boundaries at fault radii
    :arg stations: observation points in epicentral coordinates
    :arg epochs: observation times
    """
    sphero_modes, toro_modes = post_modes

    # indices: [axis, point, time]
    u = numpy.zeros((3,) + stations.shape + epochs.shape)

    # index of the layer with the source at its bottom
    source_index = earth.find_index(fault.radius)

    for l, root in sorted(toro_modes):
        mode = toro_modes[l, root]

        # coefficients of excitation by the source
        coefficients = toroidal_excitation_coefficients(mode, source_index,
                                                        fault.M, stations)

        # calculate contributions from the residues of toroidal modes
        u += toroidal_displacements(mode, coefficients, stations, epochs)

    for l, root in sorted(sphero_modes):
        mode = sphero_modes[l, root]

        # coefficients of excitation by the source
        coefficients = spheroidal_excitation_coefficients(mode,
                                                          source_index,
                                                          fault.M, stations)

        # calculate contributions from the residues of spheroidal modes
        u += spheroidal_displacements(mode, coefficients, stations, epochs)

    return u
