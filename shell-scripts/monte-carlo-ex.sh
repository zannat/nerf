#!/bin/bash
set -eou pipefail

for task in "summation" "transformation"; do
    for N in $(seq 5 5 50); do
	batch=$(echo 10000 / $N | bc)
	ex=$(echo python monte_carlo.py --task $task --N $N --batch $batch -x 2. \
	          --T-precision=0.0001 --delta-T-precision=0.01 \
		  --R-precision=0.01 --delta-R-precision=1.)
	echo $ex
	nice $ex > ${task}-${N}-x-2.log
	# echo $ex --Voronoi
	# nice $ex --Voronoi > ${task}-${N}-Voronoi.log
    done
done
