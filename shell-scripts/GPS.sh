#!/bin/bash
set -eou pipefail

for pair in Sumatra-Andaman,2004-12-26 Tohoku-Oki,2011-03-11; do
    OLD_IFS=$IFS
    IFS=","
    set $pair
    EARTHQUAKE=$1
    DATE=$2
    IFS=$OLD_IFS

    time nice python GPS.py -d --task ITRF -n $EARTHQUAKE/itrf2008 \
        -f $EARTHQUAKE \
        --stations-file=core_sites_itrf2008
    time nice python GPS.py -d --task ITRF -n $EARTHQUAKE/itrf2014 \
        -f $EARTHQUAKE \
        --stations-file=core_sites_itrf2014
    time nice python GPS.py -d --task SOPAC -n $EARTHQUAKE/SOPAC \
        --date $DATE -f $EARTHQUAKE
    time nice python GPS.py -d --task SOPAC-GPS -n $EARTHQUAKE/SOPAC-GPS \
        --date $DATE
done


