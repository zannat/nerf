#!/bin/bash

FAULT_FILE=stat2g.in_SUM1Dgr
FAULT_DIR=faults
SCRIPTS=shell-scripts
START_L=100
END_L=100
STEP_L=10

while [[ $# -gt 0 ]]
do
    key="$1"
    case $key in
        --fault)
            FAULT_FILE="$2"
            shift
            ;;
        --start-l)
            START_L="$2"
            shift
            ;;
        --end-l)
            END_L="$2"
            shift
            ;;
        --step-l)
            STEP_L="$2"
            shift
            ;;
        *)
            echo "I do not understand option [$key]"
            exit 0
            ;;
    esac
    shift
done

both() {
time python quake.py --task=coseismic --fault "$FAULT_FILE" --max-l "$1"
python plot.py plot log.quake.log -o "${FAULT_FILE}.${1}.us"
}

echo ============================================
echo testing "$FAULT_FILE"
echo ============================================
echo

for i in $(seq "$START_L" "$STEP_L" "$END_L");
do
    echo ----------------------------------------------
    echo current max-l "$i"
    echo ----------------------------------------------
    echo
    both "$i"
done
