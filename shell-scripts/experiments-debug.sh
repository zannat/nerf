#!/bin/bash
set -eou pipefail
OUT=../data
for METHOD in angle-grid area-grid monte-carlo; do
echo doing $METHOD

PROJECT=lean-$METHOD
mkdir -p $OUT/$PROJECT

for moment in dc-1; do
  for depth in 5; do
    for div in 400; do
      for pair in 1,1 0,-1; do
          W=$(echo $div + $div | bc)
          L=$div
          OLD_IFS=$IFS
          IFS=","
          set $pair
          minl=$1
          maxl=$2
          IFS=$OLD_IFS
          folder=$PROJECT/$moment/depth-$depth/l-${minl}-${maxl}/size-$L
          ex=$(echo python tally.py -o $OUT -n $folder --method=$METHOD \
                    -t co -m $moment --depth $depth --min-l=$minl --max-l=$maxl -L $L -W $W)
          echo $ex
          nice $ex
      done
    done
  done
done

done
