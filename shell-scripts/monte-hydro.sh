#!/bin/bash
set -eou pipefail

 for method in "summation" "transformation"; do
    for N in $(seq 6 2 50); do
        ex=$(echo nice python hydro.py -d --task monte-carlo \
               --method $method -N $N --grid-folder=../data/hydro/anomaly-grid2)
        echo $ex
        nice $ex > ${method}-${N}.log
        echo $ex --Voronoi
        nice $ex --Voronoi > ${method}-${N}-Voronoi.log
    done
done
