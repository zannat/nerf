#!/bin/bash
set -eou pipefail

for task in "summation" "transformation"; do
    for N in $(seq 6 4 50); do
        batch=$(echo "5000 / sqrt($N)" | bc)
        ex=$(echo python monte_carlo.py --task $task --N $N --batch $batch)
        echo $ex
        nice $ex > ${task}-${N}.log
        # echo $ex --Voronoi
        # nice $ex --Voronoi > ${task}-${N}-Voronoi.log
    done
done
