#!/bin/bash
set -eou pipefail
OUT=../data

METHOD=angle-grid
PROJECT=solid-okada

mkdir -p $OUT/$PROJECT

for moment in dc-1 ; do
  for depth in 10 ; do
      FOLDER=$PROJECT/$moment/$depth
      ex=$(echo python tally.py -d -o $OUT -n $FOLDER --method=$METHOD \
		-t co -m $moment --depth $depth \
		-L 1200 -W 2400 --love-numbers-file love.pickle)
      echo $ex
      nice $ex
#      pushd $OUT/$FOLDER
#      tar vcf data.tar *
#      gzip data.tar
#      rm field.log
#      rm options.json
#      popd
  done
done
