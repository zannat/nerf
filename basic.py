"""
Elementary mathematical and geometrical functions.
"""
import operator
from itertools import dropwhile

from numpy import sqrt, pi, sin, cos
from numpy import arccos, arctan2
from numpy import einsum, array
from numpy import cross, eye
import numpy

from scipy.linalg import expm

# the imaginary unit
ii = complex(0., 1.)

# the range of influence of a seismic source
# in units of the wavelength of the spherical harmonic mode
CUTOFF = 2.5


def X00(l):
    r"""
    :math:`\lim_{\theta \to 0} X_{l, 0}(\theta)`.
    Needed for calculating strain at the seismic source
    in epicentral coordinates.
    See [Pollitz1996]_ (page 4).
    """
    return sqrt((2. * l + 1.) / (4. * pi))


def dX10(l):
    r"""
    :math:`\lim_{\theta\to 0} X'_{l,1}(\theta)`
    where the prime denotes differentiation.
    Needed for calculating strain at the seismic source
    in epicentral coordinates.
    See [Pollitz1996]_ (page 4).
    """
    return -X00(l) * sqrt(l * (l + 1.)) / 2.


def ddX20(l):
    r"""
    :math:`\lim_{\theta\to 0} X''_{l,2}(\theta)`
    where the primes denote differentiations.
    Needed for calculating strain at the seismic source
    in epicentral coordinates.
    See [Pollitz1996]_ (page 4).
    """
    return -dX10(l) * sqrt((l - 1.) * (l + 2.)) / 2.


def wavelength(l):
    """
    Wavelength of the degree-:math:`l`
    spherical harmonics.
    """
    return 4. * numpy.pi / (2. * l + 1)


def cutoff_l(source_radius, cutoff=CUTOFF):
    """
    The maximum spherical harmonic degree :math:`l`
    allowed by the wavelength cutoff.
    """
    depth = 1. - source_radius
    # wavelength corresponding to l is 4 * pi / (2 * l + 1)
    # we want depth < cutoff * wavelength
    return int(numpy.ceil(((cutoff * 4. * pi) / depth - 1) / 2.))


def cutoff_max_l(max_l, source_radius, cutoff=CUTOFF):
    """
    The maximum spherical harmonic degree :math:`l`
    allowed by the wavelength cutoff.
    """
    # in our internal units
    # pick the maximum l whose wavelength is greater than source depth
    def off_limits(l):
        """
        Whether l is more than the cutoff.
        """
        return cutoff * wavelength(l) < 1. - source_radius

    try:
        return next(dropwhile(off_limits, range(max_l, 2 - 1, -1)))
    except StopIteration:
        return max_l


def norm(l, m):
    r"""
    The normalization of :math:`Y_{lm}(\theta,\phi)`.
    We assume non-negative :math:`m` which is sufficient for our purposes.
    See [Pollitz1996]_ (page 2).
    Our norm differs by a factor of :math:`(-1)^m` from that of Pollitz
    because the implementation of the associated Legendre functions
    in `SciPy`_ differ by the same factor.

    .. _SciPy: http://www.scipy.org
    """
    assert m >= 0

    if m > l:
        return 0.

    def fac_sqrt(l, m):
        """ square root of (l - m)! / (l + m)! """

        def product(iterable):
            """ The product of all elements from the iterable. """
            return reduce(operator.mul, iterable, 1)

        # to avoid overflow take the sqrt before taking the product
        return 1. / product([sqrt(float(x))
                             for x in range(l - m + 1, l + m + 1)])

    return X00(l) * fac_sqrt(l, m)


def angular_to_cartesian(theta, phi):
    """
    Cartesian coordinates of a point on the unit sphere.
    Also works if *theta* and *phi* are same-sized arrays.
    """
    return array([sin(theta) * cos(phi),
                  sin(theta) * sin(phi),
                  cos(theta)])


def cartesian_to_angular(vector):
    """
    Spherical angular coordinates of a cartesian vector
    on the unit sphere. Also works for arrays. The function
    :func:`cartesian_to_angular` is the inverse transformation of
    :func:`angular_to_cartesian`.
    """
    # the first index is the axis (0, 1, 2 for x, y, z respectively)
    # the ... stands for whatever indexes there might be for the array
    theta = arccos(vector[2, ...])
    phi = arctan2(vector[1, ...], vector[0, ...])

    return theta, phi


def cartesian_to_spherical_matrix(theta, phi):
    """
    The orthogonal matrix that transforms a vector in cartesian
    coordinates into one in spherical coordinates. Also works
    when *theta* and *phi* are same-sized arrays. In that case, the
    result is actually an array of matrices, one for each
    position.
    """
    sin_theta = sin(theta)
    cos_theta = cos(theta)
    sin_phi = sin(phi)
    cos_phi = cos(phi)

    result = array([[sin_theta * cos_phi, sin_theta * sin_phi, cos_theta],
                    [cos_theta * cos_phi, cos_theta * sin_phi, -sin_theta],
                    [-sin_phi, cos_phi, numpy.zeros_like(phi)]])

    return result


def cartesian_to_spherical(theta, phi, vector):
    r"""
    Converts a cartesian vector at specified position on the unit
    sphere to a spherical vector. In case *theta* and *phi* are arrays,
    the result is an array of transformed spherical vectors.

    Our representation of a vector
    :math:`v_r\;\hat{\mathbf{r}}
    + v_{\theta}\;\hat{\mathbf{\theta}}
    + v_{\phi}\;\hat{\mathbf{\phi}}`
    in the spherical coordinate system
    is by a :mod:`numpy` array :code:`[v_r, v_theta, v_phi]`.
    """

    # einsum stands for Einstein summation convention
    # so ij,j represents matrix multiplication of a matrix and a vector
    return einsum('ij...,j...->i...',
                  cartesian_to_spherical_matrix(theta, phi), vector)


def spherical_to_cartesian(theta, phi, vector):
    r"""
    Converts a spherical vector at specified position on the unit
    sphere to a cartesian vector. In case *theta* and *phi* are arrays,
    the result is an array of transformed cartesian vectors.
    The function :func:`spherical_to_cartesian` is the inverse of
    :func:`cartesian_to_spherical`.

    Our representation of a vector
    :math:`v_x\;\hat{\mathbf{x}}
    + v_{y}\;\hat{\mathbf{y}}
    + v_{z}\;\hat{\mathbf{z}}`
    in the cartesian coordinate system
    is by a :mod:`numpy` array :code:`[v_x, v_y, v_z]`.
    """

    # einsum stands for Einstein summation convention
    # so ji,j stands for matrix multiplication of the transpose
    # of a matrix with a vector
    # since the matrix is orthogonal, this is in fact its inverse
    return einsum('ji...,j...->i...',
                  cartesian_to_spherical_matrix(theta, phi), vector)


def rotate_around(axis, angle):
    """
    The matrix that rotates its input vector around the given
    axis by the given angle.
    """
    return expm(cross(eye(3), axis * angle))


def to_prime_meridian(fault):
    """
    The matrix that sends the epicenter to the prime meridian.
    """
    e3 = array([0., 0., 1.])
    phi = fault.longitude

    return rotate_around(e3, -phi)


def to_pole(fault):
    """
    The matrix that sends the epicenter at the prime meridian to
    the north pole.
    """
    e2 = array([0., 1., 0.])
    theta = fault.colatitude

    return rotate_around(e2, -theta)


def geographical_to_epicentral(fault):
    """
    The global geographical-to-epicentral cartesian coordinate transformation
    matrix. It sends the earthquake epicenter to the north pole.
    """
    return to_pole(fault).dot(to_prime_meridian(fault))


def normalize(vector):
    """
    Normalizes the given vector. The input may be multi-dimensional,
    in that case it is normalized only along the first axis.
    """
    return vector / sqrt(einsum("i...,i...->...", vector, vector))


# this function sums up contributions from negative as well as positive m
# upto this point the calculations were for non-negative m only
# Pollitz's convention determines the contributions
# of the negative m entirely
def mode_count(m):
    """
    For m > 0, there are contributions from both m and -m modes.
    The sign factors from y and Y due to Pollitz's convention
    cancel each other, so these two contributions are complex conjugates.
    Therefore, the sum is twice the real part of any one of these.

    For m == 0, y and Y are real.
    """
    if m == 0:
        return 1.
    else:
        return 2.
