"""
Implementation of layered earth models.
"""
import numpy
from utils import Record


class Layer(Record):
    """
    Represents one layer in the earth model. Its properties
    should include its top and bottom radii, bulk modulus :math:`\\kappa`,
    shear modulus :math:`\\mu`, density :math:`\\rho` and viscosity
    :math:`\\eta`. This is the base class with default implementations
    of the methods needed to propagate a radial solution
    of the equation of motion across the layer for different physical
    models of the earth.
    """
    # list of known fields
    _fields = ['top', 'bottom', 'density', 'mu', 'kappa', 'eta']

    @classmethod
    def mimic(cls, layer, **properties):
        """
        Create an instance of the given class having the
        properties of an existing layer. It is meant to be
        called by the subclasses of :class:`Layer`. Additional
        properties can also be set.
        """
        result = cls(**vars(layer))
        result.populate(**properties)
        return result

    # NOTE: Python properties look like ordinary attributes
    # however, their values are calculated, not stored in the object
    # we use them here for derived properties.

    @property
    def lamda(self):
        r"""
        Lame parameter :math:`\lambda`.
        """
        return self.kappa - 2. * self.mu / 3.

    @property
    def sigma(self):
        """
        Coefficient related to the *P*-wave velocity :math:`\\alpha`
        by the relation :math:`\\alpha^2 = \\sigma / \\rho`
        where :math:`\\rho` is the density.
        """
        return self.lamda + 2. * self.mu

    @property
    def gamma(self):
        """
        Another convenient coefficient that appears in the matrices.
        :math:`\\gamma = \\lambda + \\mu - \\lambda^2 / \\sigma`.
        Adopted from [Pollitz1992]_.
        """
        return self.lamda + self.mu - self.lamda ** 2 / self.sigma

    def differential_matrix(self, r):
        """
        The matrix :math:`A` in the equation of motion
        :math:`{d\\mathbf{y}}/{dr} = A(r)\\;\\mathbf{y}(r)`.
        To be implemented by the subclasses.
        """
        raise NotImplementedError()

    def matrizant_matrix(self, r):
        """
        The matrix :math:`P` with the linearly independent solutions to the
        equation of motion as columns.
        Obeys the equation :math:`dP/dr = A(r)\\;P(r)`.
        To be implemented by the subclasses.
        """
        raise NotImplementedError()

    def inverse_matrizant_matrix(self, r):
        """
        The inverse of the matrizant matrix.
        """
        return numpy.linalg.inv(self.matrizant_matrix(r))

    def __contains__(self, r):
        """
        Returns whether radius *r* lies inside the layer.
        Enables the Python syntax :code:`r in layer`.
        """
        return self.bottom <= r and r <= self.top

    def propagate_numerically(self, y, div=10):
        """
        Numerically propagate a radial solution *y* to the equation of motion
        at the bottom of the layer to the top.
        The propagation is done by dividing the layer into *div* pieces.
        """
        r_all, dr = numpy.linspace(self.bottom, self.top, div,
                                   endpoint=False, retstep=True)

        result = numpy.copy(y)
        identity = numpy.identity(self.dimension)

        def step(r):
            """
            One step in the numerical propagation.
            If dr is small, dy is approximately A(r) y dr.
            """
            return identity + dr * self.differential_matrix(r + dr / 2.)

        for r in r_all:
            result = step(r).dot(result)

        return result

    def regular_initial_condition(self):
        """
        Returns the half of the solutions of the equation of motion
        that are regular at the origin. In the subclasses,
        we will place the regular columns of the matrizant matrices
        to the left.
        """
        return self.matrizant_matrix(self.bottom)[:, :(self.dimension / 2)]

    def __str__(self):
        """
        String representation of a layer for easy inspection.
        """
        def attr(key):
            """ Format a single attribute. """
            value = getattr(self, key)
            if isinstance(value, float):
                return "%s: %.3e" % (key, value)
            return "{}: {}".format(key, value)

        return " ".join([attr(key) for key in sorted(vars(self).keys())])

    def physical_property_names(self):
        """
        Set of the names of attributes that represent physical properties.
        Used to identify physically distinct layers.
        """
        return {name
                for name in vars(self)
                if name not in ['top', 'bottom', 'tag']}

    def __eq__(self, other):
        """
        Whether two layers have the same physical properties.
        Used to identify physically distinct layers.
        """
        if self.physical_property_names() != other.physical_property_names():
            return False

        for key in self.physical_property_names():
            if getattr(self, key) != getattr(other, key):
                return False

        return True


class StaticToroidal(Layer):
    """
    Implementation of solution propagation for the toroidal mode of the
    equation of motion in the static case. Here, the normal mode frequency
    :math:`\\omega` is zero. The dimension of the solution space is two.
    """
    dimension = 2

    # list of known fields
    _fields = Layer._fields + ['l']

    def differential_matrix(self, r):
        """
        The matrix :math:`A` in the equation of motion
        :math:`{d\\mathbf{y}}/{dr} = A(r)\\;\\mathbf{y}(r)`.
        See [TakeuchiSaito1972]_ (section II.C).
        """
        l, mu = self.l, self.mu

        result = numpy.zeros((2, 2))
        result[0, 0] = 1. / r
        result[0, 1] = 1. / mu
        result[1, 0] = (l - 1.) * (l + 2.) * mu / (r * r)
        result[1, 1] = -3. / r
        return result

    def matrizant_matrix(self, r_at):
        """
        The matrix with the linearly independent solutions to the
        equation of motion as columns.
        The column regular at the origin is to the left.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        See [Pollitz1992]_ (equation 37).
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        # rescaling helps prevent overflow
        # adopted from Pollitz's code
        r = r_at / b

        # it is easy to see that if mu is rescaled as well
        # then the code is identical to the equation in the paper
        mu = self.mu / b

        # there is a duality between the two solutions
        l = self.l
        dual_l = -(l + 1)
        r_l = r ** l
        r_dual_l = 1. / (r_l * r)

        result = numpy.empty((2, 2) + extra_indices)

        result[0, 0, ...] = r_l
        result[1, 0, ...] = mu * (l - 1.) * r_l / r

        result[0, 1, ...] = r_dual_l
        result[1, 1, ...] = mu * (dual_l - 1.) * r_dual_l / r
        return result

    def inverse_matrizant_matrix(self, r_at):
        """
        The inverse of the matrizant matrix.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        # the rescaling helps prevent overflow
        r = r_at / b

        # the same rescaling preserves the form of the equation
        mu = self.mu / b

        l = self.l
        r_l = r ** l

        # analyical expression for the inverse
        # found with the help of Mathematica
        result = numpy.empty((2, 2) + extra_indices)

        result[0, 0, ...] = (l + 2) / r_l
        result[0, 1, ...] = (r / r_l) / mu

        result[1, 0, ...] = (l - 1) * r_l * r
        result[1, 1, ...] = -(r_l * (r * r)) / mu

        return result / (2. * l + 1.)

    def epsilon_integrand(self, r, y):
        """
        Integrand function to calculate :math:`\\epsilon` for a residual mode.
        See equations 29 and 30 in [Pollitz1992]_, and also
        [TakeuchiSaito1972]_ (section III.C).
        Our integrand differs that from [Pollitz1992]_ by a factor of
        :math:`l  (l + 1)` coming from different normalizations
        of toroidal modes.

        :param r: sampling points
        :param y: displacement-stress vector sampled at *r*
        """
        l = self.l
        mu = self.mu
        mu_derivative = self.mu_derivative

        # the indexes are shifted by one according to Python's convention
        return (l * (l + 1.) * (r ** 2 * y[:, 1] ** 2 / mu ** 2 +
                                (l - 1.) * (l + 2.) * y[:, 0] ** 2) *
                mu_derivative)


class StaticSpheroidal(Layer):
    """
    Implementation of solution propagation for the spheroidal mode of the
    equation motion in the static case. Here, the normal mode frequency
    :math:`\\omega` is zero. The dimension of the solution space is four.
    """
    dimension = 4

    # list of known fields
    _fields = Layer._fields + ['l']

    def differential_matrix(self, r):
        """
        The matrix :math:`A` in the equation of motion
        :math:`{d\\mathbf{y}}/{dr} = A(r)\\;\\mathbf{y}(r)`.
        See [TakeuchiSaito1972]_ (section II.C).
        """
        (l, lamda, mu, sigma, gamma) = (self.l, self.lamda, self.mu,
                                        self.sigma, self.gamma)

        r_2 = r * r

        result = numpy.zeros((4, 4))
        result[0, 0] = -(2. * lamda) / (sigma * r)
        result[0, 1] = 1. / sigma
        result[0, 2] = (l * (l + 1.) * lamda) / (sigma * r)

        result[1, 0] = 4. * gamma / r_2
        result[1, 1] = 2. * (lamda / sigma - 1.) / r
        result[1, 2] = -2. * l * (l + 1.) * gamma / r_2
        result[1, 3] = l * (l + 1.) / r

        result[2, 0] = -1. / r
        result[2, 2] = 1. / r
        result[2, 3] = 1. / mu

        result[3, 0] = -2. * gamma / r_2
        result[3, 1] = -lamda / (sigma * r)
        result[3, 2] = l * (l + 1.) * (gamma + mu) / r_2 - 2. * mu / r_2
        result[3, 3] = -3. / r

        return result

    def matrizant_matrix(self, r_at):
        """
        The matrix with the linearly independent solutions to the
        equation of motion as columns.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        See [Pollitz1992]_ (equation 50).
        The columns have been rearranged to place the solutions regular
        at the origin to the left.
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        l = self.l
        dual_l = -(l + 1)

        # rescaling helps avoid overflow
        r = r_at / b

        # turns out rescaling the other variables is possible
        # for the code to conform to the equation in the paper
        mu, lamda = self.mu / b, self.lamda / b
        f = b / (2. * (2. * l + 3.))
        dual_f = b / (2. * (2. * dual_l + 3.))

        r_l = r ** l
        r_dual_l = 1. / (r_l * r)
        r_2 = r * r

        result = numpy.empty((4, 4) + extra_indices)
        result[0, 0, ...] = l * r_l / r
        result[1, 0, ...] = 2. * mu * l * (l - 1.) * r_l / r_2
        result[2, 0, ...] = r_l / r
        result[3, 0, ...] = 2. * mu * (l - 1.) * r_l / r_2

        result[0, 2, ...] = dual_l * r_dual_l / r
        result[1, 2, ...] = (2. * mu *
                             dual_l * (dual_l - 1.) * r_dual_l / r_2)
        result[2, 2, ...] = r_dual_l / r
        result[3, 2, ...] = 2. * mu * (dual_l - 1.) * r_dual_l / r_2

        result[0, 1, ...] = ((l + 1.) * (lamda * l + mu * (l - 2.)) *
                             f * r_l * r)
        result[1, 1, ...] = (2. * mu * (l + 1.) *
                             (lamda * (l ** 2 - l - 3.) +
                              mu * (l ** 2 - l - 2.)) *
                             f * r_l)
        result[2, 1, ...] = (lamda * (l + 3.) + mu * (l + 5.)) * f * r_l * r
        result[3, 1, ...] = (2. * mu *
                             (lamda * (l ** 2 + 2. * l) +
                              mu * (l ** 2 + 2. * l - 1.)) * f * r_l)

        result[0, 3, ...] = ((dual_l + 1.) *
                             (lamda * dual_l + mu * (dual_l - 2.)) *
                             dual_f * r_dual_l * r)
        result[1, 3, ...] = (2. * mu * (dual_l + 1.) *
                             (lamda * (dual_l ** 2 - dual_l - 3.) +
                              mu * (dual_l ** 2 - dual_l - 2.)) *
                             dual_f * r_dual_l)
        result[2, 3, ...] = ((lamda * (dual_l + 3.) + mu * (dual_l + 5.)) *
                             dual_f * r_dual_l * r)
        result[3, 3, ...] = (2. * mu *
                             (lamda * (dual_l ** 2 + 2. * dual_l) +
                              mu * (dual_l ** 2 + 2. * dual_l - 1.)) *
                             dual_f * r_dual_l)

        return result

    def inverse_matrizant_matrix(self, r_at):
        """
        The inverse of the matrizant matrix.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        l = self.l

        # rescaling helps avoid overflow
        r = r_at / b

        # interestingly the same rescaling works here as well
        mu, lamda, sigma = self.mu / b, self.lamda / b, self.sigma / b
        f = b / (2. * (2. * l + 3.))
        dual_f = b / (2. * (2. * (-(l + 1)) + 3.))

        r_l = r ** l
        r_2 = r * r

        # explicit expression for the inverse of the matrix
        # was found with the help of Mathematica
        # and resulted in performance improvement
        result = numpy.empty((4, 4) + extra_indices)

        result[0, 0, ...] = (r / r_l) * ((lamda + mu) * l * (l + 3.) - lamda)
        result[0, 1, ...] = ((r_2 / r_l) * (sigma + mu + l * (lamda + mu)) /
                             (2. * mu))
        result[0, 2, ...] = -((l + 1.) * (r / r_l) *
                              (l ** 2 * (lamda + mu) - sigma))
        result[0, 3, ...] = -((l + 1.) * (r_2 / r_l) *
                              (l * (lamda + mu) - 2. * sigma)) / (2. * mu)
        result[0, :, ...] /= (4. * l ** 2 - 1.) * sigma

        result[1, 0, ...] = -(l + 2) / (r_l * r)
        result[1, 1, ...] = -(1. / r_l) / (2. * mu)
        result[1, 2, ...] = l * (l + 2.) / (r_l * r)
        result[1, 3, ...] = (l / r_l) / (2. * mu)
        result[1, :, ...] /= (2. * l + 1.) * (2. * l + 3.) * sigma * f

        result[2, 0, ...] = (r_l * r_2 * ((l ** 2 - l) * (lamda + mu) -
                                          sigma - 2. * lamda))
        result[2, 1, ...] = ((r_l * r_2 * r * (2. * mu - l * (lamda + mu))) /
                             (2. * mu))
        result[2, 2, ...] = (l * r_l * r_2 *
                             ((l ** 2 + 2. * l) * (lamda + mu) - mu))
        result[2, 3, ...] = -((l * r_l * r_2 * r *
                               (l * (lamda + mu) + 3. * lamda + 5. * mu)) /
                              (2. * mu))
        result[2, :, ...] /= (2. * l + 1.) * (2. * l + 3.) * sigma

        result[3, 0, ...] = (l - 1.) * r_l
        result[3, 1, ...] = -r_l * r / (2. * mu)
        result[3, 2, ...] = (l ** 2 - 1.) * r_l
        result[3, 3, ...] = -(l + 1.) * r_l * r / (2. * mu)
        result[3, :, ...] /= (4. * l ** 2 - 1.) * sigma * dual_f

        return result

    def epsilon_integrand(self, r, y):
        """
        Integrand function to calculate :math:`\\epsilon` for a residual mode.
        See equations 18 and 19 in [Pollitz1997]_, and also
        [TakeuchiSaito1972]_ (section III.C).

        :param r: sampling points
        :param y: displacement-stress vector sampled at *r*
        """
        l = self.l
        mu = self.mu
        lamda = self.lamda
        sigma = self.sigma
        mu_derivative = self.mu_derivative

        return (((2. * y[:, 1] * r / sigma -
                  ((1. + 2. * lamda / sigma) *
                   (2. * y[:, 0] - l * (l + 1.) * y[:, 2]))) ** 2 / 3. +
                 l * (l + 1.) * (r * y[:, 3] / mu) ** 2 +
                 l * (l - 1.) * (l + 1.) * (l + 2.) * y[:, 2] ** 2) *
                mu_derivative)


class StaticSpheroidalDegree0(Layer):
    r"""
    Implementation of solution propagation for the spheroidal mode of the
    equation motion in the static case with :math:`l = 0`.
    Here, the normal mode frequency :math:`\\omega` is zero.
    The dimension of the solution space is two.
    """
    dimension = 2

    # list of known fields
    _fields = Layer._fields + ['l']

    def differential_matrix(self, r):
        """
        The matrix :math:`A` in the equation of motion
        :math:`{d\\mathbf{y}}/{dr} = A(r)\\;\\mathbf{y}(r)`.
        See [TakeuchiSaito1972]_ (section II.C).
        """
        l, lamda, sigma, gamma = (self.l, self.lamda,
                                  self.sigma, self.gamma)

        assert l == 0

        result = numpy.zeros((2, 2))
        result[0, 0] = -(2. * lamda) / (sigma * r)
        result[0, 1] = 1. / sigma
        result[1, 0] = 4. * gamma / (r * r)
        result[1, 1] = 2. * (lamda / sigma - 1) / r
        return result

    def matrizant_matrix(self, r_at):
        """
        The matrix with the linearly independent solutions to the
        equation of motion as columns.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        The columns have been rearranged to place the solutions regular
        at the origin to the left.
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        r = r_at

        mu, kappa = self.mu, self.kappa

        result = numpy.empty((2, 2) + extra_indices)
        result[0, 0, ...] = r / 3.
        result[1, 0, ...] = kappa
        result[0, 1, ...] = -1. / (r * r)
        result[1, 1, ...] = 4. * mu / (r ** 3)

        return result

    def inverse_matrizant_matrix(self, r_at):
        """
        The inverse of the matrizant matrix.
        As a performance trick, we allow the physical properties
        of the layer to be :mod:`numpy` arrays for this method.
        """
        b = self.bottom
        extra_indices = numpy.shape(b)

        r = r_at

        mu, kappa, sigma = self.mu, self.kappa, self.sigma

        # explicit expression for the inverse of the matrix
        # was found with the help of Mathematica
        # and resulted in performance improvement
        result = numpy.empty((2, 2) + extra_indices)

        result[0, 0, ...] = 4. * mu / r
        result[0, 1, ...] = 1.
        result[1, 0, ...] = -r ** 2 * kappa
        result[1, 1, ...] = r ** 3 / 3.

        return result / sigma

    def epsilon_integrand(self, r, y):
        """
        Integrand function to calculate :math:`\\epsilon` for a residual mode.
        See equations 18 and 19 in [Pollitz1997]_, and also
        [TakeuchiSaito1972]_ (section III.C).

        :param r: sampling points
        :param y: displacement-stress vector sampled at *r*
        """
        lamda = self.lamda
        sigma = self.sigma
        mu_derivative = self.mu_derivative

        return (((2. * y[:, 1] * r / sigma -
                  ((1. + 2. * lamda / sigma) *
                   (2. * y[:, 0]))) ** 2 / 3.) *
                mu_derivative)


class BurgersRheology(object):
    """
    Implementation of the correspondence principle for Burgers rheology.
    """

    @classmethod
    def mimic(cls, layer, **properties):
        """
        Create an instance of the given class having the
        physical properties of an existing layer.
        Expected additional property to be provided is
        the variable :math:`s`, corresponding to the time variable :math:`t`,
        after a Laplace transform.
        """

        # all the physical properties of the given layer except mu and eta
        rest = {key: value
                for key, value in vars(layer).items()
                if key not in ['eta', 'mu', 'eta1', 'mu_prime']}

        new = cls(**rest)

        # save the static mus and etas to calculate mu from
        if not hasattr(new, 'static_mu'):
            setattr(new, 'static_mu', layer.mu)
        if not hasattr(new, 'static_eta'):
            setattr(new, 'static_eta', layer.eta)
        if not hasattr(new, 'static_mu_prime'):
            if not hasattr(layer, 'mu_prime'):
                setattr(new, 'static_mu_prime', 0.)
            else:
                setattr(new, 'static_mu_prime', layer.mu_prime)
        if not hasattr(new, 'static_eta1'):
            if not hasattr(layer, 'eta1'):
                setattr(new, 'static_eta1', float('inf'))
            else:
                setattr(new, 'static_eta1', layer.eta1)

        new.populate(**properties)

        return new

    @property
    def s(self):
        r""" Laplace domain variable :math:`s`.

        When set, recalculates :math:`\mu(s)` from
        the static values of :math:`\mu` and :math:`\eta`
        according to the correspondence principle.
        """

        # the value of the property s is stored in the attribute _s
        return self._s

    @property
    def mu2(self):
        r"""
        The other shear element :math:`\mu2` in parallel with
        :math:`\eta`.
        """
        mu, mu_prime = self.static_mu, self.static_mu_prime

        return mu * mu_prime / (mu - mu_prime)

    @property
    def relaxation_times(self):
        """
        List of relaxation times.
        """
        mu, mu2 = self.static_mu, self.mu2
        eta, eta1 = self.static_eta, self.static_eta1

        return [mu / eta1, mu2 / eta, mu / eta]

    @s.setter
    def s(self, _s):
        r"""
        Sets the Laplace domain variable s.
        Recalculates :math:`\mu(s)` from
        the static values of :math:`\mu` and :math:`\eta`
        according to the correspondence principle.
        """

        # the value of the property s is stored in the attribute _s
        setattr(self, '_s', _s)

        mu = self.static_mu
        tau1, tau2, tau3 = self.relaxation_times

        try:
            setattr(self, 'mu', ((mu * _s * (_s + tau2)) /
                                 (_s * tau3 + (_s + tau1) * (_s + tau2))))

        except (ZeroDivisionError, FloatingPointError):
            # division by zero may have occurred in an array operation
            # the results were inf but that is exactly what we wanted
            # otherwise, set it to inf
            if not hasattr(self, 'mu'):
                setattr(self, 'mu', float('inf'))

    @property
    def mu_derivative(self):
        """
        :math:`{{d\\mu}/{ds}}`
        for Burgers rheology.
        """
        s = self.s
        mu = self.static_mu
        tau1, tau2, tau3 = self.relaxation_times

        return (mu * (tau1 * (s + tau2) ** 2 + s ** 2 * tau3) /
                ((s + tau1) * (s + tau2) + s * tau3) ** 2)

    def polynomial_coefficients(self):
        """
        Coefficients in the polynomials in :math:`s`
        that appear in :math:`\\mu(s)`.
        """
        tau1, tau2, tau3 = self.relaxation_times

        # from Pollitz's code, not sure if to use it
        return [x
                for x in [-tau3, -tau1, -(tau2 + tau3)]
                if x != 0.]

    def poles(self):
        """
        Poles of :math:`\\mu(s)`.
        """
        tau1, tau2, tau3 = self.relaxation_times
        sum_tau = tau1 + tau2 + tau3
        disc = numpy.sqrt(sum_tau ** 2 - 4 * tau1 * tau2)

        return [x
                for x in [-(sum_tau - disc) / 2., -(sum_tau + disc) / 2.]
                if x != 0.]


class MaxwellRheology(object):
    """
    Implementation of the correspondence principle for Maxwell rheology.
    """

    @classmethod
    def mimic(cls, layer, **properties):
        """
        Create an instance of the given class having the
        physical properties of an existing layer.
        Expected additional property to be provided is
        the variable :math:`s`, corresponding to the time variable :math:`t`,
        after a Laplace transform.
        """

        # all the physical properties of the given layer except mu and eta
        rest = {key: value
                for key, value in vars(layer).items()
                if key not in ['eta', 'mu']}

        new = cls(**rest)

        # save the static mu and eta to calculate mu from
        if not hasattr(new, 'static_mu'):
            setattr(new, 'static_mu', layer.mu)
        if not hasattr(new, 'static_eta'):
            setattr(new, 'static_eta', layer.eta)

        new.populate(**properties)

        return new

    @property
    def s(self):
        r""" Laplace domain variable :math:`s`.

        When set, recalculates :math:`\mu(s)` from
        the static values of :math:`\mu` and :math:`\eta`
        according to the correspondence principle.
        See [Pollitz1992]_ (equation 17).
        """

        # the value of the property s is stored in the attribute _s
        return self._s

    @s.setter
    def s(self, _s):
        r"""
        Sets the Laplace domain variable s.
        Recalculates :math:`\mu(s)` from
        the static values of :math:`\mu` and :math:`\eta`
        according to the correspondence principle.
        See [Pollitz1992]_ (equation 17).
        """

        # the value of the property s is stored in the attribute _s
        setattr(self, '_s', _s)

        mu, [tau] = self.static_mu, self.relaxation_times

        try:
            setattr(self, 'mu', (_s * mu) / (_s + tau))
        except ZeroDivisionError:
            setattr(self, 'mu', float('inf'))
        except FloatingPointError:
            # division by zero occurred in an array operation
            # the results were inf but that is exactly what we wanted
            pass

    @property
    def relaxation_times(self):
        """
        List of relaxation times.
        """
        mu, eta = self.static_mu, self.static_eta

        return [mu / eta]

    @property
    def mu_derivative(self):
        """
        :math:`{{d\\mu}/{ds}}`
        for Maxwell rheology.
        """
        s, mu, [tau] = self.s, self.static_mu, self.relaxation_times

        return (mu * tau) / (s + tau) ** 2

    def polynomial_coefficients(self):
        """
        Coefficients in the polynomials in :math:`s`
        that appear in :math:`\\mu(s)`.
        """
        [tau] = self.relaxation_times
        return [x for x in [-tau] if x != 0.]

    def poles(self):
        """
        Poles of :math:`\\mu(s)`.
        """
        [tau] = self.relaxation_times
        return [x for x in [-tau] if x != 0.]


def stress_rows(solution):
    """
    The stress rows of the displacement-stress vector
    :math:`\\mathbf{y}(r)`,
    that is, the radial solution of the equation
    of motion.
    """
    rows = solution.shape[0]
    # due to Python's indexing, the stress rows are the odd rows
    return solution[[x for x in range(rows) if x % 2 == 1], ...]
