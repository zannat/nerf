#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
"""
Plots displacement fields.
"""
from argparse import ArgumentParser
import numpy
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl

from utils import write_fortran, autocomplete, Record
from stations import uniform_angle_values

ENABLE_SEABORN = True


def enable_seaborn():
    """
    Better looking plots.
    """
    try:
        saved_params = dict(mpl.rcParams)
        import seaborn
        seaborn.set(context='paper')

        def restore(key):
            """
            Restore the value before importing ``seaborn``.
            """
            mpl.rcParams[key] = saved_params[key]

        restore('patch.linewidth')
        restore('xtick.major.size')
        restore('ytick.major.size')
        mpl.rcParams['axes.linewidth'] = saved_params['axes.linewidth'] / 2.
        mpl.rcParams['text.usetex'] = True
        mpl.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

        return seaborn

    except ImportError:

        return None


if ENABLE_SEABORN:
    enable_seaborn()

# plot arrow head width
HEAD_WIDTH = 0.0025

# plot arrow head length
HEAD_LENGTH = 0.0025

# magnification of the scale arrow
# relative to the maximum magnitude of displacement
MAGNIFY_SCALE = 5


def make_grid(lat_range, lon_range):
    """
    Makes a grid of observation points from
    the given latitude and longitude ranges.
    """
    for lon in lon_range:
        for lat in lat_range:
            yield lat, lon


def vector_color(vx, vy):
    """
    Returns a color that depends on the direction of the vector (*vx*, *vy*)
    for easy visualization.
    """
    hue = (numpy.arctan2(vy, vx) / (2 * numpy.pi)) + 0.5

    if hue < 0.:
        hue = 0.
    elif hue > 1.:
        hue = 1.

    hsv = numpy.array([[[hue, .95, .5]]])
    result = colors.hsv_to_rgb(hsv)
    return result[0, 0]


def read_field(grid_file):
    """
    Reads the displacement field from a file.
    """

    result = []

    with open(grid_file) as fl:
        for line in fl:
            words = line.split()
            latitude, longitude, E, N, U = [float(w) for w in words[:5]]

            if numpy.isfinite(E) and numpy.isfinite(N) and numpy.isfinite(U):
                # lets access parts as attributes
                result.append(Record(latitude=latitude,
                                     longitude=longitude,
                                     vector=numpy.array([E, N, U])))

    return result


def field_scale(arrows, horizontal):
    """
    Maximum magnitude of the displacement field.
    """

    def norm(vector):
        """
        Norm of the projection of the vector that will be plotted.
        """
        if horizontal:
            return numpy.sqrt(vector[0] ** 2 + vector[1] ** 2)
        else:
            return numpy.abs(vector[2])

    return numpy.max(numpy.array([norm(arrow.vector)
                                  for arrow in arrows]))


def delta(ls):
    """
    Average spacing of a list of plot point coordinates.
    """

    # count only unique elements
    return (max(ls) - min(ls)) / (len(set(ls)) - 1)


def between(start, end, fraction):
    """
    A point between *start* and *end* going *fraction* of the total length.
    """
    return start + (end - start) * fraction


def plot_field(pdf_file, grid_file, horizontal, magnification=1.):
    """
    Plots a displacement field.

    :arg pdf_file: the output ``.pdf`` file name
    :arg grid_file: the input file to read the displacement field from
    :arg horizontal: whether to plot the :math:`x, y` components or the
                     :math:`z` component
    :arg magnification: optional magnification factor
    """
    arrows = read_field(grid_file)
    scale = field_scale(arrows, horizontal)

    latitudes = [arrow.latitude for arrow in arrows]
    longitudes = [arrow.longitude for arrow in arrows]

    def plot_arrow(arrow, scale_arrow=False):
        """
        Put one arrow on the plot.
        """
        v = magnification * spacing * arrow.vector / scale

        if not horizontal and not scale_arrow:
            # draw the vertical component as arrows
            # at a particular angle
            draw_angle = numpy.pi / 2 - numpy.pi / 15
            v[0] = v[2] * numpy.cos(draw_angle)
            v[1] = v[2] * numpy.sin(draw_angle)

        if scale_arrow:
            color = 'black'
            width = 2.5 * HEAD_WIDTH
            length = 2.5 * HEAD_LENGTH
        else:
            color = vector_color(v[0], v[1])
            width = HEAD_WIDTH
            length = HEAD_LENGTH

        axis.arrow(arrow.longitude - v[0] / 2., arrow.latitude - v[1] / 2.,
                   v[0], v[1], alpha=0.75,
                   edgecolor=color, facecolor=color,
                   head_width=width, head_length=length,
                   length_includes_head=True)

    def plot2d():
        """
        A 2D arrow plot.
        """
        axis.set_title("scale: {} $\\times$ {:.4g} cm"
                       .format(MAGNIFY_SCALE, scale))
        axis.set_aspect('equal')

        xlim = (min(longitudes) - 2. * spacing, max(longitudes) + 2. * spacing)
        ylim = (min(latitudes) - 2. * spacing, max(latitudes) + 2. * spacing)

        # scale arrow
        x_direction = numpy.array([1., 0., 0.])
        arrow = Record(latitude=between(ylim[0], ylim[-1], 0.9),
                       longitude=between(xlim[0], xlim[-1], 0.2),
                       vector=MAGNIFY_SCALE * scale * x_direction)
        plot_arrow(arrow, scale_arrow=True)

        # data arrows
        for arrow in arrows:
            plot_arrow(arrow)

        plt.xlim(xlim[0], xlim[-1])
        plt.ylim(ylim[0], ylim[-1])

    def plot1d():
        """
        A simple plot.
        """
        try:
            assert delta(latitudes) is not None
            # successful, so plot data as a function of latitudes
            x = numpy.array(latitudes)

        except ZeroDivisionError:
            assert delta(longitudes) is not None
            # successful, so plot data as a function of longitudes
            x = numpy.array(longitudes)

        y = numpy.array([arrow.vector for arrow in arrows])

        if horizontal:
            axis.plot(x, y[:, 0], 'r', x, y[:, 1], 'b')
        else:
            axis.plot(x, y[:, 2])

    fig = plt.figure()
    axis = fig.add_subplot(1, 1, 1)

    try:
        spacing = max(delta(latitudes), delta(longitudes))

        plot2d()

    except ZeroDivisionError:
        # failed to get grid spacing for 2D plot
        # so plot 1D
        plot1d()

    plt.savefig(pdf_file)
    plt.clf()


def grid(lat_spec, lon_spec):
    """
    Prints a grid of observation points.
    """

    def read_spec(spec):
        """
        Reads a range specification in the format :code:`START,END,NUMBER`.
        """
        words = spec.split(',')
        if len(words) != 3:
            raise ValueError('spec must be of form START,END,NUMBER')

        try:
            start = float(words[0])
            end = float(words[1])
            number = float(words[2])
        except ValueError:
            raise ValueError('START, END, and NUMBER must be numbers')

        return numpy.linspace(start, end, number, endpoint=True)

    print str(read_spec(lat_spec).shape[0] * read_spec(lon_spec).shape[0])

    for lat, lon in make_grid(read_spec(lat_spec), read_spec(lon_spec)):
        print write_fortran('2f13.6', lat, lon)


def plot(input_file, output_prefix, magnification):
    """
    Plots a displacement field and saves it to ``.pdf`` files.
    """

    plot_field('figures/' + output_prefix + '.xy.pdf',
               input_file, horizontal=True, magnification=magnification)

    plot_field('figures/' + output_prefix + '.z.pdf',
               input_file, horizontal=False, magnification=magnification)


def uniform(L, W):
    """
    Print out a uniform grid.
    """
    theta, phi = uniform_angle_values(L, W)
    latitudes = 90. - numpy.degrees(theta)
    longitudes = numpy.degrees(phi)

    print len(theta)
    for i in range(len(theta)):
        print write_fortran('2f13.6',
                            latitudes[i], longitudes[i])


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    parser = ArgumentParser(description=__doc__)
    command_help = "task to execute (run with option '-h' for details)"
    subparsers = parser.add_subparsers(dest='command',
                                       help=command_help)

    grid_parser = subparsers.add_parser('grid',
                                        help="prints out a grid")

    format_string = "START,END,NUMBER"
    grid_parser.add_argument('--lat', metavar='RANGE', type=str, required=True,
                             help="latitudes in the format " + format_string)
    grid_parser.add_argument('--lon', metavar='RANGE', type=str, required=True,
                             help="longitudes in the format " + format_string)

    plot_parser = subparsers.add_parser('plot',
                                        help="plots a displacement field")

    plot_parser.add_argument('input',
                             help="the file containing the displament field")

    plot_parser.add_argument('--output-prefix', '-o', metavar='OUT',
                             type=str, default='plot',
                             help="prefix to output .pdf files")

    plot_parser.add_argument('--magnification', '-m',
                             type=float, default=1.,
                             help="magnify arrows")

    uniform_parser = subparsers.add_parser('uniform-grid',
                                           help="print out a uniform grid")

    uniform_parser.add_argument("--latitudes", "-L", type=int, default=40,
                                help="number of latitude divisions")
    uniform_parser.add_argument("--longitudes", "-W", type=int, default=80,
                                help="number of longitude divisions")

    return parser


if __name__ == '__main__':
    autocomplete(argument_parser())
    args = argument_parser().parse_args()

    # command dispatch
    if args.command == 'grid':
        grid(args.lat, args.lon)
    elif args.command == 'plot':
        plot(args.input, args.output_prefix, args.magnification)
    elif args.command == 'uniform-grid':
        uniform(args.latitudes, args.longitudes)
    else:
        raise ValueError
