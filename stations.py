"""
Mathematical functions at observations points calculated on demand.
"""
import numpy
from scipy import special

from utils import lazy
from basic import ii, norm
from basic import geographical_to_epicentral
from basic import angular_to_cartesian, cartesian_to_angular
from basic import spherical_to_cartesian, cartesian_to_spherical

# number of stations to process at once
STATIONS_BATCH = 250

# number of latitudes to process at once for a grid
LAT_BATCH = 1
# number of longitudes to process at once for a grid
LON_BATCH = 250


class Stations(object):
    """
    Calculates mathematical functions at specified array of stations,
    that is, observation points, lazily as needed.
    These stations are points in space as well as time.
    """

    def __init__(self, theta, phi, max_l, max_m=2):
        """
        Store observation points.

        :arg array theta, phi: same-sized arrays of coordinates
                               :math:`(\\theta, \\phi)`
                               on the surface of a sphere
        :arg max_l: maximum required :math:`l`
        :arg max_m: maximum required :math:`m`, defaults to 2
                    since in epicentral coordinates higher modes
                    are not excited
        """

        # indices: [point]
        self.theta = theta
        self.phi = phi

        self.max_m = max_m
        self.max_l = max(max_m, max_l)

        # only m in {0, 1, 2} are needed in epicentral coordinates
        self.m = numpy.arange(max_m + 1)

    @property
    def batches(self):
        """
        Divide the task into batches.
        """
        count = self.shape[0]
        batches = [slice(offset, offset + STATIONS_BATCH)
                   for offset in range(0, count, STATIONS_BATCH)]

        for batch in batches:
            stations_batch = Stations(self.theta[batch], self.phi[batch],
                                      self.max_l, self.max_m)

            yield batch, stations_batch

    @lazy
    def shape(self):
        """ The dimensions of the observation points list. """
        return self.theta.shape

    @lazy
    def sin_theta(self):
        """ :math:`\\sin\\theta` for all given :math:`\\theta`. """
        # indices: [point]
        return numpy.sin(self.theta)

    @lazy
    def cos_theta(self):
        """ :math:`\\cos\\theta` for all given :math:`\\theta`. """
        # indices: [point]
        return numpy.cos(self.theta)

    @lazy
    def sin_phi(self):
        """ :math:`\\sin\\phi` for all given :math:`\\phi`. """
        # indices: [point]
        return numpy.sin(self.phi)

    @lazy
    def cos_phi(self):
        """ :math:`\\cos\\phi` for all given :math:`\\phi`. """
        # indices: [point]
        return numpy.cos(self.phi)

    @lazy
    def exp_iphi(self):
        """ :math:`\\exp i \\phi` for all given :math:`\\phi`. """
        # indices: [point]
        return self.cos_phi + ii * self.sin_phi

    @lazy
    def legendre_and_derivative(self):
        """
        :math:`P_{lm}(\\cos\\theta)`
        and
        :math:`\\frac{d}{dx}P_{lm}(x)` with :math:`x = \\cos\\theta`
        for all required :math:`l, m` and all given :math:`\\theta`.
        """
        # indices: [point, func/deriv, m, l]

        return numpy.array([special.lpmn(self.max_m, self.max_l, x)
                            for x in self.cos_theta])

    @lazy
    def legendre(self):
        """
        :math:`P_{lm}(\\cos\\theta)`
        for all required :math:`l, m` and all given :math:`\\theta`.
        """
        # indices: [point, l, m]
        return numpy.einsum('ijk->ikj',
                            self.legendre_and_derivative[:, 0, :, :])

    @lazy
    def legendre_derivative(self):
        """
        :math:`\\frac{d}{dx}P_{lm}(x)` with :math:`x = \\cos\\theta`
        for all required :math:`l, m` and all given :math:`\\theta`.
        """
        # indices: [point, l, m]
        return numpy.einsum('ijk->ikj',
                            self.legendre_and_derivative[:, 1, :, :])

    @lazy
    def norms(self):
        """
        Normalization constants of :math:`Y_{lm}`
        for all required :math:`l` and :math:`m`
        to ensure that the integral of :math:`|Y_{lm}|^2`
        over the unit sphere is one.
        """
        # indices: [l, m]
        return numpy.array([[norm(l, m) for m in range(self.max_m + 1)]
                            for l in range(self.max_l + 1)])

    @lazy
    def exp_i_m_phi(self):
        """
        :math:`\\exp(i m \\phi)`
        for all required :math:`m` and all given :math:`\\phi`.
        """
        eiphi = self.exp_iphi

        # indices: [point, 1, m]
        return numpy.array([[eiphi ** m] for m in range(self.max_m + 1)]).T
#        return numpy.array([[numpy.ones_like(eiphi)],
#                            [eiphi],
#                            [eiphi * eiphi]]).T

    @lazy
    def X(self):
        """
        :math:`Y_{lm}(\\theta, \\phi) = X_{lm}(\\theta)\\;\\exp(i m \\phi)`
        for all required :math:`l, m,` and all given :math:`\\theta`.
        """
        # indices: [point, l, m]
        return self.norms * self.legendre

    @lazy
    def dX(self):
        """
        :math:`\\frac{d}{d\\theta}X_{lm}(\\theta)`
        for all required :math:`l, m` and all given :math:`\\theta`.
        """
        # indices: [point, l, m]
        return -((self.norms * self.legendre_derivative).T * self.sin_theta).T

    @lazy
    def Y(self):
        """
        Spherical harmonics :math:`Y_{lm}(\\theta, \\phi)`
        for all required :math:`l, m` and the given
        coordinates :math:`(\\theta, \\phi)` of the observation points.
        """
        # indices: [point, l, m]
        return self.X * self.exp_i_m_phi

    @lazy
    def Y_theta(self):
        """
        :math:`\\partial_{\\theta} Y_{lm}(\\theta, \\phi)`
        for all required :math:`l, m` and the given
        coordinates :math:`(\\theta, \\phi)` of the observation points.
        """
        # indices: [point, l, m]
        return self.dX * self.exp_i_m_phi

    @lazy
    def Y_phi(self):
        """
        :math:`\\partial_{\\phi} Y_{lm}(\\theta, \\phi)`
        for all required :math:`l, m` and the given
        coordinates :math:`(\\theta, \\phi)` of the observation points.
        """
        # indices: [point, l, m]
        return ii * self.Y * self.m

    @lazy
    def sin_2phi(self):
        """ :math:`\\sin 2 \\phi` for all given :math:`\\phi`. """
        # indices: [point]
        return numpy.sin(2. * self.phi)

    @lazy
    def cos_2phi(self):
        """ :math:`\\cos 2 \\phi` for all given :math:`\\phi`. """
        # indices: [point]
        return numpy.cos(2. * self.phi)

    def to_epicentral(self, fault):
        """ Move to the epicentral coordinate for a fault. """
        return EpicentralStations(self, fault)


class EpicentralStations(Stations):
    """
    Stations in the epicentral coordinate. Remembers
    the geographical-to-epicentral transformation matrix.
    """

    def __init__(self, geo, fault):
        """
        Creates stations with locations expressed
        in the epicentral coordinate from those in the geographical coordinate
        and a seismic source.

        :arg geo: stations in geographical coordinates
        :arg fault: the source fault
        """

        # remember original data
        self.colatitude = geo.theta
        self.longitude = geo.phi

        # a global rotation matrix that rotates all the stations
        # indices: [axis, axis]
        self.R = geographical_to_epicentral(fault)

        # epicentral cartesian coordinates of the stations
        # indices: [axis, point]
        epi_cart = self.R.dot(angular_to_cartesian(geo.theta, geo.phi))

        # epicentral spherical coordinates of the stations
        # indices: [point]
        theta, phi = cartesian_to_angular(epi_cart)
        super(EpicentralStations, self).__init__(theta, phi,
                                                 geo.max_l, geo.max_m)

    def to_geographical(self, u):
        """
        Rotates the displacement field
        back to the spherical geographical coordinates
        from the spherical epicentral coordinate.
        """
        # u has indices [axis, point, ...]
        # axis can be 0, 1, 2 for r, theta, phi respectively
        # point is the index of the location of the station
        u_xyz = spherical_to_cartesian(self.theta, self.phi, u)

        # R transpose is the inverse of R
        u_xyz_geo = numpy.einsum('ij,j...->i...', self.R.T, u_xyz)

        # this chain of transformations can be avoided with the
        # application of spherical geometry
        # the r component stays the same whereas the horizontal components
        # are rotated by an angle dependent on the geographical locations
        # of the station and the fault
        # Pollitz follows this route
        # while this is arguably simpler to understand
        return cartesian_to_spherical(self.colatitude, self.longitude,
                                      u_xyz_geo)


def mid_points(start, end, divisions):
    """
    Mid-points of interval divisions.
    """
    boundaries, size = numpy.linspace(start, end, divisions,
                                      retstep=True, endpoint=False)
    return boundaries + size / 2.


def uniform_area_angle_values(L=1000, W=2000):
    """
    Creates a uniform grid over the surface of the earth.
    Returns the unique values for :math:`\\theta` and
    :math:`\\phi`.
    """
    z_values = mid_points(1., -1., L)
    theta_values = numpy.arccos(z_values)
    phi_values = mid_points(0., 2. * numpy.pi, W)

    return theta_values, phi_values


def uniform_angle_values(L=1000, W=2000):
    """
    Creates a uniform grid over the surface of the earth.
    Returns the unique values for :math:`\\theta` and
    :math:`\\phi`.
    """
    theta_values = mid_points(0., numpy.pi, L)
    phi_values = mid_points(0., 2. * numpy.pi, W)

    return theta_values, phi_values


def mesh(x_values, y_values):
    """
    Create grid, then flatten.
    """
    x_mesh, y_mesh = numpy.meshgrid(x_values, y_values)
    return numpy.hstack(x_mesh), numpy.hstack(y_mesh)


def uniform_area_grid(latitude_count, longitude_count, max_l, max_m=2):
    """
    Uniform grid over the surface area of the earth.
    """
    theta_values, phi_values = uniform_area_angle_values(latitude_count,
                                                         longitude_count)

    return Grid(theta_values, phi_values, max_l, max_m)


def uniform_angle_grid(latitude_count, longitude_count, max_l, max_m=2):
    """
    Uniform grid over the surface area of the earth.
    """
    theta_values, phi_values = uniform_angle_values(latitude_count,
                                                    longitude_count)

    return Grid(theta_values, phi_values, max_l, max_m)


class Grid(object):
    """
    Mock stations object for uniformly spaced grid points
    in epicentral coordinates.
    """
    def __init__(self, theta_values, phi_values, max_l, max_m=2):
        """
        Create a grid object whose primary purpose
        is to supply the observation points in batches.
        """
        self.theta_values = theta_values
        self.phi_values = phi_values
        self.max_l = max_l
        self.max_m = max_m

    @lazy
    def latitude_count(self):
        """
        Number of divisions in the latitude direction.
        """
        return self.theta_values.shape[0]

    @lazy
    def longitude_count(self):
        """
        Number of divisions in the longitude direction.
        """
        return self.phi_values.shape[0]

    @lazy
    def lat_batches(self):
        """
        Batches of colatitude values for separate processing.
        """
        return [slice(offset, offset + LAT_BATCH)
                for offset in range(0, self.latitude_count,
                                    LAT_BATCH)]

    @lazy
    def lon_batches(self):
        """
        Batches of longitude values for separate processing.
        """
        return [slice(offset, offset + LON_BATCH)
                for offset in range(0, self.longitude_count,
                                    LON_BATCH)]

    @lazy
    def theta(self):
        """
        Observation point colatitudes with batches merged.
        """
        all_values = []

        for lat_batch in self.lat_batches:
            theta_batch = self.theta_values[lat_batch]

            for lon_batch in self.lon_batches:
                phi_batch = self.phi_values[lon_batch]

                theta, _ = mesh(theta_batch, phi_batch)

                all_values.append(theta)

        return numpy.concatenate(all_values)

    @lazy
    def phi(self):
        """
        Observation point longitudes with batches merged.
        """
        all_values = []

        for lat_batch in self.lat_batches:
            theta_batch = self.theta_values[lat_batch]

            for lon_batch in self.lon_batches:
                phi_batch = self.phi_values[lon_batch]

                _, phi = mesh(theta_batch, phi_batch)

                all_values.append(phi)

        return numpy.concatenate(all_values)

    @property
    def shape(self):
        """
        Number of observation points.
        """
        return (self.latitude_count * self.longitude_count,)

    def to_epicentral(self, _):
        """
        Already in epicentral coordinates, so do nothing.
        """
        return self

    @property
    def batches(self):
        """
        Generator object providing stations objects in batches
        for sequential processing.
        """
        processed = 0

        for lat_batch in self.lat_batches:
            theta_values = self.theta_values[lat_batch]

            empty_batch = Stations(theta_values,
                                   numpy.array([]),
                                   self.max_l, self.max_m)
            X = empty_batch.X
            dX = empty_batch.dX

            for lon_batch in self.lon_batches:
                phi_values = self.phi_values[lon_batch]

                theta, phi = mesh(theta_values, phi_values)
                count = theta.shape[0]

                batch = slice(processed, processed + count)
                stations_batch = Stations(theta, phi, self.max_l, self.max_m)

                phi_count = phi_values.shape[0]

                stations_batch.X = numpy.tile(X, (phi_count, 1, 1))
                stations_batch.dX = numpy.tile(dX, (phi_count, 1, 1))

                yield batch, stations_batch
                processed = processed + count

    def to_geographical(self, spherical_u):
        """
        The source is at the north pole, so do nothing.
        """
        _ = self
        return spherical_u
