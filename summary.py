#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK
"""
Display statistics.
"""
import os
import os.path
from argparse import ArgumentParser
import cPickle as pickle

import numpy
from numpy import pi, array, degrees, squeeze, einsum
from numpy.linalg import norm
from scipy.interpolate import interp2d

from frame import MICRO_ARC_SECONDS, PPB
from basic import angular_to_cartesian, spherical_to_cartesian
from basic import cartesian_to_spherical, mode_count
from utils import info, autocomplete, care, lazy
from frame import read_options, shift_in_scale


class Distribution(object):
    """
    Histogram of a random variable.
    """
    def __init__(self, frequencies, bins):
        """
        Create a histogram. It is assumed that the bins are
        all of equal width.
        """
        assert len(frequencies.shape) == 1, "only 1D data is supported"
        assert len(bins.shape) == 1, "only 1D bins are supported"
        widths = bins[1:] - bins[:-1]
        assert numpy.allclose(widths, widths[0]), "bins are not of equal width"
        self.frequencies = frequencies
        self.bins = bins
        self.width = widths[0]
        self.data = (bins[1:] + bins[:-1]) / 2.
        assert numpy.allclose(self.total, 1)

    @property
    def total(self):
        """
        Integral of the distribution.
        """
        return self.frequencies.sum() * self.width

    @classmethod
    def from_dataset(cls, data_set, bin_count=1000, weights=None):
        """
        Create a histogram from a data set.
        """
        frequencies, bins = numpy.histogram(data_set,
                                            bins=bin_count,
                                            weights=weights,
                                            density=True)
        return cls(frequencies, bins)

    @lazy
    def mean(self):
        """
        Mean of the random variable as inferred from the histogram.
        """
        return (self.data * self.frequencies * self.width).sum()

    @lazy
    def std(self):
        """
        Standard deviation of the random variable as inferred from
        the histogram.
        """
        mean = self.mean
        sq = (self.data ** 2 * self.frequencies * self.width).sum()
        return numpy.sqrt(sq - mean ** 2)

    def plot(self, plt):
        """
        Plot the histogram.
        """
        heights = self.frequencies.repeat(2)
        edges = self.bins.repeat(2)[1:-1]
        plt.fill_between(edges, heights, facecolor='steelblue',
                         edgecolor='steelblue')

    def convolve(self, other):
        """
        Convolve the histogram with some other histogram.
        The resulting distribution is that of the sum of the two
        random variables.
        """
        assert numpy.allclose(self.width, other.width)
        width = self.width

        N = self.data.shape[0]
        M = other.data.shape[0]

        frequencies = numpy.convolve(self.frequencies,
                                     other.frequencies) * width
        bins = (self.data[0] + other.data[0] +
                (numpy.arange(N + M) - 0.5) * width)

        return Distribution(frequencies, bins)

    def rescale(self, alpha):
        """
        Rescale the random variable by some constant factor.
        """
        return Distribution(self.frequencies / alpha, alpha * self.bins)

    def samplings(self):
        """
        Generates the sampling distributions of *self* one by one.
        """

        total = self
        N = 1

        while True:
            yield total.rescale(1. / N)
            total = total.convolve(self)
            N = N + 1


class Field(object):
    """
    Displacement field along with position on the sphere.
    """
    def __init__(self, theta, phi, cartesian_u):
        """
        Create a displacement field.
        """
        self.theta = theta
        self.phi = phi
        self.cartesian_u = cartesian_u

    @classmethod
    def from_spherical(cls, theta, phi, spherical_u):
        """
        Create a displacement field where the displacements
        are given in spherical coordinates.
        """
        return cls(theta, phi,
                   spherical_to_cartesian(theta, phi, spherical_u))

    @property
    def spherical_u(self):
        """
        Converts field to spherical coordinates.
        """
        return cartesian_to_spherical(self.theta, self.phi,
                                      self.cartesian_u)

    @property
    def geographical_u(self):
        """
        Converts to geographical (E, N, U) coordinates.
        """
        return geographical_to_spherical(self.spherical_u)

    def cartesian_w(self, radius):
        """
        An auxiliary field for EOP calculations.
        """
        u = self.cartesian_u
        r = angular_to_cartesian(self.theta, self.phi)
        integrand = 3. * numpy.cross(r.T, u.T).T / 2.

        return MICRO_ARC_SECONDS * integrand / (radius * 1.e+5)

    def cartesian_s(self, radius):
        """
        An auxiliary field for scale calculations.
        """
        u = self.cartesian_u
        r = angular_to_cartesian(self.theta, self.phi)
        integrand = array([einsum('ij...,ij...->j...', r, u)])

        return PPB * integrand / (radius * 1.e+5)

    def method_weights(self, method):
        """
        Weights of the data points for the method of data collection.
        """
        if method == 'monte-carlo' or method == 'area-grid':
            return numpy.ones_like(self.theta)
        elif method == 'angle-grid':
            return numpy.sin(self.theta)
        else:
            raise ValueError

    def u_mean(self, options):
        r"""
        Population mean of the :math:`\mathbf{u}` field.
        """
        weights = self.method_weights(options["method"])

        return weighted_mean(self.cartesian_u, weights)

    def u_std(self, options):
        r"""
        Population standard deviation of the :math:`\mathbf{u}` field.
        """
        weights = self.method_weights(options["method"])

        return weighted_std(self.cartesian_u, weights)

    def w_mean(self, options):
        r"""
        Population mean of the :math:`\mathbf{w}` field.
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        w = self.cartesian_w(radius)

        return weighted_mean(w, weights)

    def w_std(self, options):
        r"""
        Population standard deviation of the :math:`\mathbf{w}` field.
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        w = self.cartesian_w(radius)

        return weighted_std(w, weights)

    def s_mean(self, options):
        r"""
        Population mean of the :math:`{s}` field.
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        s = self.cartesian_s(radius)

        return weighted_mean(s, weights, axis=1)

    def s_std(self, options):
        r"""
        Population standard deviation of the :math:`{s}` field.
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        s = self.cartesian_s(radius)

        return weighted_std(s, weights, axis=1)

    def u_distributions(self, options, bin_count=1000):
        """
        Distributions of displacement components (in cm).
        """
        u = self.cartesian_u
        weights = self.method_weights(options["method"])

        return [Distribution.from_dataset(u[i, :],
                                          bin_count=bin_count,
                                          weights=weights)
                for i in range(3)]

    def w_distributions(self, options, bin_count=1000):
        """
        Distributions of rotation components (in μas).
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        w = self.cartesian_w(radius)

        return [Distribution.from_dataset(w[i, :],
                                          bin_count=bin_count,
                                          weights=weights)
                for i in range(3)]

    def s_distributions(self, options, bin_count=1000):
        """
        Distributions of the scale components (in ppb).
        """
        weights = self.method_weights(options["method"])
        radius = options["model"]["radius"]

        s = self.cartesian_s(radius)

        return [Distribution.from_dataset(s[0, :],
                                          bin_count=bin_count,
                                          weights=weights)]

    def select(self, criteria):
        """
        Create another field with only those points that
        satisfy the given *criteria*.
        """
        selected = criteria(self.theta, self.phi, self.cartesian_u)

        return Field(self.theta[selected], self.phi[selected],
                     self.cartesian_u[:, selected])

    def interpolate(self, options):
        """
        Interpolate field values.
        """
        def convert(data):
            """
            Convert 1D data to 2D grid.
            """
            return numpy.reshape(data,
                                 (options['longitudes'], options['latitudes']),
                                 order='F')

        # add the north and the south poles
        theta = numpy.concatenate((array([0.]),
                                   convert(self.theta)[0, :],
                                   array([pi])))

        # wrap values around to make it periodic
        phi = numpy.concatenate((array([self.phi[-1] - 2. * pi]),
                                 convert(self.phi)[:, 0],
                                 array([self.phi[0] + 2. * pi])))

        def pad(u):
            """
            Pad array with extra elements at the borders to
            enforce boundary conditions.
            """
            pad_phi = numpy.concatenate((u[[-1], :], u, u[[0], :]))
            north = u[:, 0].mean()
            south = u[:, -1].mean()
            north_pad = array([numpy.repeat(north, pad_phi.shape[0])]).T
            south_pad = array([numpy.repeat(south, pad_phi.shape[0])]).T
            return numpy.concatenate((north_pad, pad_phi, south_pad), axis=1)

        def cubic(u):
            """ Cubic interpolation of some field. """
            return interp2d(theta, phi, pad(convert(u)), kind='cubic')

        if len(self.cartesian_u.shape) == 2:
            # snapshot data
            components = [cubic(self.cartesian_u[i, :])
                          for i in range(3)]

            def result(theta, phi):
                """ The interpolated function.  """
                return numpy.array([squeeze(components[i](theta, phi))
                                    for i in range(3)]).reshape(3)

            return result

        elif len(self.cartesian_u.shape) == 3:
            _, _, n = self.cartesian_u.shape

            components = [[cubic(self.cartesian_u[i, :, t])
                           for t in range(n)]
                          for i in range(3)]

            def result(theta, phi):
                """ The interpolated function.  """
                return numpy.array([[squeeze(components[i][t](theta, phi))
                                     for t in range(n)]
                                    for i in range(3)])

            return result

        else:
            raise ValueError("the shape of u not understood")

    def write(self, output_file):
        """
        Write out a displacement field to a file.
        """
        u = geographical_to_spherical(self.spherical_u)
        latitudes = 90. - degrees(self.theta)
        longitudes = degrees(self.phi)

        with open(output_file, 'w') as fl:
            for i in range(latitudes.shape[0]):
                lat, lon = latitudes[i], longitudes[i]
                v = u[:, i]

                fl.write("{:.3f}\t{:.3f}\t{:.4e}\t{:.4e}\t{:.4e}\n"
                         .format(lat, lon, v[0], v[1], v[2]))

    def write_raw(self, output_file):
        """
        Write out a displacement field to a file
        without changing coordinates.
        """
        colatitudes = degrees(self.theta)
        longitudes = degrees(self.phi)
        u = self.cartesian_u

        with open(output_file, 'w') as fl:
            for i in range(colatitudes.shape[0]):
                colat, lon = colatitudes[i], longitudes[i]
                v = u[:, i]

                fl.write("{:.3f}\t{:.3f}\t{:.4e}\t{:.4e}\t{:.4e}\n"
                         .format(colat, lon, v[0], v[1], v[2]))


def geographical_to_spherical(geographic_u):
    """
    Converts field to spherical coordinates.
    """
    east = geographic_u[0, ...]
    north = geographic_u[1, ...]
    up = geographic_u[2, ...]

    return numpy.array([up, -north, east])


def read_data(folder, log_file='field.log'):
    """
    Read grid data from folder.
    """
    try:
        return binary_to_field(folder)

    except IOError:
        log_file = os.path.join(folder, log_file)
        theta, phi, east, north, up = numpy.loadtxt(log_file, unpack=True)

        theta = numpy.radians(90. - theta)
        phi = numpy.radians(phi)
        geo_u = numpy.array([east, north, up])
        spherical_u = geographical_to_spherical(geo_u)
        cartesian_u = spherical_to_cartesian(theta, phi, spherical_u)

        return Field(theta, phi, cartesian_u)


def weighted_mean(data_set, weights, axis=1):
    """
    Weighted mean of a data set.
    """
    return numpy.average(data_set, axis=axis, weights=weights)


def convert_to_binary(folder, filename='field'):
    """
    Convert folder content (.log file) to binary (.npz file).
    """
    field = read_data(folder, log_file=filename + '.log')

    numpy.savez_compressed(os.path.join(folder, filename + '.npz'),
                           theta=field.theta,
                           phi=field.phi,
                           u=field.cartesian_u)


def binary_to_field(folder, npz_file='field.npz'):
    """
    Read field from the binary (.npz) format.
    """
    npz_file = numpy.load(os.path.join(folder, npz_file))
    return Field(npz_file["theta"],
                 npz_file["phi"],
                 npz_file["u"])


def convert_directory_to_binary(directory, remove=True):
    """
    Convert every field in a directory to binary.
    """
    for folder, _, files in os.walk(directory):
        if 'field.log' in files:
            print folder
            convert_to_binary(folder)
            if remove:
                os.remove(os.path.join(folder, 'field.log'))


def weighted_std(data_set, weights, mean=None, axis=1):
    """
    Weighted standard deviation of a data set.
    """
    if mean is None:
        mean = weighted_mean(data_set, weights, axis=axis)

    if axis is None:
        squares = (data_set - mean) ** 2
    else:
        squares = (data_set.T - mean).T ** 2

    variance = numpy.average(squares, axis=axis,
                             weights=weights)
    return numpy.sqrt(variance)


def weighted_magnitude_std(data_set, weights, mean=None):
    """
    Weighted standard deviation of the magnitude of a vector data set.
    """
    if mean is None:
        mean = weighted_mean(data_set, weights)

    deviations = numpy.sum((data_set.T - mean) ** 2, axis=1)

    variance = numpy.average(deviations, weights=weights)
    return numpy.sqrt(variance)


def display_raw_statistics(field, options):
    """
    Statistics of a field directly from data.
    """
    radius = options["model"]["radius"]
    method = options["method"]

    weights = field.method_weights(method)
    u = field.cartesian_u

    def details(header, data_set):
        """
        Show key statistics.
        """
        info("{}\n", header)

        mean = weighted_mean(data_set, weights)
        std = weighted_std(data_set, weights, mean=mean)
        mag_std = numpy.array([weighted_magnitude_std(data_set,
                                                      weights, mean=mean)])

        info("   mean {}\n", repr(mean))
        info("    std {}\n", repr(std))
        info("    mag {}\n", repr(mag_std))

        info("\n")

        return std

    w = field.cartesian_w(radius)
    s = field.cartesian_s(radius)

    # in cm, μas, just in case
    return (details("origin (mm):", u * 10.) / 10., details("EOP (μas):", w),
            details("scale (ppb):", s))


def display_distribution_statistics(field, options, bins):
    """
    Statistics of a field from its frequency distribution.
    """
    def details(header, dists):
        """
        Show key statistics.
        """
        info("{}\n", header)

        means = [dists[i].mean for i in range(3)]
        info("    mean [{:.5f}, {:.5f}, {:.5f}]\n".format(*means))
        stds = [dists[i].std for i in range(3)]
        info("     std [{:.5f}, {:.5f}, {:.5f}]\n".format(*stds))
        info("\n")

    details("origin (cm):", field.u_distributions(options, bin_count=bins))
    details("EOP (μas):", field.w_distributions(options, bin_count=bins))


def display_statistics(data, options, bins):
    """
    Display statistics.
    """
    info("raw\n")
    display_raw_statistics(data, options)

    info("\n")

    info("histogram\n")
    display_distribution_statistics(data, options, bins)


def standard_deviation_T(spheroidal, toroidal, T_CF, radius):
    """
    Standard deviation of the shift for :math:`N=1`.
    """
    u_2 = 0.

    for l, m in sorted(spheroidal):
        y = spheroidal[l, m]

        u_2 += mode_count(m) * numpy.absolute(y[0]) ** 2
        if l >= 1:
            u_2 += mode_count(m) * l * (l + 1) * numpy.absolute(y[2]) ** 2

    for l, m in sorted(toroidal):
        y = toroidal[l, m]

        u_2 += mode_count(m) * l * (l + 1) * numpy.absolute(y[0]) ** 2

    u_2 *= (radius * 1.e+5) ** 2

    variance = u_2 / (4. * numpy.pi) - T_CF.dot(T_CF)
    stddev = numpy.sqrt(variance)
    return stddev


def standard_deviation_R(spheroidal, toroidal, R_CF):
    """
    Standard deviation of the orientation for :math:`N=1`.
    """
    w_2 = 0.

    for l, m in sorted(spheroidal):
        y = spheroidal[l, m]

        if l >= 1:
            w_2 += mode_count(m) * l * (l + 1) * numpy.absolute(y[2]) ** 2

    for l, m in sorted(toroidal):
        y = toroidal[l, m]

        w_2 += mode_count(m) * l * (l + 1) * numpy.absolute(y[0]) ** 2

    w_2 *= (3. / 2.) ** 2 * MICRO_ARC_SECONDS ** 2

    variance = w_2 / (4. * numpy.pi) - R_CF.dot(R_CF)
    stddev = numpy.sqrt(variance)
    return stddev


def standard_deviation_D(spheroidal, D_CF):
    """
    Standard deviation of the scale for :math:`N=1`.
    """
    s_2 = 0.

    for l, m in sorted(spheroidal):
        y = spheroidal[l, m]

        s_2 += numpy.absolute(y[0]) ** 2

    s_2 *= PPB ** 2

    variance = s_2 / (4. * numpy.pi) - D_CF ** 2
    stddev = numpy.sqrt(variance)
    return stddev


def read_love_numbers(folder, love_numbers_file):
    """
    Read pickled Love numbers from file.
    """
    filename = os.path.join(folder, love_numbers_file)
    with open(filename) as fl:
        love_numbers = pickle.load(fl)

    assert len(love_numbers) == 1, 'multiple sources not supported'

    return love_numbers[0]


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    parser = ArgumentParser(description=__doc__)

    parser.add_argument('--output', '-o', default='../data', type=str,
                        help="output directory")
    parser.add_argument('--debug', '-d', action='count', default=0,
                        help='emit debug information')

    parser.add_argument('--bins',
                        type=int, default=1000,
                        help="number of bins in histogram")

    parser.add_argument('--love-numbers-file', type=str,
                        help="read love numbers from the given file")
    parser.add_argument('--name', '-n', required=True,
                        help="name of the experiment")

    return parser


def main(args):
    """
    Display summary without collecting data.
    """
    def single_folder(folder):
        """
        Display statistics for a single folder.
        """
        info(("-" * 30) + "\n")
        info("in folder {}\n".format(folder))
        info(("-" * 30) + "\n")

        depth = folder.split(os.sep)[-1]

        options = read_options(folder)

        if args.love_numbers_file:
            spheroidal, toroidal = read_love_numbers(folder,
                                                     args.love_numbers_file)
        else:
            spheroidal, toroidal = None, None

        radius = options['model']['radius']
        T_CF = numpy.array(options['shift']['origin'])
        R_CF = numpy.array(options['shift']['EOP'])

        info("expected shift:\n")
        info("origin (mm): {}\n".format(repr(T_CF * 10.)))
        info("  EOP (μas): {}\n".format(repr(R_CF)))
        if spheroidal is not None:
            D_CF = shift_in_scale(spheroidal) * PPB
            info("scale (ppb): {}\n".format(D_CF))
        else:
            D_CF = None
        info("\n")

        if spheroidal is not None and toroidal is not None:
            T_std = numpy.array(standard_deviation_T(spheroidal, toroidal,
                                                     T_CF, radius))
            R_std = numpy.array(standard_deviation_R(spheroidal, toroidal,
                                                     R_CF))
            D_std = numpy.array(standard_deviation_D(spheroidal, D_CF))

            info(" T (mm): {}\n".format(repr(T_std * 10.)))
            info("R (μas): {}\n".format(repr(R_std)))
            info("D (ppb): {}\n".format(repr(D_std)))
            info("\n")
        else:
            T_std = None
            R_std = None
            D_std = None

        (std_u, std_w,
         std_s) = display_raw_statistics(read_data(folder), options)
        info(("-" * 30) + "\n\n")

        # means are in mm, μas, ppb, errors are in m, mas, ppm
        return " | ".join(["", depth,
                           str(T_CF * 10.),
                           str(array([norm(std_u) * 10. / 1000.])),
                           str(T_std * 10. / 1000.),
                           str(D_CF),
                           str(array([std_s / 1000.])),
                           str(D_std / 1000.),
                           str(R_CF),
                           str(array([norm(std_w) / 1000.])),
                           str(R_std / 1000.), ""])

    lines = []
    mother_folder = os.path.join(os.path.expanduser(args.output), args.name)
    for folder, _, files in os.walk(mother_folder):
        if 'options.json' in files:
            lines.append(single_folder(folder))

    headers = ["", "depth", "T_CF", "std(u)", "<std(u)>",
               "D_CF", "std(s)", "<std(s)>",
               "R_CF", "std(w)", "<std(w)>", ""]
    info(" | ".join(headers) + "\n")
    info("\n".join([line.replace('[', '\\(').replace(']', '\\)')
                    for line in lines]))
    info("\n")


if __name__ == '__main__':
    numpy.set_printoptions(precision=3, suppress=True)
    command_line_parser = argument_parser()
    autocomplete(command_line_parser)

    # raise errors if encountered with invalid numerical operations
    # rather than failing silently
    with care:
        main(command_line_parser.parse_args())
