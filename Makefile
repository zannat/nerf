.PHONY: clean
clean:
	rm -f *.pyc *.log *.out* *.in* tmp

.PHONY: clean-all
clean-all: clean
	rm -f bin/* figures/*
