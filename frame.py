#!/usr/bin/env python
"""
Functionality related to calculating reference frame parameters.
"""
import os
import cPickle as pickle

import numpy
from numpy.linalg import inv
from scipy.sparse import diags

from basic import spherical_to_cartesian, cartesian_to_spherical
from basic import angular_to_cartesian, ii
from basic import geographical_to_epicentral
from utils import Record

from quake import read_faults
from utils import info, read_json, write_json

from voronoi import Voronoi_decomposition, areas


# number of Monte Carlo runs
NUMBER_OF_TRIALS = 100

# moment tensor in our units
# this is M_w 9.0
M_0 = 1.897e-9

# conversion factor from radians to micro arc seconds
MICRO_ARC_SECONDS = 180. * 60. * 60. * 1.e+6 / numpy.pi

# parts per billion
PPB = 1.e+9

# the normalization is from Stein, Wysession (2009)
unit_moments = {
    'iso': numpy.array([[1., 0., 0.],
                        [0., 1., 0.],
                        [0., 0., 1.]]) / numpy.sqrt(3.),

    'dc-1': numpy.array([[0., 1., 0.],
                         [1., 0., 0.],
                         [0., 0., 0.]]) / numpy.sqrt(2.),

    'dc-2': numpy.array([[0., 0., 0.],
                         [0., 0., 1.],
                         [0., 1., 0.]]) / numpy.sqrt(2.),

    'dc-3': numpy.array([[0., 0., 1.],
                         [0., 0., 0.],
                         [1., 0., 0.]]) / numpy.sqrt(2.),

    'clvd-1': numpy.array([[1., 0., 0.],
                           [0., -2., 0.],
                           [0., 0., 1.]]) / numpy.sqrt(6.),

    'clvd-2': numpy.array([[1., 0., 0.],
                           [0., 1., 0.],
                           [0., 0., -2.]]) / numpy.sqrt(6.)
}


def constant_translation(theta, phi, vector):
    """
    Displacement field of rigid translation.
    A basic test case.
    """
    return cartesian_to_spherical(theta, phi,
                                  numpy.array(vector)[:, numpy.newaxis])


def constant_rotation(theta, phi, omega):
    """
    Displacement field of rigid rotation.
    Another test case.
    """
    cartesian_u = numpy.cross(numpy.array(omega),
                              angular_to_cartesian(theta, phi).T).T
    return cartesian_to_spherical(theta, phi, cartesian_u)


def summation_shift(field, radius, Voronoi=False):
    """
    Calculates the shift in origin and EOPs
    where the integration weight is either constant or given by
    the Voronoi cell area.
    """
    theta = field.theta
    phi = field.phi
    cartesian_u = field.cartesian_u

    # radius should be in cm
    r_hat = angular_to_cartesian(theta, phi)
    r = r_hat * radius

    u = cartesian_u
    _, N = r.shape

    if Voronoi:
        d_A = areas(Voronoi_decomposition(r_hat)) / (4. * numpy.pi)
        assert numpy.allclose(d_A.sum(), 1.)

        if len(cartesian_u.shape) == 3:
            d_A = d_A[:, numpy.newaxis]
    else:
        d_A = 1. / N

    T = (u * d_A).sum(axis=1)
    D = (numpy.einsum('ij,ij...->j...', r, u) * d_A / radius ** 2).sum(axis=0)
    R = ((3. * numpy.cross(r.T, u.T).T / (2 * radius ** 2)) * d_A).sum(axis=1)

    return T, D * PPB, R * MICRO_ARC_SECONDS


def transformation_shift(field, radius, Voronoi=False):
    """
    Calculates the shift in all the Helmert parameters all at once
    by the design matrix approach.
    """
    cartesian_u = field.cartesian_u

    # radius should be in cm
    r_hat = angular_to_cartesian(field.theta, field.phi)

    _, N = r_hat.shape

    def design_matrix():
        """
        The matrix for the overdetermined system.
        """
        def design_row(point):
            """
            Return three rows of the design matrix that the
            given point specifies.
            """
            x, y, z = point
            yield [1., 0., 0., x, 0., z, -y]
            yield [0., 1., 0., y, -z, 0., x]
            yield [0., 0., 1., z, y, -x, 0.]

        return numpy.array([row
                            for i in range(N)
                            for row in design_row(r_hat[:, i] * radius)])

    def weight_matrix():
        """
        The weights for the stations.
        """
        if Voronoi:
            d_Omega = areas(Voronoi_decomposition(r_hat))

        def weight_row(index):
            """
            Returns three rows in the weight matrix.
            """
            rows = numpy.zeros((3, 3 * N))

            if Voronoi:
                weight = d_Omega[index]
            else:
                weight = 1.

            rows[0, 3 * index] = weight
            rows[1, 3 * index + 1] = weight
            rows[2, 3 * index + 2] = weight

            yield rows[0]
            yield rows[1]
            yield rows[2]

        return numpy.array([row
                            for i in range(N)
                            for row in weight_row(i)])

    A = design_matrix()
    W = weight_matrix()

    params = (inv(A.T.dot(W).dot(A))
              .dot(A.T).dot(W)
              .dot(cartesian_u.ravel(order='F')))

    T, D, R = params[:3], params[3], params[4:]
    return T, D * PPB, R * MICRO_ARC_SECONDS


def transformation_shift_timeseries(field, radius, dt, Voronoi=False):
    """
    Calculates the shift in all the Helmert parameters all at once
    by the design matrix approach.
    """
    cartesian_u = field.cartesian_u

    # radius should be in cm
    r_hat = angular_to_cartesian(field.theta, field.phi)

    def design_matrix():
        """
        The matrix for the overdetermined system.
        """
        def design_row(point, t):
            """
            Return three rows of the design matrix that the
            given point specifies.
            """
            x, y, z = point
            A = [1., 0., 0., x, 0., z, -y, t, 0., 0., t * x, 0., t * z, -t * y]
            B = [0., 1., 0., y, -z, 0., x, 0., t, 0., t * y, -t * z, 0., t * x]
            C = [0., 0., 1., z, y, -x, 0., 0., 0., t, t * z, t * y, -t * x, 0.]
            yield A
            yield B
            yield C

        _, N, n = cartesian_u.shape

        return numpy.array([row
                            for t in range(n)
                            for i in range(N)
                            for row in design_row(r_hat[:, i] * radius,
                                                  dt[t])])

    def weight_matrix():
        """
        The weights for the stations.
        """
        _, N, n = cartesian_u.shape

        if Voronoi:
            d_Omega = areas(Voronoi_decomposition(r_hat))
            diagonal = numpy.tile(numpy.repeat(d_Omega, 3), n)
        else:
            diagonal = numpy.ones(3 * N * n)

        return diags(diagonal)

    A = design_matrix()
    W = weight_matrix()

    def break_up(params):
        """ Break up Helmert parameters into components. """
        MAS = MICRO_ARC_SECONDS

        u, s, w = params[:3], [params[3] * PPB], params[4:7] * MAS
        v, sdot, omega = params[7:10], [params[10] * PPB], params[11:] * MAS

        return (u, s, w), (v, sdot, omega)

    return break_up((inv(A.T.dot(W.dot(A)))
                     .dot(A.T).dot(W.dot(cartesian_u.ravel(order='F')))))


def shift_in_Helmert_parameters(theta, phi, spherical_u, args, earth):
    """
    Calculates the shift in all the Helmert parameters all at once
    by the design matrix approach.
    """
    field = Record(theta=theta, phi=phi,
                   cartesian_u=spherical_to_cartesian(theta, phi,
                                                      spherical_u))

    # earth radius in km
    return transformation_shift(field, earth.radius * 1.e+5, args.Voronoi)


def shift_in_origin(spheroidal):
    """
    Shift in origin for a point source
    in the epicentral coordinates.
    """
    pi = numpy.pi
    sqrt = numpy.sqrt

    y10 = spheroidal.get((1, 0), numpy.zeros((4,)))
    y11 = spheroidal.get((1, 1), numpy.zeros((4,)))
    x = -2. * (y11[0] + 2. * y11[2])
    y = -2. * ii * (y11[0] + 2. * y11[2])
    z = sqrt(2.) * (y10[0] + 2. * y10[2])
    return sqrt(2. * pi / 3.) * numpy.array([x, y, z]).real / (4. * pi)


def shift_in_scale(spheroidal):
    """
    Shift in the scale parameter for a point source
    """
    pi = numpy.pi
    sqrt = numpy.sqrt

    y00 = spheroidal.get((0, 0), numpy.zeros((2,)))
    return y00[0].real / (2. * sqrt(pi))


def theoretical_shift_in_origin(earthquake):
    """
    Theoretical shift in origin.
    """
    result = numpy.array([0., 0., 0.])
    earth = earthquake.earth

    try:
        pickle_file = earthquake.love_numbers_file
        if pickle_file is None:
            raise TypeError

        with open(pickle_file) as fl:
            info("loading from pickle file {} for theory\n"
                 .format(pickle_file))
            all_co_modes = pickle.load(fl)

    except (IOError, TypeError):
        info("calculating love numbers for theory\n")
        all_co_modes = earthquake.co_modes

    for co_mode_index, horizontal_piece in enumerate(earthquake.faults):
        spheroidal, _ = all_co_modes[co_mode_index]

        # in cm
        shift = shift_in_origin(spheroidal) * earth.radius * 1.e+5

        for fault in horizontal_piece:
            R = geographical_to_epicentral(fault)
            result += R.T.dot(shift)

    return result


def shift_in_EOPs(toroidal):
    """
    Shift in EOP for a point source
    in the epicentral coordinates.
    """
    pi = numpy.pi
    sqrt = numpy.sqrt

    y10 = toroidal.get((1, 0), numpy.zeros((2,)))
    y11 = toroidal.get((1, 1), numpy.zeros((2,)))
    x = -2. * y11[0]
    y = -2. * ii * y11[0]
    z = sqrt(2.) * y10[0]
    return (2. * 3. * sqrt(2. * pi / 3.) *
            numpy.array([x, y, z]).real / (8. * pi))


def theoretical_shift_in_EOPs(earthquake):
    """
    Theoretical shift in earth orientation parameters.
    """
    result = numpy.array([0., 0., 0.])

    try:
        pickle_file = earthquake.love_numbers_file
        if pickle_file is None:
            raise TypeError

        with open(pickle_file) as fl:
            info("loading from pickle file {} for theory\n"
                 .format(pickle_file))
            all_co_modes = pickle.load(fl)

    except (IOError, TypeError):
        info("calculating love numbers for theory\n")
        all_co_modes = earthquake.co_modes

    for co_mode_index, horizontal_piece in enumerate(earthquake.faults):
        _, toroidal = all_co_modes[co_mode_index]

        # in micro arc seconds
        shift = shift_in_EOPs(toroidal) * MICRO_ARC_SECONDS

        for fault in horizontal_piece:
            R = geographical_to_epicentral(fault)
            result += R.T.dot(shift)

    return result


def theoretical_shift_in_scale(earthquake):
    """
    Theoretical shift in scale.
    """
    result = 0.

    try:
        pickle_file = earthquake.love_numbers_file
        if pickle_file is None:
            raise TypeError

        with open(pickle_file) as fl:
            info("loading from pickle file {} for theory\n"
                 .format(pickle_file))
            all_co_modes = pickle.load(fl)

    except (IOError, TypeError):
        info("calculating love numbers for theory\n")
        all_co_modes = earthquake.co_modes

    for co_mode_index, horizontal_piece in enumerate(earthquake.faults):
        spheroidal, _ = all_co_modes[co_mode_index]

        # in parts per billion
        shift = shift_in_scale(spheroidal) * PPB

        for _ in horizontal_piece:
            result += shift

    return result


def theoretical_shifts(earthquake):
    """
    Both shifts together.
    """
    return {'origin': theoretical_shift_in_origin(earthquake).tolist(),
            'EOP': theoretical_shift_in_EOPs(earthquake).tolist(),
            'scale': theoretical_shift_in_scale(earthquake)}


def standard_fault(kind, radius, colatitude=0., longitude=0.):
    """
    Create a magnitude 9.0 earthquake.

    :arg kind: one of the unit moment names
    :arg radius: radius of the source in earth radius units
    :arg colatitude: colatitude in radians
    :arg longitude: longitude in radians
    """
    return Record(radius=radius, M=M_0 * unit_moments[kind],
                  colatitude=colatitude, longitude=longitude)


def parse_faults(args, earth):
    """
    Parse fault specification from *args*.
    See [SteinWysession2009]_.
    """
    if args.fault is not None:
        return read_faults(args.fault, earth)[0]
    elif args.depth is not None and args.moment is not None:
        radius = 1. - args.depth / earth.radius
        return [standard_fault(args.moment, radius)]
    else:
        raise ValueError("no seismic source given")


def process_options(args, faults, earth):
    """
    Extract details of the experiment.
    """
    result = dict(vars(args))
    result['output'] = os.path.expanduser(result['output'])
    result['model'] = {
        "units": earth.units,
        "radius": earth.radius,
        "layers": str(earth.unique_layers()).split('\n')
    }

    if result['task'] == 'co':
        result['task'] = 'coseismic'
    elif result['task'] == 'post':
        result['task'] = 'postseismic'
    elif result['task'] == 'both':
        result['task'] = 'combined'

    if 'depth' in result:
        del result['depth']
    if 'fault' in result:
        del result['fault']
    if 'moment' in result:
        del result['moment']
    if 'debug' in result:
        del result['debug']
    if 'point_source' in result:
        del result['point_source']

    def fault_details(fault):
        """
        Details of a point-like fault segment.
        """
        return {
            'moment': fault.M.tolist(),
            'source_radius': fault.radius,
            'colatitude': fault.colatitude,
            'longitude': fault.longitude
        }

    result['faults'] = [[fault_details(fault) for fault in horizontal_piece]
                        for horizontal_piece in faults]

    return result


def read_data(folder):
    """
    Read data collected so far.
    """
    try:
        result = read_json(os.path.join(folder, 'raw_data.json'))
        info("read data\n")
        return result
    except IOError:
        info("no previous data\n")
        return []


def read_options(folder):
    """
    Read details of the experiment.
    """
    return read_json(os.path.join(folder, 'options.json'))


def write_options(folder, options):
    """
    Write details of the experiment.
    """
    def ignore_trivials(dictionary):
        """
        Ignore the trivial entries in the dictionary.
        """
        return {key: value
                for key, value in dictionary.iteritems()
                if key not in ['trials']}

    try:
        old_options = read_options(folder)
        if ignore_trivials(old_options) != ignore_trivials(options):
            print 'warning, old options differ from current options'
            for key in set(old_options.keys() + options.keys()):
                if key not in old_options:
                    print key, 'is missing in old'
                elif key not in options:
                    print key, 'is missing in new'
                elif old_options[key] != options[key]:
                    print 'key:', key
                    print 'old value'
                    print old_options[key]
                    print 'new value'
                    print options[key]
        else:
            info("option list is intact\n")
    except IOError:
        write_json(os.path.join(folder, 'options.json'), options)
        info("wrote down option list\n")
