"""
Utility functions related to earth layers.
"""
import itertools
import copy
import numpy
from numpy import linalg
from utils import read_fortran, lazy
from layers import Layer, MaxwellRheology, BurgersRheology
from layers import StaticSpheroidal, StaticToroidal
from layers import StaticSpheroidalDegree0


# physical constants in SI units
G = 6.67408e-11
earth_mass = 5.97219e+24
earth_radius = 6.371e+6
hour = 3.6e3
year = 3.15576e7

# radius of the excluded sphere as a portion of the earth radius
# about 64 km
EXCLUDED_CENTER_RADIUS = 0.01

# maximum allowed thickness of a layer as a portion of the earth radius
# for the purposes of sampling the radial solution for numerical integration
# about 6.4 km
SAMPLING_WIDTH = 0.001

# resolution for numerical propagation of radial solution
# about 320 m
NUMERICAL_SAMPLING_WIDTH = 0.00005

# minimum thickness a synthetic layer must have to be included
MINIMUM_SYNTHETIC_THICKNESS = 0.005

# expected maximum order of magnitude of elements
# of radial solution vector and matrizant matrices
EXPECTED_MAGNITUDE = 50.

# a layer needs to have viscosity eta below this fraction
# with respect to the viscosity of the top-most layer
# in order to qualify as a "viscoelastic" layer
VISCOSITY_RATIO = 0.001

# the radial solutions are defined only upto a constant factor
# MAX_NORM is the maximum norm allowed by this program
# if the norm gets larger than MAX_NORM, we will rescale it
MAX_NORM = 1.e+25


def split_layer(layer, r):
    """
    Splits a layer at a given radius.
    Does nothing if the radius is not inside the layer.
    """

    if r not in layer:
        return [layer]

    if abs(r - layer.top) == 0. or abs(r - layer.bottom) == 0.:
        return [layer]

    layer_above = copy.deepcopy(layer)
    layer_below = copy.deepcopy(layer)

    layer_above.bottom = r
    layer_below.top = r

    return [layer_below, layer_above]


def conversion_factors(units, radius):
    """
    Conversion factors for different unit systems.
    The inputs are from Pollitz's earth model files.
    """
    L = radius * 1.e+3
    M = earth_mass
    T = hour

    factors = {
        'SI': {
            'length': 1.e+3,
            'density': 1.e+3,
            'elastic': 1.e+10,
            'visco': 1.e+18,
            'G': 1.
        },

        'scaled': {
            'length': 1.e+3 / L,
            'density': 1.e+3 * (L ** 3) / M,
            'elastic': 1.e+10 * (L * T ** 2) / M,
            'visco': 1.e+18 * (L * T) / M,
            'G': M / (L ** 3)
        }
    }

    return factors[units]


def read_layer(line, radius, units="SI"):
    """
    Reads the physical properties of individual earth layers for
    different rheologies in Pollitz's file format.
    """
    factors = conversion_factors(units, radius)

    mu_prime = None
    eta1 = None

    # bottom and top are in km
    # density is in g / cm ** 3
    # kappa, mu, mu_prime are in 10 ** 10 Pa
    # eta, eta1 are in 10 ** 18 Pa-s
    try:
        # Burgers material
        (bottom, top, density,
         kappa, mu, mu_prime, eta, eta1) = read_fortran('6f9.3,2e13.6e2',
                                                        line)
    except ValueError:
        try:
            # standard linear solid
            (bottom, top, density,
             kappa, mu, mu_prime, eta) = read_fortran('6f9.3,e13.6e2',
                                                      line)
        except ValueError:
            # Maxwell or Kelvin (only Maxwell is implemented)
            (bottom, top, density,
             kappa, mu, eta) = read_fortran('5f9.3,e13.6e2',
                                            line)

    # convert physical properties to appropriate units
    # the argument names will become attributes of the Layer object
    result = Layer(bottom=(bottom * factors['length']),
                   top=(top * factors['length']),
                   density=(density * factors['density']),
                   kappa=(kappa * factors['elastic']),
                   mu=(mu * factors['elastic']),
                   eta=(eta * factors['visco']))

    if mu_prime is not None:
        result.mu_prime = mu_prime * factors['elastic']

    if eta1 is not None:
        result.eta1 = eta1 * factors['visco']

    return result


def read_model(model_file='earth.model', units='scaled'):
    """
    Reads an earth model file in Pollitz's format.
    Returns an :class:`Earth` object with the physical properties in the
    specified unit and the radius of the earth in km as it appears in the file.
    It is assumed that the layers are sorted from the bottom to the top.
    """
    with open(model_file) as fl:
        n, _, radius, _ = read_fortran('2i2,2f10.3', fl.readline())
        layers = [read_layer(fl.readline(), radius, units=units)
                  for _ in range(n)]

    return Earth(layers, radius, units)


class Earth(object):
    """
    Representation of a layered earth.
    """

    def __init__(self, layers, radius, units):
        """
        Creates a layered :class:`Earth` object.

        :arg layers: a list of :class:`Layer` objects
        :arg radius: radius of the earth in km
        :arg units: unit system for the earth
        """
        self.layers = layers
        self.radius = radius
        self.units = units

    def replace_layers(self, layers):
        """
        Creates an :class:`Earth` object from a list
        (or possibly iterator) of layers.
        """
        return Earth(list(layers), self.radius, self.units)

    def transform(self, function):
        """
        Creates an :class:`Earth` object by applying the given
        *function* to each of the layers.
        """
        return self.replace_layers([function(layer)
                                    for layer in self.layers])

    @lazy
    def monster(self):
        """
        The whole earth as one layer.
        Its properties are in fact arrays of those of its children.
        Since it is lazily evaluated, any mutation to the
        :class:`Earth` object or its children layers are not reflected
        after the monster layer is first accessed. To update the child
        layers safely, use :func:`update_layers`.
        """
        # pick one child layer
        first = self.layers[0]

        # arrays of physical properties from the layers
        properties = {name: numpy.array([getattr(layer, name)
                                         for layer in self.layers])
                      for name in vars(first)
                      if name not in ['tag']}

        # the type of the layers
        cls = type(first)

        # one layer with these properties
        return cls(**properties)

    def update_layers(self, **kwargs):
        """
        Updates the given properties of the child layers.
        Also, updates the monster layer accordingly.
        """
        for key in kwargs:
            for layer in self.layers:
                setattr(layer, key, kwargs[key])
            setattr(self.monster, key, kwargs[key])

    def add_core(self):
        """
        Prepend an additional layer to the earth layers that covers (most of)
        the inside (excluding a small sphere around the center).
        """

        # the center has to be excluded because the matrizant matrices are
        # rescaled by the bottom of the layer which would result in a division
        # by zero
        first = self.layers[0]
        factors = conversion_factors(self.units, self.radius)

        # the center is excluded from integration
        excluded_center = (self.radius * EXCLUDED_CENTER_RADIUS *
                           factors['length'])

        if first.bottom <= excluded_center:
            return self

        # copy the physical properties
        new_first = copy.deepcopy(first)

        new_first.bottom = excluded_center
        new_first.top = first.bottom

        # mark this layer as synthetic
        # helps prevent_matrizant_overflow decide whether
        # a layer came from the earth model file or not
        new_first.tag = 'synthetic'

        return self.replace_layers([new_first] + self.layers)

    def ensure_sampling(self):
        """
        Ensure satisfactory sampling of the radial solution
        :math:`\\mathbf{y}` for numerical integration.
        Since :math:`\\mathbf{y}` is sampled at the layer
        boundaries, it suffices to break layers down to some maximum
        allowed thickness.
        """
        factors = conversion_factors(self.units, self.radius)

        # maximum allowed thickness for a layer
        maximum_thickness = self.radius * SAMPLING_WIDTH * factors['length']

        def divide(layer):
            """
            Keeps dividing a layer until the maximum allowed thickness
            is reached. Yields the divided layers to an iterator rather
            than returning a list.
            """
            top = layer.top
            bottom = layer.bottom

            if abs(top - bottom) < maximum_thickness:
                yield layer
            else:
                # split the layer in the middle.
                mid = (top + bottom) / 2

                for kid_layer in split_layer(layer, mid):
                    # divide recursively if necessary
                    for grand_kid_layer in divide(kid_layer):
                        yield grand_kid_layer

        # collect the iterators into another iterator
        divided_layers = (divide(layer) for layer in self.layers)

        # merge the iterator of iterators and convert into a list
        return self.replace_layers(itertools.chain(*divided_layers))

    def prevent_matrizant_overflow(self, max_l):
        """
        Divide the layers into small enough pieces so that calculating
        the matrizant matrices does not cause numerical overflow for upto
        the given maximum :math:`l`.
        """
        factors = conversion_factors(self.units, self.radius)

        # minimum thickness a synthetic layer must have
        minimum_thickness = (self.radius *
                             MINIMUM_SYNTHETIC_THICKNESS) * factors['length']

        def divide(layer):
            """
            Keeps dividing a layer until matrizant matrices are safe from
            overflow. Yields the divided layers to a iterator rather
            than returning a list.
            """
            top = layer.top
            bottom = layer.bottom

            log_ratio = numpy.log10(top / bottom)
            order_of_magnitude = max_l * log_ratio
            divisions = int(numpy.ceil(order_of_magnitude /
                                       EXPECTED_MAGNITUDE))

            if divisions < 2:
                yield layer
            else:
                # dividing the layer evenly does not do the job
                # we have to divide it in geometrical progression
                bottoms = [bottom * numpy.power(10., k * log_ratio / divisions)
                           for k in range(divisions)]
                tops = bottoms[1:] + [top]

                # create sublayers with the same physical properties
                for k in range(divisions):
                    new_layer = copy.deepcopy(layer)
                    new_layer.top = tops[k]
                    new_layer.bottom = bottoms[k]
                    yield new_layer

        def too_small(layer):
            """
            Decide if a synthetic layer is too small to be included.
            """
            if not hasattr(layer, 'tag'):
                # not a synthetic layer, so must include
                return False

            return layer.top - layer.bottom < minimum_thickness

        # collect the iterators into another iterator
        divided_layers = (divide(layer) for layer in self.layers)

        # merge the iterators
        # remove those layers at the beginning that are too small
        layers = itertools.dropwhile(too_small,
                                     itertools.chain(*divided_layers))
        return self.replace_layers(layers)

    def unique_layers(self):
        """
        Merge contiguous that share the same physical properties.
        """
        return self.replace_layers(unique_layers(self.layers))

    def with_interior(self, max_l):
        """
        Create a version of the earth with interior layers covering
        the core and ensuring required sampling for numerical integration.
        """
        return (self
                .add_core()
                .prevent_matrizant_overflow(max_l)
                .ensure_sampling())

    def find_index(self, r):
        """
        Given a radius, gives the index of the layer that contains it
        in the list of layers.
        In case it lies on a layer boundary,
        returns the index of the layer that has the radius at its bottom.
        """
        for index, layer in enumerate(self.layers):
            if r in layer:
                if abs(layer.top - r) == 0. and index + 1 < len(self.layers):
                    return index + 1
                else:
                    return index

        raise ValueError("{} not inside Earth".format(r))

    def find_layer(self, r):
        """
        Given a radius, returns the layer that contains it.
        """
        return self.layers[self.find_index(r)]

    def put_source(self, r):
        """
        Splits the layer containing the source at the given source radius.
        """

        return self.replace_layers(itertools.chain(*[split_layer(layer, r)
                                                     for layer
                                                     in self.layers]))

    def __str__(self):
        """
        String representation of the list of layers for easy inspection.
        """
        return "\n".join([str(layer) for layer in self.layers])

    def constant_solution(self, value, shape, surface_only):
        """
        Constant solution of appropriate size.
        Useful for early termination of propagation when it fails.
        """
        if surface_only:
            return numpy.full(shape, value)
        else:
            return numpy.full((len(self.layers) + 1,) + shape, value)

    def radial_solution(self, cur_layer=0, cur_y=None,
                        surface_only=False, renormalize=True):
        """
        Radial solution of the equation of motion.

        :arg cur_layer: index of layer to start propagation from
        :arg cur_y: initial value of radial solution :math:`\\mathbf{y}`
        :arg surface_only: whether to return the solution only at the surface
        :arg renormalize: whether renormalization of the solution is allowed
        """
        # if the starting layer is somewhere in the middle
        # then pass the task to another Earth object
        # whose layers start from that one
        if cur_layer != 0:
            return (self.replace_layers(self.layers[cur_layer:])
                    .radial_solution(cur_y=cur_y,
                                     surface_only=surface_only,
                                     renormalize=renormalize))

        first = self.layers[0]

        # initial y can be supplied to the procedure
        # if not, take the solutions that are regular at the origin
        # from the matrizant matrix of the inner-most layer
        if cur_y is None:
            cur_y = first.regular_initial_condition()
        if not numpy.all(numpy.isfinite(cur_y)):
            raise ArithmeticError

        # the solutions at the layer boundaries are accumulated here
        collection = numpy.array([cur_y])

        # create all the propagator matrices of the layers at once
        # these are matrices that propagate a solution from bottom to top
        monster = self.monster
        matrizant_matrices = monster.matrizant_matrix(monster.top)
        inverse_matrizants = monster.inverse_matrizant_matrix(monster.bottom)

        for index in range(len(self.layers)):
            layer = self.layers[index]

            # try propagating analytically
            result = (matrizant_matrices[:, :, index]
                      .dot(inverse_matrizants[:, :, index].dot(cur_y)))

            if not numpy.all(numpy.isfinite(result)):
                # if failed, propagate numerically
                div = int(numpy.ceil((layer.top - layer.bottom) /
                                     NUMERICAL_SAMPLING_WIDTH))
                result = layer.propagate_numerically(cur_y, div=div)

                if not numpy.all(numpy.isfinite(result)):
                    # if even that failed, abort
                    return self.constant_solution(float('nan'),
                                                  cur_y.shape, surface_only)

            # append the current y to the collection
            collection = numpy.concatenate((collection, numpy.array([result])))

            # calculate the euclidian norm of y
            # it may not be dimensinally correct to do so
            # but it does the job
            norm = linalg.norm(result)

            # if norm is zero then no need to propagate anymore
            # return immediately with appropriately sized zero array
            if norm == 0.:
                return self.constant_solution(0., cur_y.shape, surface_only)

            # rescale if needed
            # the solutions are defined only upto a constant
            # unless a discontuity caused by a seismic source
            # is being propagated
            while renormalize and norm > MAX_NORM:
                collection /= MAX_NORM
                norm /= MAX_NORM

            while renormalize and norm < 1. / MAX_NORM:
                collection *= MAX_NORM
                norm *= MAX_NORM

            # set up for next layer
            cur_y = collection[-1]

        if surface_only:
            return collection[-1]
        else:
            return collection


def merge_layers(layers):
    """
    Merges a list of adjacent layers with the same physical properties
    into one.
    """
    result = copy.deepcopy(layers[0])
    result.top = layers[-1].top
    return result


def unique_layers(layers):
    """
    Physically distinct layers.
    Merges contiguous layers in the original list that share the same
    physical properties.
    """
    # groupby calls Layer.__eq__ to decide if two layers are equal
    return [merge_layers(list(ls)) for _, ls in itertools.groupby(layers)]


def is_viscous(layer, crust):
    """
    Whether a layer is to be considered "viscoelastic" as opposed to "elastic".
    We compare its viscosity :math:`\\eta` with that of the crust
    and see if it is too small.
    """
    return layer.eta / crust.eta < VISCOSITY_RATIO


def count_viscoelastic_layers(layers):
    """
    Count the number of viscoelastic layers in the given list of layers
    with distinct physical properties.
    """
    return len([layer for layer in layers if is_viscous(layer, layers[-1])])


def count_viscoelastic_interfaces(layers):
    """
    Count the number of boundaries separating viscoelastic layers from
    elastic ones from a list of layers with distinct physical properties.
    """
    crust = layers[-1]

    return len([(first, second)
                for first, second in zip(layers[:-1], layers[1:])
                if is_viscous(first, crust) != is_viscous(second, crust)])


class MaxwellToroidal(MaxwellRheology, StaticToroidal):
    """
    Implementation of toroidal motion in the case of Maxwell viscoelastic
    rheology.  By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    See [Pollitz1992]_.
    """

    @staticmethod
    def number_of_roots(layers):
        """
        Number of roots of the characteristic function.
        See [VISCO1D]_ manual.
        """
        uniques = unique_layers(layers)

        M_1 = count_viscoelastic_interfaces(uniques)
        M_2 = count_viscoelastic_layers(uniques)

        return M_1 + M_2 - 1


class MaxwellSpheroidal(MaxwellRheology, StaticSpheroidal):
    """
    Implementation of spheroidal motion in the case of Maxwell viscoelastic
    rheology. By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    See [Pollitz1992]_.
    """

    @staticmethod
    def number_of_roots(layers):
        """
        Number of roots of the characteristic function.
        See [VISCO1D]_ manual.
        """
        uniques = unique_layers(layers)

        M_1 = count_viscoelastic_interfaces(uniques)
        M_2 = count_viscoelastic_layers(uniques)

        return 3 * M_1 + 4 * (M_2 - 1)


class MaxwellSpheroidalDegree0(MaxwellRheology, StaticSpheroidalDegree0):
    """
    Implementation of spheroidal motion in the case of Maxwell viscoelastic
    rheology for the special case of :math:`l = 0`.
    By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    See [Pollitz1992]_.
    """

    @staticmethod
    def number_of_roots(_):
        """
        Number of roots of the characteristic function.
        Currently unknown.
        """
        return None


class BurgersToroidal(BurgersRheology, StaticToroidal):
    """
    Implementation of toroidal motion in the case of Burgers viscoelastic
    rheology.  By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    """

    @staticmethod
    def number_of_roots(_):
        """
        Number of roots of the characteristic function.
        Currently unknown.
        """
        return None


class BurgersSpheroidal(BurgersRheology, StaticSpheroidal):
    """
    Implementation of spheroidal motion in the case of Burgers viscoelastic
    rheology. By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    """

    @staticmethod
    def number_of_roots(_):
        """
        Number of roots of the characteristic function.
        Currently unknown.
        """
        return None


class BurgersSpheroidalDegree0(BurgersRheology, StaticSpheroidalDegree0):
    """
    Implementation of spheroidal motion in the case of Burgers viscoelastic
    rheology for the special case of :math:`l = 0`.
    By the correspondence principle, the equation of motion is
    identical to the static case with a modified shear modulus :math:`\\mu`.
    See [Pollitz1992]_.
    """

    @staticmethod
    def number_of_roots(_):
        """
        Number of roots of the characteristic function.
        Currently unknown.
        """
        return None


def pick_rheology(rheology, modes):
    """
    Pick appropriate rheology class from *rheology* and *modes*
    specification in strings.
    """
    assert modes in ['toroidal', 'spheroidal']
    assert rheology in ['Maxwell', 'Burgers']

    if modes == 'toroidal':
        if rheology == 'Maxwell':
            return MaxwellToroidal
        elif rheology == 'Burgers':
            return BurgersToroidal
    elif modes == 'spheroidal':
        if rheology == 'Maxwell':
            return MaxwellSpheroidal
        elif rheology == 'Burgers':
            return BurgersSpheroidal


def sampling_points(monster):
    """
    The radii of the layer boundaries.
    """
    return numpy.concatenate((monster.bottom, numpy.array([monster.top[-1]])))


def discrete_integral(r, y, integrand):
    """
    Performs integration from a discrete sampling.
    The *y* values are sampled at the layer boundaries *r*.

    :arg r: the sampling points, that is, the radii of the layer boundaries
    :arg y: radial solution of the equation of motion
    :arg integrand: function to be integrated that depends on (*r*, *y*)
    """

    # if the number of layers is N then there are N + 1 boundary radii
    # so r has shape N + 1, y has shape (N + 1, :)

    # integrand values evaluated at the bottom and top of the layers
    bottom = integrand(r[:-1], y[:-1])
    top = integrand(r[1:], y[1:])

    # layer thickness
    dr = r[1:] - r[:-1]

    # trapezoid rule
    return sum((bottom + top) * dr / 2.)


def mass_of_earth(original_earth):
    """
    Calculates the mass of the earth from the density profile.
    """
    earth = original_earth.with_interior(0)

    def integrand(r, _):
        """
        The mass integrand.
        """
        rho = earth.monster.density

        return r ** 2 * rho

    r = sampling_points(earth.monster)
    y = earth.monster.density
    return 4. * numpy.pi * discrete_integral(r, y, integrand)
