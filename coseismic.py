"""
Calculation of coseismic displacement fields.
"""
import numpy
from scipy import linalg

from basic import X00, dX10, ddX20, ii, cutoff_l, mode_count
from layers import StaticToroidal, StaticSpheroidal, stress_rows
from layers import StaticSpheroidalDegree0
from earth import sampling_points, discrete_integral


def remember(modes, l, m, y):
    """
    Save the value of the radial displacement-stress vector *y* at the surface
    in the dictionary of *modes* only if it is non-zero.
    """
    if y is not None and numpy.any(y):
        modes[l, m] = y


def solution_at_top(homo_sol, del_y, earth, source_index):
    """
    The displacement-stress vector at the surface
    for a given discontinuity.
    """
    # return immediately if there is no discontinuity
    if not numpy.any(del_y):
        return None

    # propagate it from the source radius to the surface
    del_y_sol = earth.radial_solution(surface_only=True,
                                      cur_y=del_y,
                                      cur_layer=source_index,
                                      renormalize=False)

    # x is the vector of coefficients to ensure the boundary
    # condition at the surface of the radial traction being zero
    # for spheroidal mode it is a two element vector for
    # the two independent solutions
    x = linalg.solve(stress_rows(homo_sol),
                     -stress_rows(del_y_sol))

    # the solution at the surface is the linear combination
    # that satisfies the boundary condition
    return homo_sol.dot(x) + del_y_sol


def spheroidal_delta_y(layer, l, m, M):
    """
    The discontinuity in the radial displacement-stress vector at the source
    for spheroidal motion.
    See [Pollitz1996]_.

    :arg layer: the layer with the source at the bottom
    :arg l: spherical harmonic degree
    :arg m: spherical harmonic order
    :arg M: moment tensor
    """
    # calculation of negative m follows from equation 24 of
    # Pollitz's paper

    # in epicentral coordinates only abs(m) <= 2 components are non-zero
    assert m in [0, 1, 2]

    mu, lamda, sigma = layer.mu, layer.lamda, layer.sigma
    r_s = layer.bottom

    # the result is a complex column vector
    result = numpy.zeros((4, 1), dtype=complex)

    # from Pollitz's paper
    if m == 0:
        result[0, 0] = (X00(l) / (r_s ** 2 * sigma)) * M[0, 0]
        result[1, 0] = ((X00(l) / r_s ** 3) * (-2.) *
                        ((M[1, 1] + M[2, 2]) / 2. - (lamda / sigma) * M[0, 0]))
        result[3, 0] = result[1, 0] / (-2.)

    elif m == 1:
        result[2, 0] = ((dX10(l) / (r_s ** 2 * mu * l * (l + 1.))) *
                        (M[0, 1] - ii * M[0, 2]))

    elif m == 2:
        result[3, 0] = ((-ddX20(l) / (r_s ** 3 * l * (l + 1.))) *
                        (M[1, 1] - M[2, 2] - 2 * ii * M[1, 2]))

    return result


def homogeneous_spheroidal_modes(original_earth, min_l, max_l):
    """
    Returns a dictionary that maps the spherical harmonic
    degree :math:`l` to the homogeneous solutions at the
    surface of the radial spheroidal equation of motion.
    """
    result = {}

    # will set the l value later
    earth = original_earth.transform(StaticSpheroidal.mimic)

    # radial solutions of the equation of motion with omega == 0
    # and regular initial conditions at the center of the earth
    # linearly independent solutions are arranged in columns
    # except for the case l = 1, only the displacement-stress vectors
    # at the surface are needed
    for l in range(min_l, max_l + 1):
        if l == 0:
            cls = StaticSpheroidalDegree0
            special = original_earth.transform(lambda layer:
                                               cls.mimic(layer, l=l))

            # the shape of the matrix is (2, 1)
            result[l] = special.radial_solution(surface_only=True)

        elif l == 1:
            # update the l value
            earth.update_layers(l=l)
            # have to do a numerical integration
            special = earth.with_interior(l)

            # the shape of the matrix is (N + 1, 4, 2)
            # where N is the number of layers
            # this is because we store the discretely sampled
            # values for the interior
            result[l] = special.radial_solution()

        else:
            # update the l value
            earth.update_layers(l=l)

            # the shape of the matrix is (4, 2)
            result[l] = earth.radial_solution(surface_only=True)

    return result


def spheroidal_modes(original_earth, r_s, M, homogeneous_solutions):
    """
    Returns a dictionary with :math:`(l, m)` pairs as keys
    that map to the radial solutions at the surface of the spheroidal
    equations of motion for the specified moment tensor *M*.
    It is assumed that *original_earth* has a layer boundary at *r_s*.
    """
    result = {}

    # the discontinuity must be propagated from the source radius
    # to the surface
    source_index = original_earth.find_index(r_s)

    # will set the l value later
    earth = original_earth.transform(StaticSpheroidal.mimic)

    for l in homogeneous_solutions:
        if l > cutoff_l(r_s):
            # source depth is more than the wavelength cutoff
            continue

        elif l == 0:
            # earth layers for degree 0
            cls = StaticSpheroidalDegree0
            special = original_earth.transform(lambda layer:
                                               cls.mimic(layer, l=l))

            homo_sol = homogeneous_solutions[l]

            # the only m mode for l = 0
            m = 0

            # the solution is a column vector in matrix form
            # with shape (2, 1)
            del_y = spheroidal_delta_y(special.layers[source_index],
                                       l, m, M)[:cls.dimension, :]
            top_y = solution_at_top(homo_sol, del_y,
                                    special, source_index)
            remember(result, l, m, top_y)

        elif l == 1:
            # update the l value
            earth.update_layers(l=l)
            # have to do a numerical integration
            special = earth.with_interior(l)

            # with interior layers, source index has to be recalculated
            source_index = special.find_index(r_s)

            homo_sol = homogeneous_solutions[l]

            def surface_y(m):
                """
                The displacement-stress vector for the l, m mode
                at the surface.
                """
                del_y = spheroidal_delta_y(special.layers[source_index],
                                           l, m, M)
                if not numpy.any(del_y):
                    return None

                del_y_sol = special.radial_solution(cur_y=del_y,
                                                    cur_layer=source_index,
                                                    renormalize=False)
                del_y_sol = del_y_sol[:, :, 0]

                def integrand_from(index):
                    """
                    The integrand for the angular momentum conservation
                    equation, starting from a given layer index.
                    """
                    def integrand(r, y):
                        """
                        The integrand function.
                        """
                        rho = special.monster.density[index:]

                        return r ** 2 * rho * (y[:, 0] + 2. * y[:, 2])

                    return integrand

                r = sampling_points(special.monster)
                homo_integral_1 = discrete_integral(r, homo_sol[:, :, 0],
                                                    integrand_from(0))
                homo_integral_2 = discrete_integral(r, homo_sol[:, :, 1],
                                                    integrand_from(0))
                disc_integral = discrete_integral(r[source_index:],
                                                  del_y_sol,
                                                  integrand_from(source_index))

                bc_mat = numpy.array([[homo_sol[-1, 1, 0],
                                       homo_sol[-1, 1, 1]],
                                      [homo_integral_1,
                                       homo_integral_2]])
                bc_vec = numpy.array([del_y_sol[-1, 1], disc_integral])

                x = linalg.solve(bc_mat, -bc_vec)

                y = homo_sol[-1, :, :].dot(x) + del_y_sol[-1, :]

                return y

            for m in [0, 1]:
                remember(result, l, m, surface_y(m))

            # restore the source index
            source_index = original_earth.find_index(r_s)

        else:
            # update the l value
            earth.update_layers(l=l)

            homo_sol = homogeneous_solutions[l]

            for m in [0, 1, 2]:
                # the solution is a column vector in matrix form
                # with shape (4, 1)
                del_y = spheroidal_delta_y(earth.layers[source_index],
                                           l, m, M)
                top_y = solution_at_top(homo_sol, del_y,
                                        earth, source_index)
                remember(result, l, m, top_y)

    return result


def toroidal_delta_y(layer, l, m, M):
    """
    The discontinuity in the radial displacement-stress vector at the source
    for toroidal motion.
    See [Pollitz1996]_.

    :arg layer: the layer with the source at the bottom
    :arg l: spherical harmonic degree
    :arg m: spherical harmonic order
    :arg M: moment tensor
    """
    # calculation of negative m follows from equation 24 of
    # Pollitz's paper

    # in epicentral coordinates only abs(m) <= 2 components are non-zero
    assert m in [0, 1, 2]

    mu = layer.mu
    r_s = layer.bottom

    # the result is a complex column vector
    result = numpy.zeros((2, 1), dtype=complex)

    # from Pollitz's coseismic paper
    if m == 1:
        result[0, 0] = ((dX10(l) / (r_s ** 2 * mu * l * (l + 1.))) *
                        (-M[0, 2] - ii * M[0, 1]))

    elif m == 2:
        result[1, 0] = (((ddX20(l) * ii) / (l * (l + 1.) * r_s ** 3)) *
                        (M[1, 1] - M[2, 2] - 2 * ii * M[1, 2]))

    return result


def homogeneous_toroidal_modes(original_earth, min_l, max_l):
    """
    Returns a dictionary that maps the spherical harmonic
    degree :math:`l` to the homogeneous solutions at the
    surface of the radial toroidal equation of motion.
    """
    result = {}

    # will set the l value later
    earth = original_earth.transform(StaticToroidal.mimic)

    # radial solutions of the equation of motion with omega == 0
    # and regular initial conditions at the center of the earth
    # linearly independent solutions are arranged in columns
    # except for the case l = 1, only the displacement-stress vectors
    # at the surface are needed
    for l in range(min_l, max_l + 1):
        if l == 0:
            # for toroidal modes, m = 0 is absent
            pass

        elif l == 1:
            # update the l value
            earth.update_layers(l=l)
            special = earth.with_interior(l)

            # the shape of the matrix is (N + 1, 2, 1)
            # where N is the number of layers
            # this is because we store the discretely sampled
            # values for the interior
            result[l] = special.radial_solution()

        else:
            # update the l value
            earth.update_layers(l=l)

            # the shape of the matrix is (4, 2)
            result[l] = earth.radial_solution(surface_only=True)

    return result


def toroidal_modes(original_earth, r_s, M, homogeneous_solutions):
    """
    Returns a dictionary with :math:`(l, m)` pairs as keys
    that map to the radial solutions at the surface of the toroidal
    equations of motion for the specified moment tensor *M*.
    It is assumed that *original_earth* has a layer boundary at *r_s*.
    """
    result = {}

    # the discontinuity must be propagated from the source radius
    # to the surface
    source_index = original_earth.find_index(r_s)

    # will set the l value later
    earth = original_earth.transform(StaticToroidal.mimic)

    for l in homogeneous_solutions:
        if l > cutoff_l(r_s):
            # source depth is more than the wavelength cutoff
            continue

        elif l == 0:
            # for toroidal modes, m = 0 is absent
            pass

        elif l == 1:
            # update the l value
            earth.update_layers(l=l)
            # have to do a numerical integration
            special = earth.with_interior(l)

            # with interior layers, source index has to be recalculated
            source_index = special.find_index(r_s)

            homo_sol = homogeneous_solutions[l][:, :, 0]

            # for l = 1, the only possible (positive) value for m is 1
            m = 1

            def surface_y(m):
                """
                The displacement-stress vector for the l, m mode
                at the surface.
                """
                del_y = toroidal_delta_y(special.layers[source_index], l, m, M)
                if not numpy.any(del_y):
                    return None

                del_y_sol = special.radial_solution(cur_y=del_y,
                                                    cur_layer=source_index,
                                                    renormalize=False)
                del_y_sol = del_y_sol[:, :, 0]

                def integrand_from(index):
                    """
                    The integrand for the angular momentum conservation
                    equation, starting from a given layer index.
                    """
                    def integrand(r, y):
                        """
                        The integrand function.
                        """
                        rho = special.monster.density[index:]

                        return r ** 3 * rho * y[:, 0]

                    return integrand

                r = sampling_points(special.monster)
                homo_integral = discrete_integral(r, homo_sol,
                                                  integrand_from(0))
                disc_integral = discrete_integral(r[source_index:], del_y_sol,
                                                  integrand_from(source_index))

                x = -disc_integral / homo_integral

                y = x * homo_sol[-1] + del_y_sol[-1]

                return y

            remember(result, l, m, surface_y(m))

            # restore original source layer index
            source_index = original_earth.find_index(r_s)

        else:
            # update the l value
            earth.update_layers(l=l)

            homo_sol = homogeneous_solutions[l]

            for m in [1, 2]:
                del_y = toroidal_delta_y(earth.layers[source_index],
                                         l, m, M)
                top_y = solution_at_top(homo_sol, del_y,
                                        earth, source_index)
                remember(result, l, m, top_y)

    return result


def surface_displacements(spheroidal, toroidal, stations):
    """
    The coseismic surface displacement field evaluated at specified stations,
    that is, observation points, in the epicentral coordinate.

    :arg spheroidal: dictionary from :func:`spheroidal_modes`
    :arg toroidal: dictionary from :func:`toroidal_modes`
    :arg stations: observation points in the epicentral coordinate
    """
    # a three-dimentinal vector in spherical coordinates for each station
    # indices: [axis, point]
    u = numpy.zeros((3,) + stations.shape, dtype=complex)

    # mathematical functions
    sin_theta = stations.sin_theta
    Y, Y_theta, Y_phi = stations.Y, stations.Y_theta, stations.Y_phi

    # the indexing of y is shifted by one
    # due to Python's convention

    # toroidal modes
    for l, m in sorted(toroidal.keys()):
        y = toroidal[l, m]

        # the displacements are actually the real part of these
        u[1, :] += mode_count(m) * y[0] * Y_phi[:, l, m] / sin_theta
        u[2, :] += -mode_count(m) * y[0] * Y_theta[:, l, m]

    # spheroidal modes
    for l, m in sorted(spheroidal.keys()):
        y = spheroidal[l, m]

        # the displacements are actually the real part of these
        u[0, :] += mode_count(m) * y[0] * Y[:, l, m]

        if l >= 1:
            u[1, :] += mode_count(m) * y[2] * Y_theta[:, l, m]
            u[2, :] += mode_count(m) * y[2] * Y_phi[:, l, m] / sin_theta

    # so the imaginary part is safe to throw away
    return u.real
