#!/usr/bin/env python
# PYTHON_ARGCOMPLETE_OK
"""
Record the displacement field for an earthquake.
"""

from argparse import ArgumentParser
import os
import json

import numpy

from earth import read_model
from frame import parse_faults

from frame import unit_moments, process_options
from frame import write_options
from frame import theoretical_shifts
from stations import uniform_area_grid, uniform_angle_grid, Stations
from quake import normalize_faults
from quake import setup_earthquake, add_common_options
from utils import care, write_json, autocomplete
from quake import write_displacement_field
from voronoi import random_points
from utils import info


def output_folder(root_folder, experiment_folder):
    """
    Creates the folder to put output files into
    if it does not already exist.
    """
    folder = os.path.join(root_folder, experiment_folder)

    try:
        os.makedirs(folder)
    except OSError:
        pass

    return folder


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    moment_choices = unit_moments.keys()

    method_choices = ['area-grid', 'monte-carlo', 'angle-grid']

    integrate_help = """
    whether to integrate over an extended fault
    (default: point source)
    """

    moment_help = """
    name of the unit moment tensor
    (one of [""" + ", ".join(moment for moment in moment_choices) + "])"

    method_help = """
    method of data collection
    (one of [""" + ", ".join(method_choices) + "])"

    parser = ArgumentParser(description=__doc__)

    parser.add_argument('--output', '-o', default='../data', type=str,
                        help="output directory")
    parser.add_argument('--name', '-n', required=True,
                        help="name of the experiment")
    parser.add_argument('--debug', '-d', action='count', default=0,
                        help='emit debug information')

    parser.add_argument('--method', default='angle-grid', type=str,
                        choices=method_choices,
                        help=method_help)

    add_common_options(parser)

    parser.add_argument('--fault', '-f', type=str,
                        help="single fault file or glob expression")
    parser.add_argument('--integrate', '-i', action='store_false',
                        dest='point_source',
                        help=integrate_help)
    parser.add_argument('--moment', '-m', type=str,
                        choices=moment_choices,
                        help=moment_help)
    parser.add_argument('--depth', type=float,
                        help="depth of the fault in km")

    parser.add_argument('--latitudes', '-L',
                        type=int, default=1000,
                        help="number of latitude points")

    parser.add_argument('--longitudes', '-W',
                        type=int, default=2000,
                        help="number of longitude points")

    return parser


def main(args):
    """
    Statistics of shifts in Helmert parameters for finite sampling.
    """
    model = read_model(model_file=args.model)
    faults = parse_faults(args, model)
    earthquake = setup_earthquake(args, model,
                                  normalize_faults(faults, args.point_source))

    options = process_options(args, earthquake.faults, earthquake.earth)
    options['shift'] = theoretical_shifts(earthquake)

    info("given options\n")
    info("{}\n", json.dumps(options, indent=2, sort_keys=True))

    folder = output_folder(options['output'], options['name'])
    write_options(folder, options)

    latitudes = options['latitudes']
    longitudes = options['longitudes']
    if options['method'] == 'area-grid':
        stations = uniform_area_grid(latitudes, longitudes,
                                     earthquake.max_l)
    elif options['method'] == 'angle-grid':
        stations = uniform_angle_grid(latitudes, longitudes,
                                      earthquake.max_l)
    elif options['method'] == 'monte-carlo':
        theta, phi = random_points(latitudes * longitudes)
        stations = Stations(theta, phi, earthquake.max_l)
    else:
        raise ValueError

    output_file = os.path.join(folder, 'field.log')
    field = earthquake.surface_displacement_field(stations, numpy.array([0.]))
    write_displacement_field(output_file, stations, field)

    write_json(os.path.join(folder, 'options.json'), options)


if __name__ == '__main__':
    numpy.set_printoptions(precision=6, suppress=False)
    command_line_parser = argument_parser()
    autocomplete(command_line_parser)
    command_line_args = command_line_parser.parse_args()

    # raise errors if encountered with invalid numerical operations
    # rather than failing silently
    with care:
        main(command_line_args)
