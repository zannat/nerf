Folder structure
----------------

The program is organized into the following directory tree under the
root directory of the program which is referred to by ``${NERF_ROOT}`` in this
document.

It contains:

- the Python scripts

- a GNU Make ``Makefile`` which for now just cleans up temporary files

- the ``README`` file which is a symbolic link to
  ``${NERF_ROOT}/docs/source/README.rst``

- an `org-mode <http://orgmode.org/>`__ file ``TODO.org`` to organize the
  project

- a Jupyter notebook ``visual_voronoi.ipynb`` to visually verify the Voronoi
  decomposition algorithm of the unit sphere

The subfolders are:

- ``docs`` for project documentation using
  `Sphinx <http://www.sphinx-doc.org>`__.

  It contains:

  - a ``Makefile`` for easy generation of documenation. Running ``make``
    lists the available targets. To generate PDF documentation, for example, run

    .. code-block:: bash

       make latexpdf

    which needs `LaTeX <https://www.latex-project.org>`__ to be installed.

    It also has subfolders:

  - ``build`` for generated documentation

  - ``source`` for documentation sources

- ``faults`` for fault description files

- ``figures`` for generated plots

- ``models`` for earth models

- ``shell-scripts`` for shell scripts run specific tasks. I do
  not expect all of these scripts to be up-to-date.


Comparison with ``cograv`` and ``visco1d``
------------------------------------------

Not supported (yet)
~~~~~~~~~~~~~~~~~~~
- calculation of strain and velocity fields (low priority)
- effect of gravity (low priority, since Pollitz shows that the effect is small)

Improvements
~~~~~~~~~~~~

Accuracy of results
*******************

- In calculating the postseismic relaxation, a normalization factor needs to
  be found by numerically integrating the modes over the Earth's interior.
  Pollitz ignores the core in the integration because it contributes
  significantly to only a small number of low :math:`l` modes.

  Since theoretically these modes contribute the most to a change in the origin
  or the orientation parameters, we include the core in the calculations despite
  slight performance loss.

- To be fast, Pollitz's programs heavily rely on interpolation. Although the
  loss of accuracy because of this is small in practice, his interpolation of
  the oscillatory associated Legendre polynomials for high :math:`l`
  by a relatively smaller number of points means that in the far field our
  code follows the theory more closely.

  The performance of the program did not suffer greatly because of this
  avoidance of interpolation.

- For Burgers' materials, ``visco1d`` fails to distinguish poles from zeros
  and thus has some extraneous contributions to the displacement fields.


Performance
***********
The performance of this code is generally worse than Pollitz's, as is natural
for choosing Python over Fortran.  However, the principle targets were
improved modularity, readability and reusability. In addition,
I calculate the roots by the faster Ridders' method that results in
improvement in performance for that procedure.

Numerical accuracy in calculating normal modes
**********************************************
Using Mathematica, I could invert the matrizant matrices in Pollitz's
papers. These analytical expressions are in general slighly more
numerically accurate than numerically solving the matrix equation, but
more importantly it resulted in some performance gain.

More relaxed earth model input files
************************************
In order to avoid numerical overflow, Pollitz often divides the earth
model layers into physically identical sublayers manually in his earth model files.
This program can do that algorithmically.

Implementation notes
--------------------

There are some aspects of the code that are somewhat unusual. The two most
significant of those are:

Polymorphism of numbers and arrays
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Some mathematical formulas, including the matrizant matrix code, have been
designed to be applicable to not only ordinary numbers but also :mod:`numpy`
arrays. This allows batch calculations that helps mitigate the performance
loss of using a high level language.

Lazy evaluation
~~~~~~~~~~~~~~~
Some quantities of interest are evaluated only if they are ever accessed,
but once they are, their values are stored, and any further request for
that value can be met without re-evaluation. This design, implemented by
a :code:`@lazy` decorator, resulted in much cleaner and readable code.

Voronoi decomposition
---------------------
Although I have not yet completed my implementation of measuring the sampling
distribution of Helmert parameters by GPS stations, I have already implemented
the Voronoi decomposition of the surface of the sphere once the location of
the stations are known. To visualize the algorithm, run the Jupyter server
by the following command at ``${NERF_ROOT}``.

.. code-block:: bash

   jupyter notebook

and once the browser opens, navigate to the notebook ``visual_voronoi.ipynb``.
