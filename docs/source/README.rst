Installation
============

.. note::

   Perhaps the quickest way to get started is to install `Anaconda`_ if not
   already installed. Then, run the following in your terminal

   .. code-block:: bash

      # install Python libraries
      conda install python scipy numpy matplotlib sphinx pip
      pip install fortranformat

      # install optional Python libraries
      conda install seaborn basemap jupyter pandas
      pip install argcomplete

      # generate documentation
      make -C docs html

   If you have this program in the folder ``${NERF_ROOT}``, then the
   documentation (including this file) should be readable as HTML pages
   starting at ``${NERF_ROOT}/docs/build/html/index.html``.

   This section provides more details about what was just done.  So if
   everything went smoothly, you can just skip it and go straight to
   :ref:`running the programs <running-the-programs>`.


Prerequisites
-------------

The indicated versions are the ones I have currently installed. The program
could possibly function correctly even with earlier versions.


- `Python`_ (>= 2.7.13)

   The programming language. See
   `instructions <https://www.python.org/downloads/>`__ for installation.
   Usually already available in GNU/Linux systems.

- `SciPy`_ (>= 0.18.1) and `NumPy`_ (>= 1.11.3)

   Scientific and numerical Python libraries. See
   `instructions <http://www.scipy.org/install.html>`__ for installation.

- `matplotlib`_ (>= 2.0.0)

   Plotting library. See
   `instructions <http://matplotlib.org/users/installing.html>`__ for
   installation.

- `fortranformat`_ (>= 0.2.5)

   A library that enables reading and writing according to Fortran's format
   specifications. Available from `PyPI`_.

- `basemap`_ (>= 1.0.7; optional)

   Library for plotting on geographical maps. See
   `instructions <http://matplotlib.org/basemap/users/installing.html>`__ for
   installation.

- `Jupyter`_ (>= 4.3.0; optional; for notebooks)

   Enhanced interactive data visualization environment. Formerly known as
   `IPython <https://ipython.org>`__. See
   `instructions <http://jupyter.readthedocs.io/en/latest/install.html>`__ for
   installation.

- `Seaborn`_ (>= 0.7.1; optional; for prettier plotting)

   Library to improve aesthetics of ``matplotlib`` plots.
   See `instuctions
   <https://stanford.edu/~mwaskom/software/seaborn/installing.html#installing>`__
   for installation.


- `Sphinx`_ (>= 1.5.1; optional; for documentation)

   Documentation generator. See
   `instructions <http://www.sphinx-doc.org/en/stable/install.html>`__ for
   installation.

- `GNU Make`_ (>= 4.1; optional)

   Utility for generating documentation.
   Usually already available in GNU/Linux systems.

- `argcomplete`_ (>= 1.8.2; optional)

   For ``bash`` autocompletion. See
   `instructions <https://argcomplete.readthedocs.io/en/latest/>`__ for installation.

- `pandas`_ (>= 0.19.2; optional)

   For prettier time-series plots. See
   `instructions <https://pandas.pydata.org>`__ for installation.


Code documentation (optional)
-----------------------------

With `Sphinx`_ installed, the documentation of the code can be converted into
more readable forms. At ``${NERF_ROOT}`` run the following command to
generate HTML documentation if needed.

.. code-block:: bash

   make -C docs html

Other target formats are also possible.

Running the programs
====================

.. _running-the-programs:

The main script is ``quake.py``. It performs both coseismic and postseismic
calculations.


Specifying the earth model
--------------------------

Some models of the layered structure of the Earth are located at the
``${NERF_ROOT}/models`` directory. To pick one, copy it to
``${NERF_ROOT}/earth.model``. For example, run

.. code-block:: bash

   cp models/earth.model.viscoelastic earth.model

at ``${NERF_ROOT}`` to experiment with a simple earth model with
one viscoelastic layer under the elastic crust.

Currently, the implementation of the Burgers rheology is slower and somewhat
fragile. Running realistic earth models ``earth.model.PREM`` and
``earth.model.burgers.sumatra`` is not yet recommmended.


Selecting a fault model
-----------------------

A collection of fault segments are located at the ``${NERF_ROOT}/faults``
directory. The ``stat2g.in_SUM*`` collection is for the coseismic displacement
calculations of the 2004 Sumatra earthquake. The ``strainx.inSUM*.PROF``
collection is for the postseismic relaxation calculations for the 2004 Sumatra
earthquake. The others are essentially test files.


Coseismic displacements
-----------------------

Once the ``earth.model`` is in place, run ``quake.py`` with the option
``--task coseismic`` (or, in short, ``--task co``, or even ``-t co``) to
perform coseismic displacement calculations. For example,

.. code-block:: bash

   python quake.py --task co --fault stat2g.in_SUM1Dgr --max-l 1500

Here, ``--max-l`` sets the maximum value of spherical harmonic degree :math:`l`
to sum up to. By default, the output is in the file ``log.quake.log``.


Postseismic displacements
-------------------------

With the appropriate viscoelastic ``earth.model`` in place, run ``quake.py``
with the option ``--task postseismic`` (or, in short, ``--task post``, or even
``-t post``) to perform postseismic displacement calculations. For example,

.. code-block:: bash

   python quake.py --task post --fault strainx.inTHRUST --max-l 200

The output is again in the file ``log.quake.log``. Postseismic calculations
usually take more time.

If the ``--task`` argument is not supplied, ``quake.py`` reports the sum of
both displacements. It requires the input fault file to be of ``visco1d``
format since the time interval is needed to calculate cumulative postseismic
displacements.


Plotting
--------

After generating a displacement field, usually as ``log.quake.log``, you can
plot it to a ``.pdf`` file with the ``plot.py`` script.

.. code-block:: bash

   python plot.py plot log.quake.log

The output files are in the folder ``${NERF_ROOT}/figures``. The horizontal
displacement field is saved to ``plot.xy.pdf``, and the vertical displacement
to ``plot.z.pdf``. Output file prefixes can also be given to the script.

Calculating Helmert parameters (*Work In Progress*)
---------------------------------------------------

To measure the sampling bias of a finite number of observation points, say,
GPS stations, we can place them randomly on the surface of the earth to
sample a simulated displacement field and take the statistics of the resulting
data set.

While in the ITRF, various complicated mechanisms (including a plate tectonic
model) must be considered while assigning weights to the GPS measurements, we
avoid this difficulty by weighing the measurements by the area of the Voronoi
cells around the stations as a proof of concept.

The ongoing implementation is at ``frame.py`` where at the moment the standard
deviation of the sampling distribution is estimated for a number of sample
sizes for the coseismic displacement field of a hard-coded seismic moment
tensor. We interpret this standard deviation as the expected error in the
Helmert parameters from the sampling bias.

You can run it by

.. code-block:: bash

   python frame.py

and change the source code for different fault descriptions or sampling sizes
or the number of trials. A more systematic way of invoking the script and
keeping records is currently being worked on.

.. _Anaconda: https://www.continuum.io
.. _PyPI: https://pypi.python.org
.. _pip: https://pip.pypa.io/en/stable/
.. _Python: https://www.python.org
.. _SciPy: https://www.scipy.org
.. _NumPy: http://www.numpy.org
.. _matplotlib: http://matplotlib.org
.. _fortranformat: https://pypi.python.org/pypi/fortranformat
.. _basemap: http://matplotlib.org/basemap
.. _Jupyter: http://jupyter.org
.. _Sphinx: http://www.sphinx-doc.org
.. _GNU Make: https://www.gnu.org/software/make/
.. _Seaborn: https://stanford.edu/~mwaskom/software/seaborn/
.. _argcomplete: https://github.com/kislyuk/argcomplete
.. _pandas: https://pandas.pydata.org
