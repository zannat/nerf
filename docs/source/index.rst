.. NERF documentation master file, created by
   sphinx-quickstart on Tue May 31 16:38:52 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

NERF's documentation
====================

NERF (Non-linear Effects on Reference Frames) is a collection of programs
for the Ph.D. project
of `Umma Jamila Zannat <http://rses.anu.edu.au/people/umma-zannat>`_
at the `Research School of Earth Sciences <http://rses.anu.edu.au>`_
of the `Australian National University <http://anu.edu.au>`_.

.. toctree::

   README
   implementation


Contents
--------

.. toctree::
   :maxdepth: 2

   nerf


Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

