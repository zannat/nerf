"""
Calculate residues of the Laplace domain displacement fields.
"""
from collections import deque, Counter
import copy

import numpy
from numpy import linalg
from earth import pick_rheology
from earth import sampling_points, discrete_integral
from layers import stress_rows
from utils import info

# the maximum number of times an interval gets split
MAX_GENERATION = 35

# the fraction of the total interval width in which we expect only one root
ROOT_WIDTH = 0.000025

# the fraction of the total interval width down to which we keep splitting
KEEP_SPLITTING = 0.00025

# difference in order of magnitude (on the natural log scale)
# of the values at the two ends of an interval
# that characterizes a discontinuity
JUMPINESS = 10.


class Interval(object):
    """
    An interval of :math:`s` values along with the corresponding values of the
    characteristic function. The variable :math:`s` is conjugate to the time
    variable :math:`t` via Laplace transform.
    """

    def __init__(self, interval, values, generation, ancestor_width):
        """
        Creates an interval of :math:`s` values.

        :arg interval: a pair of :math:`s` values
        :arg values: corresponding pair of characteristic function values
        :arg generation: the number of times an original interval
                         has been split
        :arg ancestor_width: the width of the origin ancestor interval
        """
        self.left_s, self.right_s = interval
        self.left_val, self.right_val = values
        self.generation = generation

        self.ancestor_width = ancestor_width

    @property
    def width(self):
        """
        Width of the interval.
        """
        return self.right_s - self.left_s

    @property
    def mid_s(self):
        """
        Midpoint of the interval.
        """
        return (self.left_s + self.right_s) / 2.

    @property
    def min_width(self):
        """
        Minimum width down to which the boring intervals are split.
        """
        return self.ancestor_width * KEEP_SPLITTING

    @property
    def root_width(self):
        """
        The width around a root that is excluded from further search.
        """
        return self.ancestor_width * ROOT_WIDTH

    def sign(self):
        """
        The product of the signs of the characteristic function values at
        the end points. The sign is negative when they are opposite and
        there is a root contained in the interval.
        """
        return (numpy.sign(self.left_val) *
                numpy.sign(self.right_val))

    def is_finite(self):
        """
        Whether the values of the characteristic function
        at both ends are finite.
        """
        return not (numpy.isnan(self.left_val) or
                    numpy.isnan(self.right_val))

    def is_bracketing(self):
        """
        Whether the interval brackets a root. If it does, it gets the highest
        priority in the processing queue.
        """
        return self.is_finite() and self.sign() <= 0

    def is_boring(self):
        """
        Whether the interval possibly has no root (could actually have
        an even number of roots but it is unlikely). These intervals
        have the lowest priority in the processing queue.
        """
        return self.is_finite() and self.sign() > 0

    def is_jumpy(self, poles):
        """
        Whether the bracketing interval actually brackets
        an infinity or a discontinuity.
        """
        left_val, right_val = self.left_val, self.right_val

        pole_distances = numpy.abs(numpy.array(poles) - self.mid_s)

        if left_val == 0. or right_val == 0.:
            # mostly to prevent division by zero in later checks
            return False

        if not numpy.all(pole_distances > self.root_width):
            # too close to a pole
            return True

        if abs(left_val) < 1. and abs(right_val) < 1.:
            # unlikely if the values are "small"
            return False

        if abs(numpy.log(abs(left_val / right_val))) < JUMPINESS:
            # sudden jump in order of magnitude should indicate
            # an infinity around
            return False

        return True

    def is_valid(self):
        """
        Whether the interval is not empty. In case of a bracketing interval,
        being invalid should end the search.
        """
        return self.width > 0.

    def zero_end(self):
        """
        For an interval of sign zero, one of the two ends must be zero.
        Returns that end.
        """
        assert self.sign() == 0

        if self.left_val == 0.:
            return self.left_s
        else:
            return self.right_s

    def split(self, earth):
        """
        Splits the interval according to Ridders' method if it contains a root.
        Splits into two equal halves otherwise.
        See [NumericalRecipes1992]_.
        """
        left, right, mid = self.left_s, self.right_s, self.mid_s
        left_val, right_val = self.left_val, self.right_val
        mid_val = characteristic_function(mid, earth)

        if self.is_bracketing():
            # Ridders' method
            # See Numerical Recipes by Press et al.
            x = mid + ((mid - left) *
                       numpy.sign(left_val - right_val) * mid_val /
                       numpy.sqrt(mid_val ** 2 - left_val * right_val))

            if left <= x and x <= right:
                # Ridders' method seems applicable
                # as x lies inside the interval
                x_val = characteristic_function(x, earth)

                # sort (s, characteristic function at s) pairs
                # by s values
                candidates = sorted([(left, left_val), (mid, mid_val),
                                     (x, x_val), (right, right_val)],
                                    key=lambda x: x[0])

                # create sub-intervals maintaining ordering of end points
                return [Interval((candidates[i][0], candidates[i + 1][0]),
                                 (candidates[i][1], candidates[i + 1][1]),
                                 self.generation + 1,
                                 self.ancestor_width)
                        for i in range(len(candidates) - 1)]

        # otherwise split into two equal halves
        left_interval = Interval((left, mid),
                                 (self.left_val, mid_val),
                                 self.generation + 1,
                                 self.ancestor_width)
        right_interval = Interval((mid, right),
                                  (mid_val, self.right_val),
                                  self.generation + 1,
                                  self.ancestor_width)

        return left_interval, right_interval

    def __cmp__(self, other):
        """
        Compare two intervals. This ordering determines the priority in the
        processing queue of intervals for root searching.

        :return: :math:`-1` if *self* has more priority, 1 if *other*,
                 0 if it is a tie
        """
        # bracketing intervals have the highest priority
        if self.is_bracketing() and not other.is_bracketing():
            return -1
        if not self.is_bracketing() and other.is_bracketing():
            return 1

        # intervals with characteristic function values that are not finite
        # should be processed next
        # intervals with poles at end points tend to contain a root
        # more than the boring (finite and not bracketing) ones
        if self.is_boring() and not other.is_boring():
            return 1
        if not self.is_boring() and other.is_boring():
            return -1

        # if still undecided
        # the interval with greater width gets more priority
        return numpy.sign(other.width - self.width)

    def __str__(self):
        """
        String representation with classification for easier inspection.
        """
        if self.is_bracketing():
            description = "bracketing"
        elif self.is_boring():
            description = "boring"
        else:
            description = "infinite"
        return ("{:.10e} - {:.10e} = {:+.3e} {:.3e}/{:.3e} {}"
                .format(self.left_s, self.right_s,
                        -self.width,
                        self.left_val, self.right_val,
                        description))


class PriorityQueue(object):
    """
    A simple priority queue.
    """
    def __init__(self):
        """
        Create an empty queue.
        """
        self.deque = deque()

    def push(self, interval):
        """
        Place an interval in the queue.
        If an interval is bracketing or touches a pole, it gets prioritized.
        """
        if interval.is_bracketing() or not interval.is_finite():
            self.deque.appendleft(interval)
        else:
            self.deque.append(interval)

    def pop(self):
        """
        Pop the next interval in the queue.
        """
        return self.deque.popleft()

    def is_empty(self):
        """
        Whether the queue is empty.
        """
        return len(self.deque) == 0


def find_roots(earth, l, cls, max_roots=None):
    """
    Find the roots of the characteristic function for a specified *l*.

    :arg earth: original :class:`Earth` object
    :arg l: spherical harmonics degree
    :arg cls: appropriate rheology class
    :arg max_roots: the maximum number of roots to look for
    """

    def process_interval(interval):
        """
        Process one interval for roots at a time.
        Can split the interval into smaller ones and push them into the
        processing queue for later processing.
        """
        if interval.generation > MAX_GENERATION or not interval.is_valid():
            # no need for further splitting
            if interval.is_bracketing() and not interval.is_jumpy(poles):
                # found a root
                return [interval.mid_s]

            # no root inside the interval
            return []

        if interval.sign() == 0 and interval.width < interval.root_width:
            # found a root
            # also do not want to look for other roots inside the interval
            # because it is small enough
            return [interval.zero_end()]

        if not interval.is_boring() or interval.width > interval.min_width:
            # break the interval down to smaller ones
            # push them into the processing queue
            for child in interval.split(earth):
                queue.push(child)

        # the interval was too small and boring to warrant further splitting
        return []

    # the given earth has only unique layers to reduce work
    earth = earth.prevent_matrizant_overflow(l)

    # create layers of appropriate class for the given l
    earth = earth.transform(lambda layer: cls.mimic(layer, l=l))

    # I do not know why, but Pollitz seems to imply that the roots must lie
    # between the poles
    poles = sorted(list(set([pole
                             for layer in earth.layers
                             for pole in layer.poles()])))

    # these should all be NaNs
    values = [characteristic_function(s, earth) for s in poles]

    # priority queue for the intervals
    # with intervals between poles as the initial entries
    queue = PriorityQueue()
    for i in range(len(poles) - 1):
        queue.push(Interval((poles[i], poles[i + 1]),
                            (values[i], values[i + 1]),
                            0,
                            poles[i + 1] - poles[i]))

    # process the intervals one by one
    # the processing itself usually will add more intervals to the queue
    # maintain a list of the unique roots found
    result = []
    while not queue.is_empty():
        result = list(set(result + process_interval(queue.pop())))

        # quit if the required number of roots are already found
        if max_roots is not None and len(result) >= max_roots:
            break

    if max_roots is not None and len(result) != max_roots:
        info("warning: found {} roots out of {} for l={}\n"
             .format(len(result), max_roots, l))

    return sorted(result)


def characteristic_function(s, earth):
    """
    Characteristic function at specified *s*.
    The boundary conditions can only be satisfied when this function is zero.
    The residues are evaluated at those roots.
    """
    # set the s variable of the earth layers
    # results in setting the physical properties of the layers
    # according to the correspondence principle
    earth.update_layers(s=s)

    # the characteristic function is the determinant of the stress rows
    # of the regular solutions at the surface of the earth
    # if it is zero then it is possible to form a linear combination
    # of the solutions that satisfies the boundary condition of
    # having no normal traction at the surface
    try:
        y_surface = earth.radial_solution(surface_only=True)
        return linalg.det(stress_rows(y_surface))
    except ArithmeticError:
        return float('nan')


def find_roots_range(original_earth, cls, l_range):
    """
    Find the roots of the characteristic function for a specified range
    of *l* values.
    """
    if l_range == []:
        return []

    earth = original_earth.unique_layers()

    number_of_roots = cls.number_of_roots(earth.layers)
    if number_of_roots is not None and number_of_roots < 0:
        raise ValueError("is the earth model viscoelastic?")

    if number_of_roots is None:
        # find the number of roots
        # best of five for now
        first_five_roots = [(l, find_roots(earth, l, cls, max_roots=None))
                            for l in l_range[:5]]

        rest = l_range[5:]
        if rest == []:
            return first_five_roots

        counter = Counter([len(roots) for l, roots in first_five_roots])
        number_of_roots, count = counter.most_common()[0]
        if count < 3 and counter.most_common()[1][1] == count:
            raise ValueError("what kind of earth model is this?")

        for l, roots in first_five_roots:
            if len(roots) != number_of_roots:
                info("warning: found {} roots out of {} for l={}\n"
                     .format(len(roots), number_of_roots, l))

        return (first_five_roots +
                [(l, find_roots(earth, l, cls, max_roots=number_of_roots))
                 for l in rest])
    else:
        return [(l, find_roots(earth, l, cls, max_roots=number_of_roots))
                for l in l_range]


def toroidal_roots(earth, rheology, min_l, max_l):
    """
    Determine the roots of the characteristic function for toroidal motion.
    """
    return find_roots_range(earth,
                            pick_rheology(rheology, 'toroidal'),
                            [l for l in range(min_l, max_l + 1) if l >= 1])


def spheroidal_roots(earth, rheology, min_l, max_l):
    """
    Determine the roots of the characteristic function for spheroidal motion.
    """
    return find_roots_range(earth,
                            pick_rheology(rheology, 'spheroidal'),
                            range(min_l, max_l + 1))


class ResidualMode(object):
    """
    Relevant data for a residual mode.
    """

    def __init__(self, y, monster, epsilon):
        """
        Stores information about a residual mode.

        :arg y: the solution of the equation of motion when :math:`s` equals
                *root* sampled at the layer boundaries
        :arg monster: the earth model as prescribed by the correspondence
                      principle with :math:`s` equal to *root*, represented as
                      a single layer whose physical properties are :mod:`numpy`
                      arrays containing those properties of the layers
        :arg epsilon: a normalization factor for the mode
        """
        self.y = y
        self.monster = monster
        self.epsilon = epsilon

    @property
    def l(self):
        """
        The degree of the mode.
        """
        return self.monster.l

    @property
    def s(self):
        """
        The root value of the characteristic function as a function of
        the Laplace variable :math:`s`.
        """
        return self.monster.s

    def at(self, source_index):
        """
        Reconstruct the layer at *source_index* from the monster layer.
        """
        def pick(key):
            """
            Find the property *key* for the layer at *source_index*.
            """
            value = getattr(self.monster, key)
            if numpy.shape(value) == ():
                # not an array
                return value
            else:
                return value[source_index, ...]

        cls = type(self.monster)
        return cls(**{key: pick(key)
                      for key in vars(self.monster)})


def residual_mode(l, root, earth):
    """
    Radial solution of equation of motion at a root of the characteristic
    function. To satisfy the boundary conditions, a linear combination
    of the independent solutions needs to be formed. Although the coefficients
    of this combinations are defined only upto a constant, the end results
    will not depend on it.

    :return: a :class:`ResidualMode` object containing information about
             the combined solution that obeys the boundary conditions
    """
    # set the s variable of the earth layers
    # results in setting the physical properties of the layers
    # according to the correspondence principle
    earth.update_layers(l=l, s=root)

    # indices: [sampling point, displacement-stress index, regular solution]
    # so, solutions.shape == (len(layers) + 1, N, N / 2)
    # where N is the dimension of the displacement-stress vector
    solutions = earth.radial_solution()
    number_of_solutions = solutions.shape[2]

    # NOTE: if gravity is ever implemented, will need to replace
    # this function with something that handles different dimensions
    # more generally
    assert number_of_solutions in [1, 2]

    if number_of_solutions == 1:
        # toroidal mode, nothing to do
        y = solutions[:, :, 0]

    else:
        # spheroidal mode

        # stress rows at the surface
        y_I = stress_rows(solutions[-1, :, 0])
        y_II = stress_rows(solutions[-1, :, 1])

        # since the characteristic function vanishes, the two components
        # of the ratio y_I / y_II should in principle be the same
        factor = -numpy.mean(y_I / y_II)

        # form the linear combination for which the stress rows vanish
        if abs(factor) < 1.:
            y = solutions[:, :, 0] + factor * solutions[:, :, 1]
        else:
            y = solutions[:, :, 0] / factor + solutions[:, :, 1]

    monster = copy.deepcopy(earth.monster)
    epsilon = discrete_integral(sampling_points(monster), y,
                                monster.epsilon_integrand)

    return ResidualMode(y, monster, epsilon)
