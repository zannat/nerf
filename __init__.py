"""
.. [Pollitz1992] **Pollitz, F. F.** Postseismic relaxation theory
   on the spherical earth.
   *Bulletin of the Seismological Society of America 82*, 1 (1992), 422--453.

.. [Pollitz1996] **Pollitz, F. F.** Coseismic deformation from
   earthquake faulting on a layered spherical earth.
   *Geophysical Journal International 125*, 1 (1996), 1--14.

.. [Pollitz1997] **Pollitz, F. F.** Gravitational viscoelastic postseismic
   relaxation on a layered spherical earth.
   *Journal of Geophysical Research: Solid Earth (1978--2012) 102*,
   B8 (1997), 17921--17941.

.. [VISCO1D] **Pollitz, F. F.** VISCO1D -- A program package to calculate
   quasi-static deformation on a layered spherical earth.
   *U.S. Geological Survey* (2007), `USGS_VISCO1D
   <http://earthquake.usgs.gov/research/software/#VISCO1D>`_.

.. [TakeuchiSaito1972] **Takeuchi, H. and Saito, M.** Seismic surface waves.
   *Methods in Computational Physics 11* (1972), 217--295.

.. [NumericalRecipes1992] **Press, W. H., Teukolsky, S. A.,
   Vetterling, W. T. and Flannery, B. P.** Numerical Recipes\\:
   The Art of Scientific Computing.
   *Cambridge University Press*, 1992.

.. [SteinWysession2009] **Stein, S. and Wysession, M.** An Introduction
   to Seismology, Earthquakes, and Earth Structure. *John Wiley \\& Sons*,
   2009.

.. [Renka1997] **Renka, R. J.** Algorithm 772\\: STRIPACK\\: Delaunay
   triangulation and Voronoi diagram on the surface of a sphere.
   *ACM Transactions on Mathematical Software 23*, 3 (1997), 416--434.

.. [Reddy2015] **Reddy, T.** ``py_sphere_Voronoi``.
   `doi\\: 10.5281/zenodo.13688 <http://dx.doi.org/10.5281/zenodo.13688>`_.

.. [BeazleyJones2013] **Beazley, D. and Jones, B.** Python Cookbook,
   3rd Edition.
   *O'Reilly Media*, 2013.
"""

__version__ = "0.7.0"
