#!/usr/bin/env python
# -*- coding: utf-8 -*-
# PYTHON_ARGCOMPLETE_OK
"""
GPS station data operations.
"""
from argparse import ArgumentParser

from os.path import join, basename
from glob import glob
import json

import numpy
from numpy import array, radians
from numpy import linalg

from earth import read_model
from earth import earth_radius, earth_mass, hour
from quake import read_faults, normalize_faults
from quake import write_displacement_field
import quake
from utils import Record, info, autocomplete
from frame import theoretical_shift_in_origin
from frame import theoretical_shift_in_EOPs
from frame import theoretical_shift_in_scale
from frame import theoretical_shifts
from frame import summation_shift, transformation_shift
from frame import process_options, write_options
from stations import Stations, uniform_angle_grid
from summary import Field
from tally import output_folder

MAX_L = -1


def is_number(s):
    """
    Returns whether string *s* represents a floating point number.
    """
    try:
        float(s)
        return True
    except ValueError:
        return False


def report_shift(label, helmert, fl, theoretical=None):
    """
    Print shifts in Helmert parameters in a standard form.
    """
    T, D, R = helmert
    fl.write(label + "\n")

    def show(title, x, diff=None):
        """ Show x with norm of *x* if needed. """
        if diff is None:
            return "{} {}\n".format(title, x)
        else:
            return "{} {} {}\n".format(title, x,
                                       array([linalg.norm(diff)]))

    if theoretical is None:
        fl.write(show("  T (mm):", T * 10))
        fl.write(show(" D (ppb):", D))
        fl.write(show(" R (μas):", R))
    else:
        T_theo, D_theo, R_theo = theoretical
        fl.write(show("  T (mm):", T * 10, (T - T_theo) * 10))
        fl.write(show(" D (ppb):", D, D - D_theo))
        fl.write(show(" R (μas):", R, R - R_theo))

    fl.write("\n")


def read_GLOBK_offsets(offset_file):
    """
    Coseismic offsets recorded by the GPS stations.
    """
    # mm to cm
    unit = 0.1

    names = []
    colatitudes = []
    longitudes = []
    spherical_u = []

    with open(offset_file) as fl:
        for line in fl:
            words = line.split()

            try:
                ID = words[-1][:4]
                east_off = float(words[2]) * unit
                north_off = float(words[3]) * unit
                up_off = float(words[9]) * unit
                colatitude = numpy.radians(90. - float(words[1]))
                longitude = numpy.radians(float(words[0]))

                # only when all data is present
                colatitudes.append(colatitude)
                longitudes.append(longitude)
                spherical_u.append([up_off, -north_off, east_off])
                names.append(ID)
            except (ValueError, IndexError, AttributeError):
                pass

    return names, array(colatitudes), array(longitudes), array(spherical_u).T


def stations_from_table(filename):
    """
    Read observation points from a text file.
    """
    records = []

    with open(filename) as fl:
        for line in fl:
            try:
                words = line.split()
                ID = words[0]
                assert len(ID) == 4
                colatitude = numpy.radians(90. - float(words[-1]))
                longitude = numpy.radians(float(words[-2]))

                records.append(Record(ID=ID, colatitude=colatitude,
                                      longitude=longitude))

            except (IndexError, AssertionError, ValueError):
                pass

    ID = [record.ID for record in records]
    colatitude = array([record.colatitude for record in records])
    longitude = array([record.longitude for record in records])

    return ID, colatitude, longitude


def stations_from_SOPAC(raw_folder):
    """
    Read observation points from SOPAC raw timeseries directory.
    """
    records = {}

    for neu_file in glob(join(raw_folder, '*')):
        data = {}
        with open(neu_file) as fl:
            for line in fl:
                words = line.split()

                if words[0] == '#':
                    key = words[1].lower()
                    if key == 'site':
                        data['ID'] = words[3]
                    elif key == 'latitude':
                        data['colatitude'] = radians(90 - float(words[3]))
                    elif key == 'longitude':
                        data['longitude'] = radians(float(words[3]))

                elif len(words) == 7 and all([is_number(s) for s in words]):
                    break

        records[data['ID']] = Record(**data)

    return records


def read_SOPAC_offsets(detrend_folder, date):
    """
    Read coseismic offsets from detrended SOPAC data.
    """
    def split_line(line):
        """
        Split at spaces and colons.
        """
        for word in line.split():
            for smaller in word.split(":"):
                if smaller:
                    yield smaller

    def find_offset(fl):
        """
        Find relevant offset entries in the comments section.
        """
        data = {}
        component = ""

        for line in fl:
            words = list(split_line(line))

            if len(words) == 9 and all([is_number(s) for s in words]):
                # end of comments section
                break

            if words[0] == '#':
                if len(words) > 2 and words[1].lower() in ['n', 'e', 'u']:
                    if words[2].lower() == 'component':
                        component = words[1].upper()

            elif words[0] == '#*':
                if line.find(date) != -1 and words[1].lower() == 'offset':
                    data[component] = float(words[3]) * unit

        if data == {}:
            return array([0., 0., 0.])
        else:
            def get(key):
                """ Return the *key* component is exists, 0 otherwise. """
                return data.get(key, 0.)

            return array([get('U'), -get('N'), get('E')])

    # mm to cm
    unit = 0.1

    result = {}

    for neu_file in glob(join(detrend_folder, '*')):
        ID = basename(neu_file)[:4]

        with open(neu_file) as fl:
            result[ID] = find_offset(fl)

    return result


def compare_methods(field, radius, fl, theoretical=None, Voronoi=True):
    """
    Compare different methods of determining the Helmert parameters
    side-by-side.
    """
    report_shift("Summation shift (geometric):",
                 summation_shift(field, radius), fl,
                 theoretical=theoretical)

    report_shift("Transformation shift (geometric):",
                 transformation_shift(field, radius), fl,
                 theoretical=theoretical)

    if Voronoi:
        report_shift("Summation shift (Voronoi):",
                     summation_shift(field, radius, Voronoi=True), fl,
                     theoretical=theoretical)

        report_shift("Transformation shift (Voronoi):",
                     transformation_shift(field, radius, Voronoi=True), fl,
                     theoretical=theoretical)


def setup_earthquake(args, pickle_file):
    """
    Earthquake with realistic extended faults.
    """
    earth = read_model(model_file=args.model)

    if args.fault == 'Sumatra-Andaman':
        faults = [[Sumatra_Andaman()]]
    elif args.fault == 'Tohoku-Oki':
        faults = [[Tohoku_Oki()]]
    else:
        faults, _ = read_faults(args.fault, earth)
        faults = normalize_faults(faults, False)

    setup = Record(task='coseismic', max_l=args.max_l, min_l=args.min_l,
                   rheology='detect', love_numbers_file=pickle_file,
                   debug=args.debug)

    return quake.setup_earthquake(setup, earth, faults)


def theory(args, love_numbers_file=None):
    """
    Theoretical prediction.
    """
    earthquake = setup_earthquake(args.replace(min_l=0, max_l=1),
                                  love_numbers_file)
    T = theoretical_shift_in_origin(earthquake)
    D = theoretical_shift_in_scale(earthquake)
    R = theoretical_shift_in_EOPs(earthquake)
    return T, D, R


def GLOBK_shifts(raw_args):
    """
    Evaluate error for a network.
    """
    args = Record(**vars(raw_args))

    folder = output_folder(args.output, args.name)
    pickle_file = join(folder, 'love.pickle')
    earthquake = setup_earthquake(args, pickle_file)

    offsets_file = join(args.offsets_folder, args.offsets_file)
    _, colatitudes, longitudes, GPS_u = read_GLOBK_offsets(offsets_file)
    stations = Stations(colatitudes, longitudes, earthquake.max_l)
    predicted_u = earthquake.surface_displacement_field(stations, array([0.]))

    predicted_field = Field.from_spherical(stations.theta, stations.phi,
                                           predicted_u)
    GPS_field = Field.from_spherical(stations.theta, stations.phi,
                                     GPS_u)

    write_displacement_field(join(folder, 'predicted.log'),
                             stations, predicted_field.spherical_u)
    write_displacement_field(join(folder, 'observed.log'),
                             stations, GPS_field.spherical_u)

    with open(join(folder, 'GLOBK.summary'), 'w') as fl:
        report_shift('Theoretically speaking:', theory(args), fl)

    with open(join(folder, 'GLOBK.summary'), 'w') as fl:
        report_shift('Theoretically speaking:', theory(args), fl)

        fl.write("Predictions by model:\n")
        compare_methods(predicted_field, earthquake.earth.radius * 1.e+5, fl,
                        Voronoi=True)

        fl.write("Observed:\n")
        compare_methods(GPS_field, earthquake.earth.radius * 1.e+5, fl,
                        Voronoi=True)


def SOPAC_GPS(raw_args):
    """
    Collect GPS observed offsets at SOPAC sites.
    """
    args = Record(**vars(raw_args))
    folder = output_folder(args.output, args.name)

    locations = stations_from_SOPAC(join(args.sopac_folder, 'raw'))
    offsets = read_SOPAC_offsets(join(args.sopac_folder, 'detrend'), args.date)

    IDs = set(offsets.keys())

    theta = array([locations[ID].colatitude for ID in IDs])
    phi = array([locations[ID].longitude for ID in IDs])
    spherical_u = array([offsets[ID] for ID in IDs]).T
    field = Field.from_spherical(theta, phi, spherical_u)
    stations = Stations(theta, phi, None)
    write_displacement_field(join(folder, 'field.log'),
                             stations, spherical_u)

    with open(join(folder, 'SOPAC-observed.summary'), 'w') as fl:
        # radius in cm
        compare_methods(field, earth_radius * 1.e+2, fl,
                        Voronoi=True)


def SOPAC_shifts(raw_args):
    """
    Compare observed displacements with theoretical predictions.
    """
    args = Record(**vars(raw_args)).replace(task='coseismic', depth=None,
                                            moment=None, point_source=False)

    folder = output_folder(args.output, args.name)
    pickle_file = join(folder, 'love.pickle')
    earthquake = setup_earthquake(args, pickle_file)

    locations = stations_from_SOPAC(join(args.sopac_folder, 'raw'))
    offsets = read_SOPAC_offsets(join(args.sopac_folder, 'detrend'), args.date)

    options = process_options(args, earthquake.faults, earthquake.earth)
    options['shift'] = theoretical_shifts(earthquake)
    write_options(folder, options)

    IDs = list(set(locations.keys()) & set(offsets.keys()))

    theta = array([locations[ID].colatitude for ID in IDs])
    phi = array([locations[ID].longitude for ID in IDs])
    spherical_u = array([offsets[ID] for ID in IDs]).T

    stations = Stations(theta, phi, earthquake.max_l)
    write_displacement_field(join(folder, "observed.log"),
                             stations, spherical_u)

    def write_summary(summary_file, field):
        """
        Write summary to a file.
        """
        with open(join(folder, summary_file), 'w') as fl:
            report_shift('Theoretically speaking:',
                         theory(args, pickle_file), fl)

            # radius in cm
            compare_methods(field, earthquake.earth.radius * 1.e+5,
                            fl, Voronoi=True,
                            theoretical=theory(args, pickle_file))

    write_summary('SOPAC-observed.summary',
                  Field.from_spherical(theta, phi, spherical_u))

    spherical_u = earthquake.surface_displacement_field(stations, array([0.]))
    write_displacement_field(join(folder, "predicted.log"),
                             stations, spherical_u)

    write_summary('SOPAC-predicted.summary',
                  Field.from_spherical(theta, phi, spherical_u))


def ITRF_shifts(raw_args):
    """
    Evaluate displacements at ITRF core sites
    and then the resulting shift in Helmert parameters.
    """
    args_record = Record(**vars(raw_args))
    args = args_record.replace(task='coseismic', depth=None, moment=None,
                               point_source=False)

    if args.stations_file is None:
        raise ValueError("argument --stations-file is required")

    folder = output_folder(args.output, args.name)
    pickle_file = join(folder, "love.pickle")
    output_file = join(folder, "predicted.log")

    earthquake = setup_earthquake(args, pickle_file)
    options = process_options(args, earthquake.faults, earthquake.earth)
    options['shift'] = theoretical_shifts(earthquake)
    write_options(folder, options)

    _, theta, phi = stations_from_table(join(args.stations_folder,
                                             args.stations_file))
    stations = Stations(theta, phi, earthquake.max_l)

    spherical_u = earthquake.surface_displacement_field(stations, array([0.]))
    write_displacement_field(output_file, stations, spherical_u)

    field = Field.from_spherical(stations.theta, stations.phi, spherical_u)

    with open(join(folder, "itrf.summary"), 'w') as fl:
        report_shift("Theoretically speaking:", theory(args), fl)

        # radius in cm
        compare_methods(field, earthquake.earth.radius * 1.e+5, fl,
                        theoretical=theory(args))


def Tohoku_Oki():
    """
    Point source description of the 2011 Tohoku-Oki earthquake
    in our internal unit system.
    """
    # in m
    depth = 20.0e+3

    # in 10 ** 29 dyne-cm
    Mrr = 1.73
    Mtt = -0.281
    Mpp = -1.45
    Mrt = 2.12
    Mrp = 4.55
    Mtp = -0.657

    # in N-m
    M_SI = numpy.array([[Mrr, Mrt, Mrp],
                        [Mrt, Mtt, Mtp],
                        [Mrp, Mtp, Mpp]]) * 1.e+22

    return Record(radius=1 - depth / earth_radius,
                  M=M_SI * hour ** 2 / (earth_mass * earth_radius ** 2),
                  colatitude=radians(90. - 37.52),
                  longitude=radians(143.05))


def Sumatra_Andaman():
    """
    Point source description of the 2004 Sumatra-Andaman earthquake
    in our internal unit system.
    """
    # in m
    depth = 28.6e+3

    # in 10 ** 29 dyne-cm
    Mrr = 1.04
    Mtt = -0.427
    Mpp = -0.61
    Mrt = 2.98
    Mrp = -2.4
    Mtp = 0.426

    # in N-m
    M_SI = numpy.array([[Mrr, Mrt, Mrp],
                        [Mrt, Mtt, Mtp],
                        [Mrp, Mtp, Mpp]]) * 1.e+22

    return Record(radius=1 - depth / earth_radius,
                  M=M_SI * hour ** 2 / (earth_mass * earth_radius ** 2),
                  colatitude=radians(90. - 3.09),
                  longitude=radians(94.26))


def epicentral_field(raw_args, point_source):
    """
    Calculate the displacement grid in the epicentral coordinates.
    """
    # essentially tally.py
    args = Record(**vars(raw_args))
    args = args.replace(task='coseismic', rheology='detect',
                        method='angle-grid',
                        love_numbers_file='love.pickle')

    folder = output_folder(args.output, args.name)
    model = read_model(model_file=args.model)
    faults = [[point_source.replace(colatitude=0., longitude=0.)]]

    earthquake = quake.setup_earthquake(args, model, faults)
    options = process_options(args, earthquake.faults, earthquake.earth)
    options['shift'] = theoretical_shifts(earthquake)

    options['latitudes'] = 1200
    options['longitudes'] = 2400

    info("given options\n")
    info("{}\n", json.dumps(options, indent=2, sort_keys=True))
    write_options(folder, options)

    stations = uniform_angle_grid(options['latitudes'],
                                  options['longitudes'],
                                  earthquake.max_l)

    output_file = join(folder, 'field.log')
    field = earthquake.surface_displacement_field(stations, array([0.]))
    write_displacement_field(output_file, stations, field)


def argument_parser():
    """
    Parses command line arguments.
    Passing ``-h`` as an option prints out help.
    """
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('--model', default='earth.model')
    parser.add_argument('--stations-folder', default='../data/GPS-timeseries')
    parser.add_argument('--stations-file', default=None)
    parser.add_argument('--output', default='../data')
    parser.add_argument('--name', '-n', required=True)
    parser.add_argument('--fault', '-f', default=None)
    parser.add_argument('--min-l', default=0, type=int)
    parser.add_argument('--max-l', '-l', default=MAX_L, type=int)
    parser.add_argument('--debug', '-d', action='count')
    parser.add_argument('--offsets-folder',
                        default='../data/GPS-timeseries/original_offsets')
    parser.add_argument('--offsets-file', default=None)
    parser.add_argument('--task', choices=tasks.keys(),
                        required=True)
    parser.add_argument('--sopac-folder', default='../data/sopac')
    parser.add_argument('--date', default=None)
    return parser


if __name__ == '__main__':
    numpy.set_printoptions(precision=3, suppress=False)

    tasks = {
        'ITRF': ITRF_shifts,
        'GLOBK': GLOBK_shifts,
        'SOPAC': SOPAC_shifts,
        'SA-epicentral-grid': lambda args: epicentral_field(args,
                                                            Sumatra_Andaman()),
        'TO-epicentral-grid': lambda args: epicentral_field(args,
                                                            Tohoku_Oki()),
        'SOPAC-GPS': SOPAC_GPS
    }

    command_line_parser = argument_parser()
    autocomplete(command_line_parser)
    command_line_arguments = command_line_parser.parse_args()

    tasks[command_line_arguments.task](command_line_arguments)
